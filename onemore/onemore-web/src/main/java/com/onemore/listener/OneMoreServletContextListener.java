/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 *
 * @author Brian
 */
@WebListener
public class OneMoreServletContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute("productImagePath", "image/product");
        sce.getServletContext().setAttribute("basePath", sce.getServletContext().getContextPath());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
    
}
