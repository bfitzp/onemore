/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.services;

import com.onemore.entity.OrderProduct;
import com.onemore.entity.OrderProductPK;
import com.onemore.entity.Product;
import com.onemore.entity.UserOrder;
import com.onemore.entity.sessionbean.OrderProductFacade;
import com.onemore.entity.sessionbean.ProductFacade;
import com.onemore.entity.sessionbean.UserOrderFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Brian
 */
@Named
@SessionScoped
public class ShoppingCartManager implements Serializable {

    UserOrder order;
    
    @EJB
    ProductFacade productFacade;
    
    @EJB
    UserOrderFacade userOrderFacade;
    
    @EJB
    OrderProductFacade orderProductFacade;
    
    @Inject
    OrderManager orderManager;

    public UserOrder getOrder() {
        return order;
    }

    public void setOrder(UserOrder order) {
        this.order = order;
    }

    public ShoppingCartManager() {
        order = new UserOrder();
        order.setOrderProductCollection(new ArrayList<OrderProduct>());
    }

    public void add(int productId, int quantity) {

        // If this product already exists just update the quantity of an existing
        // product, otherwise add a new orderproduct to the collection
        OrderProduct orderProduct = getOrderProductFromProductId(productId);

        if (orderProduct == null) {
            OrderProductPK orderProductPK = new OrderProductPK(productId, order.getOrderId());
            
            Product product = productFacade.find(productId);
            
            orderProduct = new OrderProduct();
            orderProduct.setOrderProductPK(orderProductPK);
            orderProduct.setQuantity(quantity);
            orderProduct.setProduct(product);
            
            order.getOrderProductCollection().add(orderProduct);
            
            // Sync with the database - Add the order product to the database and update the order
            orderProductFacade.create(orderProduct);
            userOrderFacade.edit(order);     
        }
        // Update the quantity for the appropriate product
        else {
            orderProduct.setQuantity(orderProduct.getQuantity() + quantity);
            
            // Sync with the database - Update the quantity of this order product
            orderProductFacade.edit(orderProduct);
        }
    }

    private OrderProduct getOrderProductFromProductId(int id) {
        for (OrderProduct orderProduct : order.getOrderProductCollection()) {
            if (orderProduct.getOrderProductPK().getProductProductId() == id) {
                return orderProduct;
            }
        }
        return null;
    }
    
    public void mergeOrders(Collection<UserOrder> orders) {
        UserOrder savedOrder = getShoppingCartOrder(orders);
            
        boolean orderChanged = false;
        
        // Have a saved user order. Have a session order.
        // Go through items in session order and if they have same productId as product in savedOverd, update quantity, otherwise add
        for (OrderProduct shoppingCartOrderProduct : order.getOrderProductCollection()) {
            int shoppingCartProductId = shoppingCartOrderProduct.getProduct().getProductId();
            for (OrderProduct savedOrderProduct : savedOrder.getOrderProductCollection()) {
                int savedProductId = savedOrderProduct.getProduct().getProductId();
                
                // If the product ids match update the quantity of the order product
                if (shoppingCartProductId == savedProductId) {
                    int shoppingCartQuantity = shoppingCartOrderProduct.getQuantity();
                    savedOrderProduct.setQuantity(shoppingCartQuantity);
                    orderChanged = true;
                }
                // The product doesn't already exist in the order so add it to the collection
                else {
                    savedOrder.getOrderProductCollection().add(shoppingCartOrderProduct);
                    orderChanged = true;
                }
            }
        }
        
        // Persist the updated order back to the database
        userOrderFacade.edit(savedOrder);
        
        // Set the order in the order manager
        orderManager.setOrder(order);
    }
    
    public UserOrder getShoppingCartOrder(Collection<UserOrder> orders) {
        for (UserOrder shoppingCartOrder : orders) {
            if (shoppingCartOrder.getStatus().equals("shopping_cart")) {
                return shoppingCartOrder;
            }
        }
        return null;
    }    
}
