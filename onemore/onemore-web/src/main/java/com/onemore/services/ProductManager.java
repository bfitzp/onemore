/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.services;

import com.onemore.entity.Product;
import com.onemore.entity.sessionbean.ProductFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 *
 * @author Brian
 */
@Named
@RequestScoped
public class ProductManager implements Serializable {
    @PersistenceContext(unitName = "com.onemore_onemore-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    @EJB
    ProductFacade productFacade;
    @Resource
    private UserTransaction utx;
    private List<Product> products = new ArrayList<Product>();
    
    private List<String> typeFilters = new ArrayList<String>();
    
    private List<Pagination> pagination = new LinkedList<Pagination>();

    public List<Pagination> getPagination() {
        return pagination;
    }

    public void setPagination(List<Pagination> pagination) {
        this.pagination = pagination;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public void loadProducts() {
        // TODO: Note that products will not have the latest product details
        // as it is using the persistance context. Try flushing it...?
        products = productFacade.findAll();

    }

    public List<String> getTypeFilters() {
        return typeFilters;
    }

    public void setTypeFilters(List<String> typeFilters) {
        this.typeFilters = typeFilters;
    }
    
    public void loadFilters() {
        

    }
    

    public void flush() {
        try {
            utx.begin();
            em.flush();
            em.getEntityManagerFactory().getCache().evictAll();
            utx.commit();
        } catch (Exception ex) {
        }
    }
}
