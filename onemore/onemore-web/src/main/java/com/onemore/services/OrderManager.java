/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.services;

import com.onemore.entity.OrderProduct;
import com.onemore.entity.UserOrder;
import com.onemore.entity.sessionbean.OrderProductFacade;
import com.onemore.entity.sessionbean.UserOrderFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Brian
 */
@Named
@SessionScoped
public class OrderManager implements Serializable {

    UserOrder order;
    
    @EJB
    UserOrderFacade orderFacade;
    
    @EJB
    UserOrderFacade userOrderFacade;
    
    @EJB
    OrderProductFacade orderProductFacade;

    public OrderManager() {
        order = new UserOrder();
        order.setOrderProductCollection(new ArrayList<OrderProduct>());
    }

    public UserOrder getOrder() {
        return order;
    }

    public void setOrder(UserOrder order) {
        this.order = order;
    }

    public void mergeOrders(Collection<UserOrder> savedOrders) {
        UserOrder savedOrder = getSavedOrder(savedOrders);

        boolean orderChanged = false;

        // Have a saved user order. Have a session order.
        // Go through items in session order and if they have same productId as product in savedOverd, update quantity, otherwise add
        for (OrderProduct shoppingCartOrderProduct : order.getOrderProductCollection()) {
            int shoppingCartProductId = shoppingCartOrderProduct.getProduct().getProductId();

            boolean match = false;
            for (OrderProduct savedOrderProduct : savedOrder.getOrderProductCollection()) {
                int savedProductId = savedOrderProduct.getProduct().getProductId();

                // If the product ids match update the quantity of the order product
                if (shoppingCartProductId == savedProductId) {
                    match = true;
                    int shoppingCartQuantity = shoppingCartOrderProduct.getQuantity();
                    int savedOrderQuantity = savedOrderProduct.getQuantity();
                    savedOrderProduct.setQuantity(shoppingCartQuantity + savedOrderQuantity);
                    
                    // Update the database row for this orderproduct with the new quantity
                    orderProductFacade.edit(savedOrderProduct);
                }
            }

            // The product doesn't already exist in the order so add it to the collection and update
            // the database
            if (match == false) {
                shoppingCartOrderProduct.getOrderProductPK().setUserOrderOrderId(savedOrder.getOrderId());
                savedOrder.getOrderProductCollection().add(shoppingCartOrderProduct);
                
                // Add this order product to the database
                orderProductFacade.create(shoppingCartOrderProduct);
            }
        }
        
        // The order already exists in the db
        // We need to update the orderProduct table in the db with the details of each
        // 
        
        order = savedOrder;
    }

    public UserOrder getSavedOrder(Collection<UserOrder> orders) {
        for (UserOrder savedOrder : orders) {
            if (savedOrder.getStatus().equals("shopping_cart")) {
                return savedOrder;
            }
        }
        return null;
    }

    /**
     * Clears the order for the current user, sets the current order to null.
     * Note, the previous method of setting a userOrder to a new instance of
     * a UserOrder (orderManager.setOrder(new UserOrder) cause EJBExceptions
     * due to nulls being placed in the database, when the UserOrder table
     * explicitly does not allow null values.
     */
    public void clearOrder() {
        this.order = null;
    }
}
