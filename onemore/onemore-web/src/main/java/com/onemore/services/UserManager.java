/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.services;

import com.onemore.entity.User;
import com.onemore.entity.sessionbean.UserFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Brian
 */
@Named
@RequestScoped
public class UserManager implements Serializable {
    @PersistenceContext(unitName = "com.onemore_onemore-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @EJB
    UserFacade userFacade;
    
    @Inject
    User user;
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }  

    public User loadUserFromEmail(String email) {
        user = (User)em.createQuery(
                "SELECT u FROM User u WHERE u.email = :email")
                .setParameter("email", email)
                .getSingleResult();
        
        return user;
    }

    public User loadUser(int userId) {
        user = (User)em.createQuery(
                "SELECT u FROM User u WHERE u.userId = :userId")
                .setParameter("userId", userId)
                .getSingleResult();
        
        int a = 1;
        return user;
    }
}
