/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.entity.sessionbean;

import com.onemore.entity.Employee;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Brian
 */
@Stateless
public class EmployeeFacade extends AbstractFacade<Employee> {

    @PersistenceContext(unitName = "com.onemore_onemore-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    private static final Logger logger = Logger.getLogger("EmployeeFacade");

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployeeFacade() {
        super(Employee.class);
    }

    public List<Employee> findByName(String firstName, String lastName) {
        List<Employee> results = null;
        try {
            Query query = em.createNamedQuery("Employee.findByName");
            query.setParameter("firstName", firstName);
            query.setParameter("lastName", lastName);
            results = query.getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error in EmployeeFacade, {0}", e.getMessage());
        }
        return results;
    }

    public List<Employee> findByPPS(String PPS) {
        List<Employee> results = null;
        try {
            Query query = em.createNamedQuery("Employee.findByPpsNo");
            query.setParameter("ppsNo", PPS);
            results = query.getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error in EmployeeFacade, {0}", e.getMessage());
        }
        return results;
    }

    public List<Employee> findByID(int id) {
        List<Employee> results = null;
        try {
            Query query = em.createNamedQuery("Employee.findByUserId");
            query.setParameter("userId", id);
            results = query.getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error in EmployeeFacade, {0}", e.getMessage());
        }
        return results;
    }
}
