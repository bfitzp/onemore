/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Shane Ryan <s.ryan25@nuigalway.ie>
 */
@Embeddable
public class StockOrderPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "supplierId")
    private int supplierId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "productId")
    private int productId;

    public StockOrderPK() {
    }

    public StockOrderPK(int supplierId, int productId) {
        this.supplierId = supplierId;
        this.productId = productId;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) supplierId;
        hash += (int) productId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StockOrderPK)) {
            return false;
        }
        StockOrderPK other = (StockOrderPK) object;
        if (this.supplierId != other.supplierId) {
            return false;
        }
        if (this.productId != other.productId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.onemore.entity.StockOrderPK[ supplierId=" + supplierId + ", productId=" + productId + " ]";
    }
    
}
