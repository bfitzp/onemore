/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.entity.sessionbean;

import com.onemore.entity.User;
import com.onemore.entity.UserOrder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Brian
 */
@Stateless
public class UserOrderFacade extends AbstractFacade<UserOrder> {
    @PersistenceContext(unitName = "com.onemore_onemore-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserOrderFacade() {
        super(UserOrder.class);
    }
    
    public List<UserOrder> findbyUser(User user) {
        List<UserOrder> results = null;
        try {
            Query query = em.createNamedQuery("UserOrder.findByUser");
            query.setParameter("user", user);
            results = query.getResultList();
        } catch (Exception e) {
            Logger.getAnonymousLogger().log(Level.SEVERE,"Error in UserFacade, {0}", e.getMessage());
        }
        return results;
    }
    
}
