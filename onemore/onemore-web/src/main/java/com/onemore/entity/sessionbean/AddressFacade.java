/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.entity.sessionbean;

import com.onemore.entity.Address;
import com.onemore.entity.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Brian
 */
@Stateless
public class AddressFacade extends AbstractFacade<Address>
{

    @PersistenceContext(unitName = "com.onemore_onemore-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager()
    {
        return em;
    }

    public AddressFacade()
    {
        super(Address.class);
    }

    public Object getAddressbyId(int addressId)
    {
        return em.createQuery("SELECT p FROM Address p WHERE p.addressId = :addressId")
                .setParameter("addressId", addressId)
                .getSingleResult();
    }

    public List<Address> countAddressbyUserId(User userId)
    {
        return em.createQuery("SELECT p FROM Address p WHERE p.userId = :userId")
                .setParameter("userId", userId)
                .getResultList();
    }
}
