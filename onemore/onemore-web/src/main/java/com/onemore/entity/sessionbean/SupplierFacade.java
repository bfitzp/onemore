/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.entity.sessionbean;

import com.onemore.entity.Supplier;
import com.onemore.entity.User;
import java.util.Date;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Shanwe
 */
@Stateless
public class SupplierFacade extends AbstractFacade<Supplier> {
    @PersistenceContext(unitName = "com.onemore_onemore-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SupplierFacade() {
        super(Supplier.class);
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public User setupUser(Integer userId, String email, String firstName, String lastName, String password, String role, Date creationDate, String status)
    {
         try {
            User user = addUser(userId, email, firstName, lastName, password, role, creationDate, status);
            
            String addressType = "primary";
            return user;
        } catch (Exception e) {
            return null;
        }       
    }
    
    private User addUser(Integer userId, String email, String firstName, String lastName, String password, String role, Date creationDate, String status) {

        User u = new User(userId, email, firstName, lastName, password, role, creationDate, status);

        em.persist(u);
        em.flush();
        return u;
    }
    
}
