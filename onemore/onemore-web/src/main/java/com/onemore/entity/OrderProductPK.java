/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Brian
 */
@Embeddable
public class OrderProductPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "product_product_id")
    private int productProductId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_order_order_id")
    private int userOrderOrderId;

    public OrderProductPK() {
    }

    public OrderProductPK(int productProductId, int userOrderOrderId) {
        this.productProductId = productProductId;
        this.userOrderOrderId = userOrderOrderId;
    }

    public int getProductProductId() {
        return productProductId;
    }

    public void setProductProductId(int productProductId) {
        this.productProductId = productProductId;
    }

    public int getUserOrderOrderId() {
        return userOrderOrderId;
    }

    public void setUserOrderOrderId(int userOrderOrderId) {
        this.userOrderOrderId = userOrderOrderId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) productProductId;
        hash += (int) userOrderOrderId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderProductPK)) {
            return false;
        }
        OrderProductPK other = (OrderProductPK) object;
        if (this.productProductId != other.productProductId) {
            return false;
        }
        if (this.userOrderOrderId != other.userOrderOrderId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.onemore.entity.OrderProductPK[ productProductId=" + productProductId + ", userOrderOrderId=" + userOrderOrderId + " ]";
    }
    
}
