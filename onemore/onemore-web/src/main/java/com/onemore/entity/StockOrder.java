/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shane Ryan <s.ryan25@nuigalway.ie>
 */
@Entity
@Table(name = "stock_order")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StockOrder.findAll", query = "SELECT s FROM StockOrder s"),
    @NamedQuery(name = "StockOrder.findBySupplierId", query = "SELECT s FROM StockOrder s WHERE s.stockOrderPK.supplierId = :supplierId"),
    @NamedQuery(name = "StockOrder.findByProductId", query = "SELECT s FROM StockOrder s WHERE s.stockOrderPK.productId = :productId"),
    @NamedQuery(name = "StockOrder.findByQuantity", query = "SELECT s FROM StockOrder s WHERE s.quantity = :quantity"),
    @NamedQuery(name = "StockOrder.findByCost", query = "SELECT s FROM StockOrder s WHERE s.cost = :cost"),
    @NamedQuery(name = "StockOrder.findByDate", query = "SELECT s FROM StockOrder s WHERE s.date = :date")})
public class StockOrder implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected StockOrderPK stockOrderPK;
    @Column(name = "quantity")
    private Integer quantity;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cost")
    private Float cost;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @JoinColumn(name = "supplierId", referencedColumnName = "supplier_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Supplier supplier;
    @JoinColumn(name = "productId", referencedColumnName = "product_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Product product;

    public StockOrder() {
    }

    public StockOrder(StockOrderPK stockOrderPK) {
        this.stockOrderPK = stockOrderPK;
    }

    public StockOrder(StockOrderPK stockOrderPK, Date date) {
        this.stockOrderPK = stockOrderPK;
        this.date = date;
    }

    public StockOrder(int supplierId, int productId) {
        this.stockOrderPK = new StockOrderPK(supplierId, productId);
    }

    public StockOrderPK getStockOrderPK() {
        return stockOrderPK;
    }

    public void setStockOrderPK(StockOrderPK stockOrderPK) {
        this.stockOrderPK = stockOrderPK;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Float getCost() {
        return cost;
    }

    public void setCost(Float cost) {
        this.cost = cost;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (stockOrderPK != null ? stockOrderPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StockOrder)) {
            return false;
        }
        StockOrder other = (StockOrder) object;
        if ((this.stockOrderPK == null && other.stockOrderPK != null) || (this.stockOrderPK != null && !this.stockOrderPK.equals(other.stockOrderPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.onemore.entity.StockOrder[ stockOrderPK=" + stockOrderPK + " ]";
    }
    
}
