/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.entity.sessionbean;

import com.onemore.entity.StockOrder;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Shanwe
 */
@Stateless
public class StockOrderFacade extends AbstractFacade<StockOrder> {
    @PersistenceContext(unitName = "com.onemore_onemore-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    private static final Logger logger = Logger.getLogger("StockOrderFacade");

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StockOrderFacade() {
        super(StockOrder.class);
    }

    public List<StockOrder> findQuantityRange(int min, int max) {
        List<StockOrder> results = null;
        try {
            Query query = em.createNamedQuery("StockOrder.findByQuantityRange");
            query.setParameter("min", min);
            query.setParameter("max", max);
            results = query.getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Error in UserFacade, {0}", e.getMessage());
        }
        return results;
    }

    public List<StockOrder> findPriceRange(int min, int max) {
        List<StockOrder> results = null;
        try {
            Query query = em.createNamedQuery("StockOrder.findByPriceRange");
            query.setParameter("min", min);
            query.setParameter("max", max);
            results = query.getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Error in UserFacade, {0}", e.getMessage());
        }
        return results;
    }

    public List<StockOrder> findByDateRange(Date startDate, Date endDate) {
        List<StockOrder> results = null;
        try {
            Query query = em.createNamedQuery("StockOrder.findByDateRange");
            query.setParameter("startDate", startDate);
            query.setParameter("endDate", endDate);
            results = query.getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE,"Error in UserFacade, {0}", e.getMessage());
        }
        return results;
    }
    
}
