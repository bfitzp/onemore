/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.entity.sessionbean;

import com.onemore.entity.Product;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Brian
 */
@Stateless
public class ProductFacade extends AbstractFacade<Product>
{

    @PersistenceContext(unitName = "com.onemore_onemore-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager()
    {
        return em;
    }

    public ProductFacade()
    {
        super(Product.class);
    }

    public List<Product> getDistinctProduct()
    {
        return em.createQuery("SELECT DISTINCT p.productType FROM Product p WHERE p.productType !='none' ")
                .getResultList();
    }

    public Object getProductbyId(int productId)
    {
        return em.createQuery("SELECT p FROM Product p WHERE p.productId = :productId")
                .setParameter("productId", productId)
                .getSingleResult();
    }
}
