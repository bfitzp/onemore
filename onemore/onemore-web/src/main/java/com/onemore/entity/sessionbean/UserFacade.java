/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.entity.sessionbean;

import com.onemore.entity.User;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Brian
 */
@Stateless
public class UserFacade extends AbstractFacade<User> {

    @PersistenceContext(unitName = "com.onemore_onemore-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    private static final Logger logger = Logger.getLogger("UserFacade");

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserFacade() {
        super(User.class);
    }

    public List<User> findbyRole(String role) {
        List<User> results = null;
        try {
            Query query = em.createNamedQuery("User.findByRole");
            query.setParameter("role", role);
            results = query.getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error in UserFacade, {0}", e.getMessage());
        }
        return results;
    }

    public List<User> findbyStatus(String status) {
        List<User> results = null;
        try {
            Query query = em.createNamedQuery("User.findByStatus");
            query.setParameter("status", status);
            results = query.getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error in UserFacade, {0}", e.getMessage());
        }
        return results;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public User setupUser(Integer userId, String email, String firstName, String lastName, String password, String role, Date creationDate, String status) {
        try {
            User user = addUser(userId, email, firstName, lastName, password, role, creationDate, status);
            return user;
        } catch (Exception e) {
            return null;
        }
    }

    private User addUser(Integer userId, String email, String firstName, String lastName, String password, String role, Date creationDate, String status) {
        User u = new User(userId, email, firstName, lastName, password, role, creationDate, status);
        em.persist(u);
        em.flush();
        return u;
    }

    public List<User> findByName(String firstName, String lastName) {
        List<User> results = null;
        try {
        Query query = em.createNamedQuery("User.findByName");
            query.setParameter("firstName", firstName);
            query.setParameter("lastName", lastName);
            results = query.getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error in UserFacade, {0}", e.getMessage());
        }
        return results;
    }

    public List<User> findById(int id) {
        List<User> results = null;
        try {
        Query query = em.createNamedQuery("User.findByUserId");
            query.setParameter("userId", id);
            results = query.getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error in UserFacade, {0}", e.getMessage());
        }
        return results;
    }

}
