/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.REDUNDANT;

import com.onemore.entity.Product;

/**
 *
 * @author Brian
 */
public class CartProduct {

    Product product;
    int quantity;

    public CartProduct(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }
    
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }    
}