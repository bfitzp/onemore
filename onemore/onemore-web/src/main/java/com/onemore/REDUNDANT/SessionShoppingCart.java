/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.REDUNDANT;

import com.onemore.entity.OrderProduct;
import com.onemore.entity.OrderProductPK;
import com.onemore.entity.Product;
import com.onemore.entity.sessionbean.ProductFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Brian
 *
 * This class represents a shopping cart that will used by both anonymous and
 * logged-in users and will be stored in their HTTP session. For logged-in users
 * it will be mapped to the shopping cart entity and persisted whenever it is
 * updated.
 *
 */
@Named
@SessionScoped
public class SessionShoppingCart implements Serializable {

    public int getCoolNumber() {
        return coolNumber;
    }

    public void setCoolNumber(int coolNumber) {
        this.coolNumber = coolNumber;
    }
    
    private int coolNumber = 999;
    
    @EJB
    private ProductFacade productFacade;
    
    private List<OrderProduct> orderProducts;

    public SessionShoppingCart() {
        orderProducts = new ArrayList<OrderProduct>();
    }

    public List<OrderProduct> getOrderProducts() {
        return orderProducts;
    }

    
    public void addItems(int productId, int quantity) {
        OrderProduct orderProduct = getOrderProductFromProductId(productId);

        // Create a new OrderProduct
        if (orderProduct == null) {
            OrderProductPK orderProductPK = new OrderProductPK();
            orderProductPK.setProductProductId(productId);
            
            Product product = productFacade.find(productId);
            
            orderProduct = new OrderProduct();
            orderProduct.setOrderProductPK(orderProductPK);
            orderProduct.setQuantity(quantity);
            orderProduct.setProduct(product);
            
            orderProducts.add(orderProduct);
        }
        // Update the quantity for the appropriate product
        else {
            orderProduct.setQuantity(orderProduct.getQuantity() + quantity);
        }
    }

    private OrderProduct getOrderProductFromProductId(int id) {
        for (OrderProduct orderProduct : orderProducts) {
            if (orderProduct.getOrderProductPK().getProductProductId() == id) {
                return orderProduct;
            }
        }
        return null;
    }
}
