/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.servlet.user;

import com.onemore.entity.Address;
import com.onemore.entity.User;
import com.onemore.entity.UserOrder;
import com.onemore.entity.sessionbean.AddressFacade;
import com.onemore.entity.sessionbean.UserFacade;
import com.onemore.entity.sessionbean.UserOrderFacade;
import com.onemore.services.OrderManager;
import com.onemore.services.UserManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Brian
 */
@WebServlet(name = "UserServlet", urlPatterns = {
    "/register",
    "/user/register",
    "/login",
    "/logout"})
public class UserServlet extends HttpServlet {

    @Inject
    UserManager userManager;
    @Inject
    OrderManager orderManager;
    @EJB
    UserFacade userFacade;
    @EJB
    AddressFacade addressFacade;
    @EJB
    UserOrderFacade userOrderFacade;

    private Logger logger = Logger.getLogger("UserServlet");
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String servletPath = request.getServletPath();
        String pathInfo = request.getPathInfo();

        // Registration
        if (servletPath.equals("/register") || servletPath.equals("/user/register")) {
            logger.log(Level.INFO, "Sending user to register servlet");
            request.getRequestDispatcher("/WEB-INF/jsp/register.jsp").forward(request, response);
        } // Login
        else if (servletPath.equals("/login")) {
            logger.log(Level.INFO, "Got user request to login servlet");
            if (userManager.getUser().getUserId() == null) {
                //user has not logged in already
                logger.log(Level.INFO, "User is not logged in, sending user to login page");
                request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
            } else {
                logger.log(Level.INFO, "User is logged in with userId {0}",userManager.getUser().getUserId());
                //set a session variable that is detected by the jsp pages to know that the user is logged in
                HttpSession session = request.getSession();
                session.setAttribute("user", userManager.getUser());
                logger.log(Level.INFO, "Setting session variable with role value {0}",userManager.getUser().getRole());
                session.setAttribute("authenication", userManager.getUser().getRole());
                response.sendRedirect(getServletContext().getContextPath() + "/products");
            }
        } else if (servletPath.equals("/logout")) {
            // Remove the user id from the session
            logger.log(Level.INFO, "Got request to logout user with userId {0}",userManager.getUser().getUserId());
            HttpSession session = request.getSession();
            session.invalidate();
            // Clear the session shopping cart
            orderManager.clearOrder();
            logger.log(Level.INFO, "User is logged out with userId {0}",userManager.getUser().getUserId());
            response.sendRedirect(getServletContext().getContextPath() + "/products");
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        // Retrieve the user details and create a user object
        String email = request.getParameter("email");
        String confirmEmail = request.getParameter("confirmEmail");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirmPassword");
        String role = "customer";
        String status = "yes";

        User user = new User(0, email, firstName, lastName, password, role, new Date(), status);

        // Retrieve the address details and create an address object
        String line1 = request.getParameter("line1");
        String line2 = request.getParameter("line2");
        String townCity = request.getParameter("townCity");
        String county = request.getParameter("county");
        String addressType = "billing";

        Address address = new Address(0, addressType, line1, townCity, county);

        if (line2 != null) {
            address.setLine2(line2);
        }

        // Perform validation on the user and the address
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<User>> userConstrationViolations = validator.validate(user);
        Set<ConstraintViolation<Address>> addressConstrationViolations = validator.validate(address);

        ArrayList<String> violationMessages = new ArrayList<String>();

        boolean detailsValid = true;

        // Confirm that the email addresses and passwords are correct
        if (!email.equals(confirmEmail)) {
            request.setAttribute("violation_confirmEmail", "The email addresses don't match");
            detailsValid = false;
        }

        // Confirm that the email addresses and passwords are correct
        if (!password.equals(confirmPassword)) {
            request.setAttribute("violation_confirmPassword", "The passwords don't match");
            detailsValid = false;
        }

        // If there are any validation problems report them back to the user
        for (ConstraintViolation<User> constrationViolation : userConstrationViolations) {
            request.setAttribute("violation_" + constrationViolation.getPropertyPath(),
                    "Invalid input: " + constrationViolation.getMessage());
            detailsValid = false;
        }

        for (ConstraintViolation<Address> constrationViolation : addressConstrationViolations) {
            request.setAttribute("violation_" + constrationViolation.getPropertyPath(),
                    "Invalid input: " + constrationViolation.getMessage());
            detailsValid = false;
        }

        // If there are violations report them to the user
        if (!detailsValid) {
            request.getRequestDispatcher("/register.jsp").forward(request, response);
        } // Persist the user and their billing address
        // Create a shopping_cart order for them
        // Redirect to the login page
        else {
            // Hash the user's password with MD5
            String hashedPassword = DigestUtils.md5Hex(password);
            user.setPassword(hashedPassword);

            // Catch exception thrown by users entering an email address that already exists in the database
            try {
                userFacade.create(user);
            } catch (EJBException e) {
                //send the error message to the user, why the registration failed
                logger.log(Level.SEVERE, "Failed to register user due to the exception {0}",e.getCausedByException());
                request.setAttribute("error_cause",e.getMessage());
                request.getRequestDispatcher(getServletContext().getContextPath() + "/jsp/error.jsp").forward(request, response);
                return;
            }

            address.setUserId(user);
            addressFacade.create(address);

            // Create a shopping cart order for the user
            UserOrder order = new UserOrder(0, "shopping_cart");
            order.setUserId(user);
            if(order != null) {
                order.setReceipt("NO_RECIEPT_GENERATED");
            }
            userOrderFacade.create(order);

            // Set a message in the session that will be picked up in the page after the redirect
            HttpSession session = request.getSession();
            session.setAttribute("registration_success", "You have successfully registered with OneMore. Please login below.");
            //changed to fix login
            response.sendRedirect(getServletContext().getContextPath() + "/login");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
