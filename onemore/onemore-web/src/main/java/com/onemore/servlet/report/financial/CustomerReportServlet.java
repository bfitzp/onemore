/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.servlet.report.financial;

import com.onemore.entity.User;
import com.onemore.entity.UserOrder;
import com.onemore.entity.sessionbean.EmployeeFacade;
import com.onemore.entity.sessionbean.ProductFacade;
import com.onemore.entity.sessionbean.ReviewFacade;
import com.onemore.entity.sessionbean.StockOrderFacade;
import com.onemore.entity.sessionbean.UserFacade;
import com.onemore.entity.sessionbean.UserOrderFacade;
import com.onemore.entity.sessionbean.WishlistFacade;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Shane Ryan <s.ryan25@nuigalway.ie>
 */
public class CustomerReportServlet extends HttpServlet {

    private Logger logger = Logger.getLogger("CustomerReportServlet");
    @EJB
    UserFacade users;
    @EJB
    UserOrderFacade userOrders;
    @EJB
    ReviewFacade reviewFacade;
    @EJB
    WishlistFacade wishlistFacade;
    @EJB
    ProductFacade productFacade;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        String firstName = request.getParameter("customerFname");
        String lastName = request.getParameter("customerLname");
        String customerId = request.getParameter("customerId");
        String minOrderQuantity = request.getParameter("orderMin");
        String maxOrderQuantity = request.getParameter("orderMax");
        List<User> allCustomers = users.findbyRole("customer");
//        List<User> activeCustomers = new ArrayList<>();
//        List<User> inactiveCustomers = new ArrayList<>();
        List<User> requestedCustomers = null;
        List<UserOrder> orders = userOrders.findAll();
        
//        List<String> orderCount = new ArrayList<>();
//        if (userOrders == null) {
//            logger.log(Level.WARNING, "Injection of UserOrderFacade managed bean failed");
//        } else {
//            logger.log(Level.INFO, "Injection of UserOrderFacade managed bean success, getting user orders");
//            for (User u : activeCustomers) {
//                //place all the orders in the map
//                orderCount.add(Integer.toString(userOrders.findbyUser(u).size()));
//            }
//        }
//        
//        for (User u : allCustomers) {
//            if (u.getRole().equalsIgnoreCase("customer") && u.getStatus().equalsIgnoreCase("active")) {
//                //add to active list
//                activeCustomers.add(u);
//            } else if (u.getRole().equalsIgnoreCase("customer") && u.getStatus().equalsIgnoreCase("inactive")) {
//                //add to inactive list
//                inactiveCustomers.add(u);
//            } else {
//                continue;
//            }
//        }
//        logger.log(Level.INFO, "Got all user orders mapped to their respective users {0}", orderCount);
//        //calculate basic metrics for the Customer data
//        //total number of allCustomers
//        int customerCount = allCustomers.size();
//        session.setAttribute("totalCustomerCount", customerCount);
//        //number of allCustomers active
//        int activeCount = activeCustomers.size();
//        session.setAttribute("activeCustomerCount", activeCount);
//        //number of allCustomers inactive
//        int inactiveCount = inactiveCustomers.size();
//        session.setAttribute("inactiveCustomerCount", inactiveCount);
//        //orders made by a customer
//        session.setAttribute("orderCount", orderCount);
//        //address details of customer
//        
//        if((firstName != null && !firstName.isEmpty()) && ((lastName != null && !lastName.isEmpty()))) {
//            requestedCustomers = users.findByName(firstName,lastName);
//        }
//        else if((customerId != null && !customerId.isEmpty())) {
//            requestedCustomers = users.findById(Integer.parseInt(customerId));
//            request.getRequestDispatcher("/admin/customer-financial.jsp").forward(request, response);
//        }
//        else if((minOrderQuantity != null && !minOrderQuantity.isEmpty()) && ((maxOrderQuantity != null && !maxOrderQuantity.isEmpty()))) {
//            //get a list of customers that have number of orders in this range
//            requestedCustomers = new ArrayList<>();
//            int min, max;
//            min = Integer.parseInt(minOrderQuantity);
//            max = Integer.parseInt(maxOrderQuantity);
//            for(User u : activeCustomers) {
//                if((u.getUserOrderCollection().size() >= min) && (u.getUserOrderCollection().size() <= max)) {
//                    requestedCustomers.add(u);
//                }
//                else {
//                    continue;
//                }
//            }
//        }
        session.setAttribute("queryResults", requestedCustomers);
        request.getRequestDispatcher("/admin/customer-financial.jsp").forward(request, response);
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
