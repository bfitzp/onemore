/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.servlet.shopping;

import com.onemore.entity.OrderProduct;
import com.onemore.entity.OrderProductPK;
import com.onemore.entity.Product;
import com.onemore.entity.UserOrder;
import com.onemore.entity.sessionbean.OrderProductFacade;
import com.onemore.entity.sessionbean.ProductFacade;
import com.onemore.entity.sessionbean.UserOrderFacade;
import com.onemore.services.OrderManager;
import com.onemore.services.UserManager;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Iterator;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author Brian
 */
@WebServlet(name = "ShoppingCartServlet", urlPatterns = {
    "/shopping_cart",
    "/shopping_cart/ajax/add",
    "/shopping_cart/ajax/remove",
    "/shopping_cart/ajax/update"})
public class ShoppingCartServlet extends HttpServlet {

    @EJB
    ProductFacade productFacade;
    @EJB
    UserOrderFacade userOrderFacade;
    @EJB
    OrderProductFacade orderProductFacade;
    @Inject
    OrderManager orderManager;
    @Inject
    UserManager userManager;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String servletPath = request.getServletPath();
        String pathInfo = request.getPathInfo();

        String output = "";
        
        int productId = Integer.parseInt(request.getParameter("productId"));
        // Add to cart
        if (servletPath.equals("/shopping_cart/ajax/add")) {
            int quantity = Integer.parseInt(request.getParameter("quantity"));
            output = addToCart(productId, quantity);
        }
        // Remove from cart
        else if (servletPath.equals("/shopping_cart/ajax/remove")) {
            output = removeFromCart(productId);
        }
        // Update quantity in cart
        else if (servletPath.equals("/shopping_cart/ajax/update")) {
            int quantity = Integer.parseInt(request.getParameter("quantity"));
            output = updateQuantityInCart(productId, quantity);
        }

        out.write(output);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String addToCart(int productId, int quantity) {
        Product product = productFacade.find(productId);

        // Add product to order ---> Bow is this done?
        // New  OrderProductPK with pID and oID = 0
        // Add OrderProductPK to new OrderProduct with quantity
        // Add OrderProduct to this order
        UserOrder order = orderManager.getOrder();

        int orderId = (order.getOrderId() != null ? order.getOrderId() : 0);
        OrderProductPK orderProductPK = new OrderProductPK(productId, orderId);
        OrderProduct orderProduct = new OrderProduct(orderProductPK, quantity);
        orderProduct.setProduct(product);
        orderManager.getOrder().getOrderProductCollection().add(orderProduct);

        // Sync with the database if the user is logged in
        if (userManager.getUser().getUserId() != null) {
            orderProductFacade.create(orderProduct);
        }

        ObjectMapper mapper = new ObjectMapper();
        String output = "";
        try {
            output = mapper.writeValueAsString(product);
        } catch (IOException ex) {
        }

        return output;
    }

    private String removeFromCart(int productId) {
        Collection<OrderProduct> orderProducts = orderManager.getOrder().getOrderProductCollection();

        Iterator<OrderProduct> orderProductIterator = orderProducts.iterator();

        while (orderProductIterator.hasNext()) {
            OrderProduct orderProduct = orderProductIterator.next();
            if (orderProduct.getProduct().getProductId() == productId) {          
                // Sync with the database if the user is logged in
                orderProductFacade.remove(orderProduct);
                synchronized(orderProductIterator) {
                    orderProductIterator.remove();
                }
            }
        }

        return "Removed item from cart!";
    }

    private String updateQuantityInCart(int productId, int quantity) {
        Collection<OrderProduct> orderProducts = orderManager.getOrder().getOrderProductCollection();

        for (OrderProduct orderProduct : orderProducts) {
            if (orderProduct.getProduct().getProductId() == productId) {
                orderProduct.setQuantity(orderProduct.getQuantity() + quantity);
                
                // Sync with the database if the user is logged in
                if (userManager.getUser().getUserId() != null) {
                    orderProductFacade.edit(orderProduct);
                }
                break;
            }
        }

        return Integer.toString(quantity);
    }
}
