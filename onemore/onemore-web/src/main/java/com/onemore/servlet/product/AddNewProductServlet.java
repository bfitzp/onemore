/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.servlet.product;

import com.onemore.entity.Product;
import com.onemore.entity.sessionbean.ProductFacade;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author c.loughnane1@nuigalway.ie - 09101916
 */
@WebServlet(name = "AddNewProductServlet", urlPatterns = {"/admin/ajax/addnewproduct"})
public class AddNewProductServlet extends HttpServlet {

    @EJB
    private ProductFacade productFacade;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        //TODO: put in validation
        String productname = request.getParameter("productname");
        float price = Float.parseFloat(request.getParameter("price"));
        String description = request.getParameter("description");
        int quantityInStock = Integer.parseInt(request.getParameter("quantity"));
        String imageurl = request.getParameter("imageurl");
        
        //check if user has entered a new ptroduct type
        String producttype = request.getParameter("producttype");
        if( producttype.equals("other"))
        {
            producttype = request.getParameter("other");
        }
        short avgRating = 0;
      
        Product p = new Product(0, productname, price, quantityInStock, imageurl, producttype, avgRating);
        p.setDescription(description);
        
        productFacade.create(p);
        
        request.setAttribute("productList", productFacade.findAll());
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {




        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
