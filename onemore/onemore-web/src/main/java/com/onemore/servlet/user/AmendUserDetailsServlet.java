/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.servlet.user;

import com.onemore.entity.Address;
import com.onemore.entity.User;
import com.onemore.entity.sessionbean.AddressFacade;
import com.onemore.entity.sessionbean.UserFacade;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author c.loughnane1@nuigalway.ie - 09101916
 */
@WebServlet(name = "AmendUserDetailsServlet", urlPatterns =
{
    "/ajax/amendmydetails"
})
public class AmendUserDetailsServlet extends HttpServlet
{

    @EJB
    private AddressFacade addressFacade;
    @EJB
    private UserFacade userFacade;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");

        int address_id = Integer.parseInt(request.getParameter("address_ID"));
        int user_id = Integer.parseInt(request.getParameter("user_ID"));
        
        String amend_email = request.getParameter("email");

        String amend_firstName = request.getParameter("amend_firstName");
        String amend_lastName = request.getParameter("amend_lastName");
        
        String hashedPassword = DigestUtils.md5Hex(request.getParameter("amend_password"));
        
        String amend_line1 = request.getParameter("amend_line1");
        String amend_line2 = request.getParameter("amend_line2");
        String amend_townCity = request.getParameter("amend_townCity");
        String amend_county = request.getParameter("amend_county");

        Address amendAddressDetails = addressFacade.find(address_id);
        
        amendAddressDetails.setLine1(amend_line1);
        amendAddressDetails.setLine2(amend_line2);
        amendAddressDetails.setTownCity(amend_townCity);
        amendAddressDetails.setCounty(amend_county);
        
        addressFacade.edit(amendAddressDetails);
        
        User amendUserDetails = userFacade.find(user_id);
        
        amendUserDetails.setEmail(amend_email);
        amendUserDetails.setFirstName(amend_firstName);
        amendUserDetails.setLastName(amend_lastName);
        amendUserDetails.setPassword(hashedPassword);
        
        userFacade.edit(amendUserDetails);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}
