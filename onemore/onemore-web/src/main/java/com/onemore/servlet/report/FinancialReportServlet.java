/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.servlet.report;

import com.onemore.entity.sessionbean.EmployeeFacade;
import com.onemore.entity.sessionbean.StockOrderFacade;
import com.onemore.entity.sessionbean.UserFacade;
import com.onemore.entity.sessionbean.UserOrderFacade;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Shane Ryan <s.ryan25@nuigalway.ie>
 */
public class FinancialReportServlet extends HttpServlet {

    private Logger logger = Logger.getLogger("FinancialReportServlet");
    @EJB
    UserFacade users;
    @EJB
    EmployeeFacade employeeFacade;
    @EJB
    UserOrderFacade userOrders;
    @EJB
    StockOrderFacade stockOrders;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        logger.log(Level.INFO, "Entering process request method of FinancialReportServlet with request for {0}", request.getServletPath());

//        switch (request.getServletPath()) {
//            case "/admin/financial-report-index": {
//                logger.log(Level.INFO, "Got request for all financial index, sending request ");
//                //add financial report data requested
//                request.getRequestDispatcher("/admin/index-financial.jsp").forward(request, response);
//                break;
//            }
//            case "/admin/financial-report-customer": {
//                logger.log(Level.INFO, "Got request for customer financial data, forwarding request to /CustomerReportServlet");
//                request.getRequestDispatcher("/admin/CustomerReportServlet").forward(request, response);
//                break;
//            }
//            case "/admin/financial-report-stock": {
//                logger.log(Level.INFO, "Got request for stock financial data,orwarding request to /StockReportServlet");
//                request.getRequestDispatcher("/admin/StockReportServlet").forward(request, response);
//                break;
//            }
//            case "/admin/financial-report-employee": {
//                logger.log(Level.INFO, "Got request for employee financial data, forwarding request to /EmployeeReportServlet");
//                request.getRequestDispatcher("/admin/EmployeeReportServlet").forward(request, response);
//                break;
//            }
//            default: {
//                logger.log(Level.WARNING, "Context request in financial servlet was not anticipated {0}", request.getContextPath());
//            }
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
