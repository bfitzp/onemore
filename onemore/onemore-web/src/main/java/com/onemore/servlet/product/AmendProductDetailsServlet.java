/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.servlet.product;

import com.onemore.entity.Product;
import com.onemore.entity.sessionbean.ProductFacade;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author c.loughnane1@nuigalway.ie - 09101916
 */
@WebServlet(name = "AmendProductDetailsServlet", urlPatterns = {"/admin/ajax/amendproductdetails"})
public class AmendProductDetailsServlet extends HttpServlet {

    @EJB
    private ProductFacade productFacade;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        int product_id = Integer.parseInt(request.getParameter("product_id"));
        String amend_productname = request.getParameter("amend_productname");
        float amend_price = Float.parseFloat(request.getParameter("amend_price"));
        String amend_description = request.getParameter("amend_description");
        int amend_quantity = Integer.parseInt(request.getParameter("amend_quantity"));
        String amend_imageurl = request.getParameter("amend_imageurl");

        //check if user has entered a new ptroduct type
        String amend_producttype = request.getParameter("amend_producttype");
        if (amend_producttype.equals("other")) {
            amend_producttype = request.getParameter("other");
        }
  
        //get product and amend values
        Product amendProductDetails = productFacade.find(product_id);
        
        amendProductDetails.setName(amend_productname);
        amendProductDetails.setPrice(amend_price);
        amendProductDetails.setDescription(amend_description);
        amendProductDetails.setQuantityInStock(amend_quantity);
        amendProductDetails.setImageUrl(amend_imageurl);
        amendProductDetails.setProductType(amend_producttype);
        
        productFacade.edit(amendProductDetails);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
