/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.servlet.product;

import com.onemore.entity.Product;
import com.onemore.entity.UserOrder;
import com.onemore.entity.sessionbean.UserOrderFacade;
import com.onemore.services.OrderManager;
import com.onemore.services.Pagination;
import com.onemore.services.ProductManager;
import com.onemore.services.UserManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

/**
 *
 * @author Brian
 */
public class ProductsServlet extends HttpServlet {

    @PersistenceContext(unitName = "com.onemore_onemore-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    @EJB
    UserOrderFacade userOrderFacade;
    @Inject
    ProductManager productManager;
    @Inject
    OrderManager orderManager;    
    @Inject
    UserManager userManager;
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String servletPath = request.getServletPath();
        String pathInfo = request.getPathInfo();

        if (pathInfo != null) {
            // Handle an AJAX query generation request
            if (pathInfo.contains("/ajax/query/")) {
                handleAJAXQuery(request, response);
            } // Show an individual product
            else {
                String[] parts = pathInfo.split("_");
                int productId = Integer.parseInt(parts[parts.length - 1]);

                productManager.flush();
                Product product = (Product) em.createNamedQuery("Product.findByProductId").setParameter("productId",
                        productId).getSingleResult();

                request.setAttribute("product", product);
                request.getRequestDispatcher("/WEB-INF/jsp/product.jsp").forward(request, response);
            }
        } // Else show the checkout
        else if (servletPath.equals("/checkout")) {
            request.getRequestDispatcher("/WEB-INF/jsp/checkout.jsp").forward(request, response);
        }
        // Else show the paypal page - TODO: Only allow access to this page after checking out
        else if (servletPath.equals("/paypal")) {
            String name = request.getParameter("payment");
            if (name != null) {
                if (name.equals("Proceed to payment")) {
                    // Change the status on the user order to complete and persist the completed order
                    orderManager.getOrder().setStatus("complete");
                    orderManager.getOrder().setDate(new Date());
                    userOrderFacade.edit(orderManager.getOrder());
                    
                    // Clear the completed order and give the user a new one
                    UserOrder order = new UserOrder(0, "shopping_cart");
                    order.setUserId(userManager.getUser());
                    userOrderFacade.create(order);
                    
                    orderManager.setOrder(order);
                    // Show the "Transferring you to PayPal page"
                    request.getRequestDispatcher("/WEB-INF/jsp/paypal.jsp").forward(request, response);
                } else {
                    showProducts(request, response);
                }
            }
            else {
                showProducts(request, response);
            }
        }        
        // Show all products
        else {
            showProducts(request, response);
        }
    }

    /**
     * TODO: Refactor this muck
     */
    private void handleAJAXQuery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        productManager.flush();
//{"filters":{"typeFilters":["beer"],"priceFilters":[]},"selects":{"sortBy":"price_low_to_high","numProducts":"5"},"pagination":"1"} 

        // Parse the JSON data and retrieve the query settings
        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper(factory);
        TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {
        };
        HashMap<String, Object> settings = mapper.readValue(request.getParameter("settings"), typeRef);

        int pageNumber = Integer.parseInt(settings.get("pageNumber").toString());
        LinkedHashMap filters = (LinkedHashMap) settings.get("filters");
        LinkedHashMap selects = (LinkedHashMap) settings.get("selects");
        String orderBy = selects.get("sortBy").toString();

        if (orderBy.equals("price_low_to_high")) {
            orderBy = "p.price ASC";
        } else if (orderBy.equals("price_high_to_low")) {
            orderBy = "p.price DESC";
        } else if (orderBy.equals("name_low_to_high")) {
            orderBy = "p.name ASC";
        } else if (orderBy.equals("name_high_to_low")) {
            orderBy = "p.name DESC";
        } else if (orderBy.equals("rating_low_to_high")) {
            orderBy = "p.averageRating ASC";
        } else if (orderBy.equals("rating_high_to_low")) {
            orderBy = "p.averageRating DESC";
        }

        int productsPerPage = Integer.parseInt(selects.get("productsPerPage").toString());

        ArrayList<String> typeFilters = (ArrayList) filters.get("typeFilters");
        ArrayList<String> priceFilters = (ArrayList) filters.get("priceFilters");

        boolean typeFilterExist = false;
        boolean priceFilterExist = false;

        String typeFilter = "";
        int numTypeFilters = typeFilters.size();
        if (numTypeFilters > 0) {
            typeFilterExist = true;
            typeFilter = "(";
            for (int i = 0; i < numTypeFilters; i++) {
                typeFilter += "p.productType = '" + typeFilters.get(i) + "' ";
                if (i != (numTypeFilters - 1)) {
                    typeFilter += "OR ";
                }
            }
            typeFilter += ")";
        }

        String priceFilter = "";
        int numPriceFilters = priceFilters.size();
        if (numPriceFilters > 0) {
            priceFilterExist = true;
            priceFilter = "(";
            for (int i = 0; i < numPriceFilters; i++) {
                priceFilter += "p.price ";

                if (priceFilters.get(i).equals("price-0-199")) {
                    priceFilter += "BETWEEN 0.00 AND 1.99";
                } else if (priceFilters.get(i).equals("price-200-499")) {
                    priceFilter += "BETWEEN 2.00 AND 4.99";
                } else if (priceFilters.get(i).equals("price-400-999")) {
                    priceFilter += "BETWEEN 4.00 AND 9.99";
                } else if (priceFilters.get(i).equals("price-1000-1999")) {
                    priceFilter += "BETWEEN 10.00 AND 19.99";
                } else if (priceFilters.get(i).equals("price-2000")) {
                    priceFilter += "> 20.00";
                }

                if (i != (numPriceFilters - 1)) {
                    priceFilter += "OR ";
                }
            }
            priceFilter += ")";
        }

        String where = "";
        if (typeFilterExist && priceFilterExist) {
            where = "WHERE " + typeFilter + " AND " + priceFilter;
        } else if (typeFilterExist) {
            where = "WHERE " + typeFilter;
        } else if (priceFilterExist) {
            where = "WHERE " + priceFilter;
        }

        /**
         * Pagination
         *
         * Need total number of items Need num items per page Need page to show
         *
         */
        // int totalProducts = 1;// SELECT COUNT FROM products WHERE xyz AND abc
        //int pages = totalItems/productsPerPage; // 157/10 = 16
        String countQuery =
                "SELECT COUNT(p) FROM Product p "
                + where;

        int totalProducts = (int) ((Long) em.createQuery(countQuery).getSingleResult()).longValue();

        productManager.setPagination(buildPagination(totalProducts, productsPerPage, pageNumber, request));
//        float numPagesFloat = totalProducts/(float)productsPerPage;
//        int numPages = 1; //round up float
//        
//        // Show links for two pages below and two above
//        int firstPage = (pageNumber - 2 <= 0) ? 1 : pageNumber - 2;
//        
//        int lastPage = (pageNumber + 2 >= numPages) ? numPages : pageNumber + 2;
//        
//        // Build the pagination links adding first page and last page links if we are a
//        // certain number of pages from the first or last pages
//        int showFirstLastPageLinkNumber = 3;
//        if (firstPage >= showFirstLastPageLinkNumber) {
//            productManager.getPagination().add(new Pagination("page-first", "First page", false));
//        }
//        for (int i = firstPage; i <= lastPage; i++) {
//            productManager.getPagination().add(new Pagination("page-" + i, Integer.toString(i), false));
//        }
//        
//        if (lastPage <= numPages - showFirstLastPageLinkNumber) {
//            productManager.getPagination().add(new Pagination("page-last", "Last page", false));
//        }

        String query =
                "SELECT p FROM Product p "
                + where + " "
                + "ORDER BY " + orderBy;

        List<Product> products = em.createQuery(query).setFirstResult(productsPerPage * (pageNumber - 1)).setMaxResults(productsPerPage).getResultList();
        productManager.setProducts(products);
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(ProductsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.getRequestDispatcher("/WEB-INF/jsp/products_ajax.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void showProducts(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        productManager.flush();

        // *** Move most of this code to the ProductManager class ***
        // Load the filters
        String query = "SELECT DISTINCT p.productType FROM Product p ORDER BY p.productType ASC";
        List<String> typeFilters = em.createQuery(query).getResultList();
        productManager.setTypeFilters(typeFilters);

        // Build the pagination links
        String countQuery = "SELECT COUNT(p) FROM Product p";
        int totalProducts = (int) ((Long) em.createQuery(countQuery).getSingleResult()).longValue();
        productManager.setPagination(buildPagination((int) totalProducts, 5, 1, request));

        // Load the products
        query = "SELECT p FROM Product p ORDER BY p.name ASC";
        List<Product> products = em.createQuery(query).setMaxResults(5).getResultList();

        productManager.setProducts(products);
        // productManager.loadProducts();  
        request.getRequestDispatcher("/WEB-INF/jsp/products.jsp").forward(request, response);
    }

    private List<Pagination> buildPagination(int totalProducts, int productsPerPage, int pageNumber, HttpServletRequest request) {
        if (totalProducts == 0) {
            request.setAttribute("noProducts", true);
            return null;
        }

        float numPagesFloat = totalProducts / (float) productsPerPage;
        int numPages = (int) Math.ceil(numPagesFloat);

        // Show links for two pages below and two above
        int startPage = (pageNumber - 2 <= 1) ? 2 : pageNumber - 2;
        int endPage = (pageNumber + 2 >= numPages - 1) ? numPages - 1 : pageNumber + 2;

        int startRange = 1 + (productsPerPage * (pageNumber - 1));
        int endRange = startRange + productsPerPage - 1;
        String paginationRange = Integer.toString(startRange) + " - " + Integer.toString(endRange);

        request.setAttribute("paginationTotal", totalProducts);

        if (numPages == 1) {
            request.setAttribute("paginationRange", totalProducts);
            return null;
        } else {
            request.setAttribute("paginationRange", paginationRange);
        }

        // Build the pagination links adding first page and last page links if we are a
        // certain number of pages from the first or last pages
        List<Pagination> pagination = new ArrayList<Pagination>();

        boolean selected = (pageNumber == 1) ? true : false;
        pagination.add(new Pagination("page-1", "1", selected));

        if (pageNumber > 4) {
            pagination.add(new Pagination("start-dots", "...", false));
        }

        for (int i = startPage; i <= endPage; i++) {
            selected = (pageNumber == i) ? true : false;
            pagination.add(new Pagination("page-" + i, Integer.toString(i), selected));
        }

        if (pageNumber < numPages - 3) {
            pagination.add(new Pagination("end-dots", "...", false));
        }

        selected = (pageNumber == numPages) ? true : false;
        pagination.add(new Pagination("page-" + Integer.toString(numPages), Integer.toString(numPages), selected));

        return pagination;
    }
}
