/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.servlet.filter;

import com.onemore.entity.User;
import com.onemore.services.OrderManager;
import com.onemore.services.UserManager;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Brian
 */
@WebFilter("/*")
public class UserFilter implements Filter {

    @Inject
    UserManager userManager;
    @Inject
    OrderManager orderManager;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        // Check if the user already has a session
        HttpServletRequest httpRequest = (HttpServletRequest)request;
        HttpSession session = httpRequest.getSession();

        Object userIdObject = session.getAttribute("userId");

        if (userIdObject != null) {
            userManager.loadUser((Integer) userIdObject);

        } else {

            // Check if the user has just logged in
            String userEmail = httpRequest.getRemoteUser();
            if (userEmail != null) {
                // Load the user into the UserManager. 
                // There is no need to check if this is successful or not. If the user's email is
                // not null then the user must exist in the database as it has already been checked
                // by the container upon login.
                User user = userManager.loadUserFromEmail(userEmail);

                session.setAttribute("userId", user.getUserId());

                // Merge the current session scoped order with the user's existing order
                orderManager.mergeOrders(user.getUserOrderCollection());
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
