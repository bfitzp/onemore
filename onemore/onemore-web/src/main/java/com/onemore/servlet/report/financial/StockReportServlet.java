/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.servlet.report.financial;

import com.onemore.entity.StockOrder;
import com.onemore.entity.Supplier;
import com.onemore.entity.UserOrder;
import com.onemore.entity.sessionbean.StockOrderFacade;
import com.onemore.entity.sessionbean.SupplierFacade;
import com.onemore.entity.sessionbean.UserFacade;
import com.onemore.entity.sessionbean.UserOrderFacade;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Shane Ryan <s.ryan25@nuigalway.ie>
 */
public class StockReportServlet extends HttpServlet {

    private Logger logger = Logger.getLogger("StockReportServlet");
    @EJB
    UserFacade userFacade;
    @EJB
    UserOrderFacade userOrdersFacade;
    @EJB
    StockOrderFacade stockOrdersFacade;
    @EJB
    SupplierFacade supplierFacade;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        logger.log(Level.INFO, "In StockReportServlet");
        HttpSession session = request.getSession();
        //List<User> users = userFacade.findAll();
        //List<StockOrder> stockOrders = stockOrdersFacade.findAll();
        List<StockOrder> quantityRange = null;
        List<Supplier> suppliers = supplierFacade.findAll();
        List<UserOrder> userOrders = userOrdersFacade.findAll();
        //get the input parameters
        String minQuantity = request.getParameter("minQuantity");
        String maxQuantity = request.getParameter("maxQuantity");
        String minPrice = request.getParameter("minPrice");
        String maxPrice = request.getParameter("maxPrice");
        String startOrderDate = request.getParameter("startOrderDate");
        String endOrderDate = request.getParameter("endOrderDate");

        if ((minQuantity != null && !minQuantity.isEmpty()) && (maxQuantity != null && !maxQuantity.isEmpty())) {
            //get the orders with this property
            logger.log(Level.INFO, "Got minQuantity {0}, maxQuantity {1}", new Object[]{minQuantity, maxQuantity});
            quantityRange = stockOrdersFacade.findQuantityRange(Integer.parseInt(minQuantity), Integer.parseInt(maxQuantity));
            session.setAttribute("stockOrders", quantityRange);
        } else if ((minPrice != null && !minPrice.isEmpty()) && (maxPrice != null && !maxPrice.isEmpty())) {
            //get the orders with this property
            logger.log(Level.INFO, "Got minQuantity {0}, maxQuantity {1}", new Object[]{minPrice, maxPrice});
            quantityRange = stockOrdersFacade.findPriceRange(Integer.parseInt(minPrice), Integer.parseInt(maxPrice));
            session.setAttribute("stockOrders", quantityRange);

        } else if ((startOrderDate != null && !startOrderDate.isEmpty()) && (endOrderDate != null && !endOrderDate.isEmpty())) {
            logger.log(Level.INFO, "Got order start date {0}, end date {1}", new Object[]{startOrderDate,endOrderDate});
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date startDate = format.parse(startOrderDate);
                Date endDate = format.parse(endOrderDate);
                logger.log(Level.INFO, "Parsed input date = {0}", startDate);
                quantityRange = stockOrdersFacade.findByDateRange(startDate,endDate);
                session.setAttribute("stockOrders", quantityRange);
            } catch (ParseException ex) {
                logger.log(Level.SEVERE, "Inputted date was not in the correct format", ex);
            }
        } else {
            logger.info("No query was specified, set noQuery attribute");
            quantityRange = null;
            session.setAttribute("noQuery", true);
        }


        if (quantityRange == null) {
            logger.info("No results were given for the query");
        } else {
            double stockOrderTotal = 0.0;
            for (StockOrder order : quantityRange) {
                stockOrderTotal += order.getCost();
            }
            session.setAttribute("totalCost", stockOrderTotal);
        }





        logger.info("Session attributes set for stock report");

        logger.log(Level.INFO, "Forwarding request to /admin/stock-financial.jsp");
        request.getRequestDispatcher("/admin/stock-financial.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
