
package com.onemore.servlet.report.financial;

import com.onemore.entity.Employee;
import com.onemore.entity.sessionbean.EmployeeFacade;
import com.onemore.entity.sessionbean.UserFacade;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Shane Ryan <s.ryan25@nuigalway.ie>
 */
public class EmployeeReportServlet extends HttpServlet {

    private Logger logger = Logger.getLogger("EmployeeReportServlet");
    @EJB
    UserFacade userFacade;
    @EJB
    EmployeeFacade employeeFacade;
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        logger.info("In EmployeeReportServlet");
        List<Employee> employees;
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String queryPPS = request.getParameter("employeePPS");
        String queryID = request.getParameter("employeeID");
        if((lastName != null && !firstName.isEmpty()) && (lastName != null && !lastName.isEmpty())) {
            logger.log(Level.INFO,"Got query for employee with name {0}, {1}",new Object[] {firstName,lastName});
            employees = employeeFacade.findByName(firstName,lastName);
            logger.log(Level.INFO,"Result of query {0}",employees);
        }
        else if(queryPPS != null && !queryPPS.isEmpty()) {
            logger.log(Level.INFO,"Got query for employee with PPS {0}",queryPPS);
            employees = employeeFacade.findByPPS(queryPPS);
            logger.log(Level.INFO,"Result of query {0}",employees);
        }
        else if(queryID != null && !queryID.isEmpty()) {
            logger.log(Level.INFO,"Got query for employee with user Id {0}",queryID);
            employees = employeeFacade.findByID(Integer.parseInt(queryID));
            logger.log(Level.INFO,"Result of query {0}",employees);
        }
        else {
            logger.info("No query was specified, set noQuery attribute");
            employees = null;
            session.setAttribute("noQuery", true);
        }
        
        //generate totals for employee wage count
        List<Employee> allEmployees = employeeFacade.findAll();
        double totalWage = 0.0;
        for(Employee e : allEmployees) {
            totalWage += e.getWage();
        }
        logger.log(Level.INFO,"Total employee wage count is {0}",totalWage);
        
        //get the employee total count
        //get the employee count for the result set
        
        //set the session atrributes
        //session.setAttribute("users", users);
        session.setAttribute("employees", employees);
        session.setAttribute("totalWage", totalWage);
        session.setAttribute("employeeCount", allEmployees.size());
        logger.log(Level.INFO, "Forwarding request to /admin/employee-financial.jsp");
        request.getRequestDispatcher("/admin/employee-financial.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
