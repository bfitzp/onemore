/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.servlet.user;

import com.onemore.entity.Address;
import com.onemore.entity.User;
import com.onemore.entity.sessionbean.AddressFacade;
import com.onemore.entity.sessionbean.UserFacade;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author c.loughnane1@nuigalway.ie - 09101916
 */
@WebServlet(name = "AddAddressServlet", urlPatterns =
{
    "/ajax/addaddress"
})
public class AddAddressServlet extends HttpServlet
{

    @EJB
    private AddressFacade addressFacade;
    @EJB
    private UserFacade userFacade;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        int user_id = Integer.parseInt(request.getParameter("user_ID"));

        User user = userFacade.find(user_id);
        List addressList = addressFacade.countAddressbyUserId(user);

        String report;
        if (addressList.size() < 3)
        {
            String new_line1 = request.getParameter("new_line1");
            String new_line2 = request.getParameter("new_line2");
            String new_townCity = request.getParameter("new_townCity");
            String new_county = request.getParameter("new_county");

            Address address = new Address(0, "delivery", new_line1, new_townCity, new_county);

            address.setLine2(new_line2);

            address.setUserId(user);

            addressFacade.create(address);
            
            report = "done";
            out.write(report);
        } 
        else
        {
            report = "toomany";
            out.write(report);
        }
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}
