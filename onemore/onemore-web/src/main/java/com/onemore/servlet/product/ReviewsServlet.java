/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.servlet.product;

import com.onemore.entity.Product;
import com.onemore.entity.Review;
import com.onemore.entity.ReviewPK;
import com.onemore.entity.sessionbean.ProductFacade;
import com.onemore.entity.sessionbean.ReviewFacade;
import com.onemore.services.ProductManager;
import com.onemore.services.UserManager;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author Brian
 */
@WebServlet(name = "ReviewsServlet", urlPatterns = {
    "/reviews/ajax/add"})
public class ReviewsServlet extends HttpServlet {

    @Inject
    UserManager userManager;
    @Inject
    ProductManager productManager;
    @EJB
    ProductFacade productFacade;
    @EJB
    ReviewFacade reviewFacade;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String servletPath = request.getServletPath();
        String pathInfo = request.getPathInfo();

        // Add review
        int productId = Integer.parseInt(request.getParameter("productId"));
        if (servletPath.equals("/reviews/ajax/add")) {
            String title = request.getParameter("title");
            short rating = Short.parseShort(request.getParameter("rating"));
            String body = request.getParameter("body");
            
            addReview(productId, title, rating, body);
            
            ObjectMapper mapper = new ObjectMapper();
            String json =  mapper.writeValueAsString(userManager.getUser());
            out.write(json);
        } // Remove review
        else if (servletPath.equals("/reviews/ajax/remove")) {
        } // Update review
        else if (servletPath.equals("/reviews/ajax/update")) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String addReview(int productId, String title, short rating, String body) {
        ReviewPK reviewPK = new ReviewPK(userManager.getUser().getUserId(), productId);
        Review review = new Review(reviewPK, title, body, rating);

        reviewFacade.create(review);
        
        // Update the average review in the product table
        productManager.flush();
        Product product = productFacade.find(productId);
        
        int count = 0;
        float total = 0;
        for (Review productReview : product.getReviewCollection()) {
            total += productReview.getRating();
            count++;
        }
        
        float averageRating = total/(float)count;
        
        product.setAverageRating((short)Math.round(averageRating));
        productFacade.edit(product);

        return "Review added";
    }
}
