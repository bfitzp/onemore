/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onemore.servlet.shopping;

import com.onemore.entity.OrderProduct;
import com.onemore.REDUNDANT.SessionShoppingCart;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author Brian
 */
@WebServlet(name = "HeaderShoppingCartServlet", urlPatterns = {"/ajax/header_shopping_cart"})
public class HeaderShoppingCartServlet extends HttpServlet {

    // CDI Bean (SessionScoped)
    @Inject
    SessionShoppingCart shoppingCart;

    public static final int maxItemsInOverlay = 5;
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
//        HttpSession session = request.getSession();
//        
//        // Retrieve the shopping cart from the session, creating a new shopping cart if none exists
//        SessionShoppingCart shoppingCart = (SessionShoppingCart)session.getAttribute("shoppingCart");
//        
//        if (shoppingCart == null) {
//            out.write("empty");
//        } else{
        
        
        

        // TODO: Fetch the actual products from the database and write them back
        List<OrderProduct> orderProducts = shoppingCart.getOrderProducts();
        int numCartProducts = orderProducts.size();
        
        int numItemsInOverlay = maxItemsInOverlay;
        if (numCartProducts < maxItemsInOverlay) {
            numItemsInOverlay = numCartProducts;
        }
        List<OrderProduct> overlayProducts = orderProducts.subList(numCartProducts - numItemsInOverlay, numCartProducts);
        
        ObjectMapper mapper = new ObjectMapper();
        String json =  mapper.writeValueAsString(overlayProducts);
      
        try {
            out.write(json);
            // Deliberate delay to test ajax-loader.gif
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(HeaderShoppingCartServlet.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
