/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var baseUrl = "/" + window.location.pathname.split( '/' )[1];

$(function() {
  
    $('.submit').click(function(){
        
            $.ajax({
            type: "POST",
            url: baseUrl + "/admin/newemployee",
            data: {
                firstName: $('input[fname="employeeFName"]').val(),
                LastName: $('input[lname="employeeLName"]').val(),
                employeePPS: $('input[name="employeePPS"]').val(),
                employeeWage: $('input[name="employeeWage"]').val(),
                line1: $('input[name="line1"]').val(),
                line2: $('input[name="line2"]').val(),
                town_city: $('input[name="townCity"]').val(),
                county: $('input[name="county"]').val(),
                password: $('input[name="password"]').val()
            }
        })
        .done(function(){

            $('.add-success-notification').fadeIn('slow', function(){
                $('.add-success-notification').delay(2000).fadeOut('slow');
            });
            
        })
        .fail(function() {
            alert('ERROR: Please recheck your input details.');
        });
    });   
});