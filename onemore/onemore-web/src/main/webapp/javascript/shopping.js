/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//TODO: Add OneMore namespace
var baseUrl = "/" + window.location.pathname.split( '/' )[1];

// Flag that indicates the shopping cart overlay needs to be updated (as a result of
// adding or deleting items from the cart)
var updateShoppingCartOverlay = true;

$(function() {
    // Define click functionalty for all products on the page
    ajaxifyProducts();
    
    // Show information on shopping cart upon mouseover
    ajaxifyShoppingCart();
});

function ajaxifyProducts() {
    $(".product").click(function(e){
        var product_id = $(this).attr("id").split("-")[1];
        var product_quantity = $("#pquantity-" + product_id).val();
        // Make an AJAX call to the server which will add the product to the user's shopping cart
        $.ajax({
            type: "POST",
            url: baseUrl + "/ajax/add_to_cart",
            data: { product_id: product_id, product_quantity: product_quantity }
        })
        .done(function(){
            // Update the shopping cart details in the header
            var current_total_price = parseFloat($("#shopping-cart-price").text(), 10);
            var product_price = parseFloat($('#price-' + product_id).text(), 10);
            var new_total_price = current_total_price + product_price * product_quantity;
            
            var current_total_quantity = parseInt($("#shopping-cart-quantity").text());
            var new_total_quantity = current_total_quantity + parseInt(product_quantity);
            
            $("#shopping-cart-price").text(new_total_price.toFixed(2));
            $("#shopping-cart-quantity").text(new_total_quantity);
            
            $('.product-quantity').val('1');
            
            // Set a flag indicating that the shopping cart overlay needs to be updated
            // with a new AJAX call
            updateShoppingCartOverlay = true;
        })
        .fail(function() {
            alert('fail');
        });
        // Don't follow the link
        e.preventDefault();
    });
}

function ajaxifyShoppingCart() {
    // Set a click handler that redirects to the shopping cart page
    $('#header-shopping-cart').click(function() {
        window.location.href = baseUrl + '/shopping_cart';
    });
    
    // Set up the shopping cart overlay functionality
    $('#header-shopping-cart').hover(function() {
        
        // Check if the overlay needs to be updated
        if (updateShoppingCartOverlay == true) {
            
            // Get shopping cart details from server with AJAX
            var ajaxLoaderImage = $('<img/>').attr({'src' : 'image/ajax/ajax-loader.gif', 'class' : 'header-shopping-cart-product-image'});
            $('#shopping-cart-overlay-content').html(ajaxLoaderImage);
            
            $.get(baseUrl + "/ajax/header_shopping_cart")
            .done(function(data){
                shoppingCartProducts = JSON.parse(data);
                
                // Check if there are any products in the cart
                if (shoppingCartProducts.length == 0) {
                    $('#shopping-cart-overlay-content').html('Your shopping cart is empty');
                } else {
                    var productsWrapper = $('<div/>').attr({'class' : 'header-shopping-cart-products-wrapper'});
                    $.each(shoppingCartProducts, function(index, cartProduct) {
                        var product = cartProduct.product;
                        var quantity = cartProduct.quantity;
                        var productImage = $('<img/>').attr({'src' : product.imageUrl, 'class' : 'header-shopping-cart-product-image'});
                        var leftDiv = $('<div/>').attr({'class' : 'left'});
                        leftDiv.html(productImage);
                        
                        var productDetailsDiv = $('<div/>').attr({'class' : 'header-shopping-cart-product-details'})
                            .append(product.name).append($('<br/>')).append("Quantity: " + quantity);
                        var rightDiv = $('<div/>').attr({'class' : 'right'});
                        rightDiv.html(productDetailsDiv);

                        var productDiv = $('<div/>').attr({'class' : 'header-shopping-cart-product clearfix'}).append(leftDiv).append(rightDiv);
                        productsWrapper.append(productDiv);
                    });
                    $('#shopping-cart-overlay-content').html(productsWrapper);
                }
                
                // Toggle the update flag
                updateShoppingCartOverlay = false;
            })
            .fail(function() {

            });
        }
        $("#shopping-cart-overlay").show();
        
    },
    function(){
        $("#shopping-cart-overlay").hide();
    });     
}