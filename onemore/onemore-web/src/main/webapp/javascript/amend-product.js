/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var baseUrl = "/" + window.location.pathname.split( '/' )[1];

$(function() {

    $('select[name="amend-producttype"], .button-amend-product').change (function () {
        $('.amend-success-notification').fadeOut('slow');
        var searchValue = $('select[name="amend-producttype"]').val();
        $.ajax({
            type: "POST",
            url: baseUrl + "/admin/ajax/getproductdetails",
            data: {
                product_id: searchValue              
            }
        })
        .done(function(json){
            
            //send collected data back to .jsp page
            product = JSON.parse(json);
            $('input[name="amend-productname"]').val(product['name']);
            $('input[name="amend-price"]').val(product['price']);
            $('textarea[name="amend-description"]').val(product['description']);
            $('input[name="amend-quantity"]').val(product['quantityInStock']);
            $('input[name="amend-imageurl"]').val(product['imageUrl']);
            $('.product-type-amend-select-menu').val(product['productType']);
            $('.amend-display-item').css('background-image', 'url("/onemore-web/image/product/'+ product['productType'] +'/'+ product['imageUrl'] + '")');
            $('.enter-amend-product-type').slideUp();
        })
        .fail(function() {
            alert('fail');
        });
    // Don't follow the link
    //e.preventDefault();
    });    
    
    
    $('.amend-submit').click(function(){
        
        //            var product_id = $('input[name="amend-productname"]').val();
        //            var amend_price = $('input[name="amend-price"]').val();
        $.ajax({
            type: "POST",
            url: baseUrl + "/admin/ajax/amendproductdetails",
            data: {
                product_id: $('select[name="amend-producttype"]').val(),
                amend_productname: $('input[name="amend-productname"]').val(),
                amend_price: $('input[name="amend-price"]').val(),
                amend_description: $('textarea[name="amend-description"]').val(),
                amend_quantity: $('input[name="amend-quantity"]').val(),
                amend_imageurl: $('input[name="amend-imageurl"]').val(),
                amend_producttype: $('.product-type-amend-select-menu').val(),
                amend_other: $('.enter-amend-product-type').val()
            }
        })
        .done(function(){
            
            //product = JSON.parse(json);
            $('.amend-success-notification').fadeIn('slow', function(){
                $('.amend-success-notification').delay(2000).fadeOut('slow');
            });
            
        })
        .fail(function() {
            alert('Select a product to amend from the drop menu.');
        });
    // Don't follow the link
    //e.preventDefault();
    });   
});