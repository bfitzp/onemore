/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(function() {
    
    $('.enter-new-product-type, .wrapper-amend-product-input-details, .enter-amend-product-type, .amend-success-notification, .add-success-notification').hide();
    
    $('.product-type-select-menu').change(function (){
        if($('.product-type-select-menu :selected').val()=='other')
            $('.enter-new-product-type').slideDown(function(){
                $('.enter-new-product-type').val('enter new type');
            }); 
    });
    $('.product-type-select-menu').change(function (){
        if($('.product-type-select-menu :selected').val()!='other')
            $('.enter-new-product-type').slideUp(function(){
                $('.enter-new-product-type').val('');
            });      
    });
    
    $('.button-add-product').click(function(){
        $('.wrapper-amend-product-input-details').fadeOut('fast');
        $('.wrapper-product-input-details, .upload-image').fadeIn('fast');
    });
    
    $('.button-amend-product').click(function(){
            $('.wrapper-product-input-details, .upload-image').fadeOut('fast');
            $('.wrapper-amend-product-input-details').fadeIn('fast');
    });
    
    $('.product-type-amend-select-menu').change(function (){
        if($('.product-type-amend-select-menu :selected').val()=='other')
            $('.enter-amend-product-type').slideDown(function(){
                $('.enter-amend-product-type').val('enter new type');
            }); 
    });
    $('.product-type-amend-select-menu').change(function (){
        if($('.product-type-amend-select-menu :selected').val()!='other')
            $('.enter-amend-product-type').slideUp(function(){
                $('.enter-amend-product-type').val('');
            });      
    });
    
    $('.first-name').val("test");
    

});

