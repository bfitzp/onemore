/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var baseUrl = "/" + window.location.pathname.split( '/' )[1];

$(function() {
    
    $('.amend-success-notification').hide();
    $('.details').css("color", "#b8b0c8");
     
    //get user details
    $('select[name="amend-mydetails"]').click(function () {
        
        $('.details').css("color", "#b8b0c8");
        var thisDetail = $('select.mydetails-select-menu option:selected').val();
        $('.addressID'+thisDetail).css("color","black");
        
        $.ajax({
            type: "POST",
            url: baseUrl + "/ajax/getuserdetails",
            data: {
                addressID: thisDetail              
            }
        })
        .done(function(json){
            
            //send collected data back to .jsp page
            address = JSON.parse(json);
            $('input[name="email"]').val(address.userId.email);
            $('input[name="firstName"]').val(address.userId.firstName);
            $('input[name="lastName"]').val(address.userId.lastName);
            $('input[name="line1"]').val(address['line1']);
            $('input[name="line2"]').val(address['line2']);
            $('input[name="townCity"]').val(address['townCity']);
            $('input[name="county"]').val(address['county']);
        })
        .fail(function() {
            alert('There was an error reteiving user details.');
        });
    });   
    
    
    //amend user details
    $('.amend-submit').click(function(){
        $.ajax({
            type: "POST",
            url: baseUrl + "/ajax/amendmydetails",
            data: {
                address_ID: $('select.mydetails-select-menu option:selected').val(),
                user_ID: address.userId.userId,
                email: address.userId.email,
                amend_password: $('input[name="password"]').val(),
                amend_firstName: $('input[name="firstName"]').val(),
                amend_lastName: $('input[name="lastName"]').val(),
                amend_line1: $('input[name="line1"]').val(),
                amend_line2: $('input[name="line2"]').val(),
                amend_townCity: $('input[name="townCity"]').val(),
                amend_county: $('input[name="county"]').val()
            }
        })
        .done(function(){
            
            //product = JSON.parse(json);
            $('.amend-success-notification').fadeIn('slow', function(){
                $('.amend-success-notification').delay(2000).fadeOut('slow');
            });
            
        })
        .fail(function() {
            alert('There was an error amending user details.');
        });
    });  
    
    //add address details
    $('.add-submit').click(function(){
        $.ajax({
            type: "POST",
            url: baseUrl + "/ajax/addaddress",
            data: {
                address_ID: $('select.mydetails-select-menu option:selected').val(),
                user_ID: address.userId.userId,
                email: address.userId.email,
                new_password: $('input[name="password"]').val(),
                amend_firstName: $('input[name="firstName"]').val(),
                amend_lastName: $('input[name="lastName"]').val(),
                new_line1: $('input[name="line1"]').val(),
                new_line2: $('input[name="line2"]').val(),
                new_townCity: $('input[name="townCity"]').val(),
                new_county: $('input[name="county"]').val()
            }
        })
        .done(function(report){
            
            if(report == "toomany")
            {
                alert('You are limited to three addresses.')
            }
            else
            {
                $('.amend-success-notification').fadeIn('slow', function(){
                    $('.amend-success-notification').delay(2000).fadeOut('slow');
                });
                alert('Address added will appear on your next login!');
            }
            
        })
        .fail(function() {
            alert('There was an error adding address.');
        });
    });  
    
});
