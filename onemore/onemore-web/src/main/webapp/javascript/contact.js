/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var baseUrl = "/" + window.location.pathname.split( '/' )[1];



$(function() {
    
    $('.contact-success-notification').hide();
    
    $('.contact-submit').click(function()
    {
        $.ajax({
            type: "POST",
            url: baseUrl + "/ajax/contactsubmitservlet",
            data: {

        }
        })
        .done(function(){
            
            $('input[name="name"]').val('');
            $('input[name="email"]').val('');
            $('textarea[name="description"]').val('Thank you for contacting ');
            $('textarea[name="description"]').val('Thank you for contacting onemore. One of our service representatives will contact you    as soon as possible.');
            $('.contact-success-notification').fadeIn('slow', function(){
                $('.contact-success-notification').delay(2000).fadeOut('slow', function(){
                    $('.contact-success-notification').delay(2000).fadeOut('slow');
                });        
            })
            .fail(function() {
                alert('There was an error in your submittion, please retry.');
            });
        });   
    });
});