$(function() {
    ajaxifyCheckout();
})

/**
 * Add ajax functionality to the checkout page
 */
function ajaxifyCheckout() {
    // Ajaxify the address section
    $("ul#addresses input").first().attr("checked", "checked");
    
    
    $('#checkout-shopping-cart tbody tr').each(function() {
        ajaxifyCheckoutShoppingCartRow(this);
    });
}

/**
 * Set click handlers for the buttons in the new shopping cart cell
 */
function ajaxifyCheckoutShoppingCartRow(row) {
    var productId = $(row).attr("id").split("-")[2];
    
    // Retrieve the cells to ajaxify
    var quantityDecreaseCell = $(row).find('.decrease-quantity').parent();
    var quantityIncreaseCell = $(row).find('.increase-quantity').parent();
    var removeCell = $(row).find('.remove');
    
    // Set the quantity decrease cell click handler
    quantityDecreaseCell.click(function() {
        var quantity = $(this).parent().find(".quantity");
        if (quantity.text() !== '1') {
            // Make an AJAX call to the server which will decrement the quantity of the item
            // in the user's shopping cart
            callServerShoppingCart("update", productId, parseInt(-1), function() {             
                quantity.text(parseInt(quantity.text()) - 1);
            });
        }
    });
    
    // Set the quantity increase cell click handler
    quantityIncreaseCell.click(function() {
        var quantity = $(this).parent().find(".quantity");
        // Make an AJAX call to the server which will increment the quantity of the item
        // in the user's shopping cart
        callServerShoppingCart("update", productId, 1, function() {
            quantity.text(parseInt(quantity.text()) + 1);
        });
      
    });
    
    // Set the remove button click handler
    removeCell.click(function() {
        var row = $(this).parent('tr');
        var rowQuantity = "-" + $(row).find(".quantity").text();
        callServerShoppingCart("remove", productId, parseFloat(rowQuantity), function() {
        });        
    });
}

/**
 * Wrapper for AJAX calls to the shopping cart on the server
 */
function callServerShoppingCart(action, productId, quantity, callback) {
    var url = baseUrl + "/shopping_cart/ajax/";
    
    // Determine the action to take
    switch(action) {
        case "add":
            url += "add";
            break;
        case "remove":
            url += "remove";
            break;
        case "update":
            url += "update";
            break;
    }
    
    // Call the server
    $.ajax({
        type: "POST",
        url: url,
        data: {
            productId: productId,
            quantity: quantity
        }
    })
    .done(function(data) {
        callback(data);
        
        // Update the shopping cart total
        var itemAmount = $("#checkout-shopping-cart #product-id-" + productId + " .price span").text();
        
        var shoppingCartTotalElement = $("#shopping-cart-total span");
        var shoppingCartTotal = parseFloat($("#shopping-cart-total span").text());

        itemsAmount = itemAmount * quantity;
        shoppingCartTotal += itemsAmount;
        
        $("#shopping-cart-total span").text(shoppingCartTotal.toFixed(2));
        
        // Remove the item if necessary
        if (action === "remove") {
            var row = $("#checkout-shopping-cart #product-id-" + productId);
            // If there are no items left in the shopping cart remove the checkout button
            var rows = row.parents("#checkout-shopping-cart").find('tr');
            if (rows.length == 2) {
                $("#shopping-cart-total").remove();
                $("#checkout-button").remove();
            }
            row.remove();
         }        
    });
}