/**
 *
 * @author Brian
 * 
 * Add AJAX functionality to every action in the shopping cart system.
 */

var baseUrl = "/" + window.location.pathname.split( '/' )[1];

/**
 * Ajaxify various items on the page
 */
$(function() {
    ajaxifyQuantityAdjusters();
    
    ajaxifyAddToCarts();
    
    ajaxifyShoppingCart();
    
    ajaxifyPaginationSortFilter();
});

function ajaxifyQuantityAdjusters() {
    $(".decrease-quantity").click(function(){
        var quantityInput = $(this).next();
        quantityInput.val(parseInt(quantityInput.val()) - 1);
        if (quantityInput.val() === '0') {
            quantityInput.val(1);
        }
    });
    $(".increase-quantity").click(function(){
        var quantityInput = $(this).prev();
        quantityInput.val(parseInt(quantityInput.val()) + 1);
    });    
}

/**
 * Add a click handler to all 'Add to cart' buttons that will place the item in the sidebar shopping cart and
 * notify the server
 */
function ajaxifyAddToCarts() {
    $(".add-to-cart").click(function(e){
        var productId = $(this).attr("id").split("-")[3];
        var quantity = $("#quantity-" + productId).val();
        
        // Determine if this product is already in the cart and if so update it's quantity,
        // otherwise add it to the cart
        var productRow;
        $("#shopping-cart").find("tbody tr").each(function() {
            if ($(this).hasClass("header") === false) {
                var cartProductId = $(this).attr("id").split("-")[2];
                // The product is already in the cart so update the quantity
                if (productId === cartProductId) {
                    productRow = this;
                }
            }
        });
        
        // The product is already in the cart so update its quantity
        if (productRow !== undefined) {
            var rowQuantity = $(productRow).find(".quantity");
            
            callServerShoppingCart("update", productId, quantity, function(data) {
                rowQuantity.text(parseInt(rowQuantity.text()) + parseInt(quantity));
            });
        }
        // The product is not in the cart so add it to the cart
        else {
            callServerShoppingCart("add", productId, quantity, function(data) {
                product = JSON.parse(data);
                // Update to the shopping cart in the sidebar
                updateSidebarShoppingCart(product, quantity);
            });
        }
        
        // Don't follow the link
        e.preventDefault();
    });
};

function updateSidebarShoppingCart(product, quantity) {
    // Build a new row
    var shoppingCartRow = $("<tr/>").attr({
        "id" : "product-id-" + product.productId
    });
    var quantityDecreaseCell = $("<td/>").attr({
        "class" : "quantity-cell"
    })
    .html($("<div/>").attr({
        "class" : "decrease-quantity", 
        "title" : "Decrease quantity"
    }).text("-"));
    var quantityCell = $("<td/>").attr({
        "class" : "quantity"
    }).text(quantity);
    var quantityIncreaseCell = $("<td/>").attr({
        "class" : "quantity-cell"
    })
    .html($("<div/>").attr({
        "class" : "increase-quantity", 
        "title" : "Increase quantity"
    }).text("+"));
    var productCell = $("<td/>").text(product.name);
    var priceCell = $("<td/>").html("€<span class='price'>" + product.price.toFixed(2) + "</span>");
    var removeCell = $("<td/>").attr({
        "class" : "remove", 
        "title" : "Remove item"
    }).text("X");
    
    // Add the cells to the row and ajaxify them
    shoppingCartRow.append(quantityDecreaseCell);
    shoppingCartRow.append(quantityCell);
    shoppingCartRow.append(quantityIncreaseCell);
    shoppingCartRow.append(productCell);
    shoppingCartRow.append(priceCell);
    shoppingCartRow.append(removeCell);
    ajaxifyShoppingCartRow(shoppingCartRow);
    
    $('#shopping-cart tbody').append(shoppingCartRow);
    
    // If the checkout button doesn't exists create it
    if ($("#checkout-button").length == 0) {
        $('<a/>').attr({
            "id" : "checkout-button",
            "class" : "standard-button", 
            "href" : baseUrl + "/checkout"
        }).text('Checkout').insertAfter('#shopping-cart table');
    }
}

function ajaxifyShoppingCart() {
    $('#shopping-cart tbody tr').each(function() {
        ajaxifyShoppingCartRow(this);
    });
}

/**
 * Set click handlers for the buttons in the new shopping cart cell
 */
function ajaxifyShoppingCartRow(row) {
    var productId = $(row).attr("id").split("-")[2];
    
    // Retrieve the cells to ajaxify
    var quantityDecreaseCell = $(row).find('.decrease-quantity').parent();
    var quantityIncreaseCell = $(row).find('.increase-quantity').parent();
    var removeCell = $(row).find('.remove');
    
    // Set the quantity decrease cell click handler
    quantityDecreaseCell.click(function() {
        var quantity = $(this).parent().find(".quantity");
        if (quantity.text() !== '1') {
            // Make an AJAX call to the server which will decrement the quantity of the item
            // in the user's shopping cart
            callServerShoppingCart("update", productId, parseInt(-1), function() {
                quantity.text(parseInt(quantity.text()) - 1);
            });
        }
    });
    
    // Set the quantity increase cell click handler
    quantityIncreaseCell.click(function() {
        var quantity = $(this).parent().find(".quantity");
        // Make an AJAX call to the server which will increment the quantity of the item
        // in the user's shopping cart
        callServerShoppingCart("update", productId, 1, function() {
            quantity.text(parseInt(quantity.text()) + 1); 
        });
      
    });
    
    // Set the remove button click handler
    removeCell.click(function() {
        var row = $(this).parent('tr');
        var rowQuantity = "-" + $(row).find(".quantity").text();
        callServerShoppingCart("remove", productId, parseFloat(rowQuantity), function() {

        });        
    });
}

/**
 * Wrapper for AJAX calls to the shopping cart on the server
 */
function callServerShoppingCart(action, productId, quantity, callback) {
    var url = baseUrl + "/shopping_cart/ajax/";
    
    // Determine the action to take
    switch(action) {
        case "add":
            url += "add";
            break;
        case "remove":
            url += "remove";
            break;
        case "update":
            url += "update";
            break;
    }
    
    // Call the server
    $.ajax({
        type: "POST",
        url: url,
        data: {
            productId: productId,
            quantity: quantity
        }
    })
    .done(function(data) {
        callback(data);
        
        // The product could either be in the shopping cart already or it is one of the
        // products listed on the page
        
        // Check the shopping cart first
        var itemAmount = $("#shopping-cart #product-id-" + productId + " .price").text();
        
        if (itemAmount == "") {
            itemAmount = $("#product-id-" + productId + " .price span").text();
        }
        
        var shoppingCartTotalElement = $("#shopping-cart-total span");
        var shoppingCartTotal = parseFloat($("#shopping-cart-total span").text());

        itemsAmount = itemAmount * quantity;
        shoppingCartTotal += itemsAmount;
                
        if (isNaN(shoppingCartTotal)) {
            // If the checkout button doesn't exists create it
            if (action !== "remove") {
                if ($("#shopping-cart-total").length == 0) {
                    var shoppingCartTotalElement =  $('<div/>').attr({
                        "id" : "shopping-cart-total"
                    });
                    $(shoppingCartTotalElement).html("Total €:<span>" + itemsAmount.toFixed(2) + "</span>");        
                    $(".shopping-cart-table").after(shoppingCartTotalElement);
                }
            }            
        } else {
            $("#shopping-cart-total span").text(shoppingCartTotal.toFixed(2));
        }
  
        // Remove the item if necessary
        if (action === "remove") {
            var row = $("#shopping-cart #product-id-" + productId);
            // If there are no items left in the shopping cart remove the checkout button
            var rows = row.parents("#shopping-cart").find('tr');
            if (rows.length == 2) {
                $("#shopping-cart-total").remove();
                $("#checkout-button").remove();
            }
            row.remove();
         }
    });
}

function ajaxifyPaginationSortFilter() {
    // Click handlers for left sidebar
    // Get what was clicked and get other selections and post to server
    $("#product-filter-sidebar li").click(function(e) {
        
        // Toggle the selected class on this element
        if ($(this).hasClass("selected")) {
            $(this).attr({
                "class" : ""
            });
        } else {
            $(this).attr({
                "class" : "selected"
            });
        }
        
        settings = retrieveQueryGenerationSettings();
        settings.pageNumber = 1;
        
        callServerProductsQueryGenerator(settings);
        
        e.preventDefault();
    });
        
    // Click handlers for header selects
    $("#content-header-selects select").change(function(e) {
        settings = retrieveQueryGenerationSettings();
        settings.pageNumber = 1;
        
        callServerProductsQueryGenerator(settings);
        
        e.preventDefault();
    });
    
    // Bind click handlers for pagination
    ajaxifyPagination();
}

function ajaxifyPagination() {
    $("#pagination li[id^='page-']").not(".selected").click(function(e) {
        $("#pagination li.selected").removeClass("selected");
        $(this).attr({
            "class" : "selected"
        });
        settings = retrieveQueryGenerationSettings();
        settings.resetPagination = false;
        
        callServerProductsQueryGenerator(settings);
        
        e.preventDefault();
    });    
}

/**
 * Retrieve the various items needed to generate a products query
 */
function retrieveQueryGenerationSettings() {
    var settings = new Object();
    settings.filters = retrieveFilters();
    settings.selects = retrieveHeaderSelects();
    settings.pageNumber = retrievePageNumber();
    
    return settings;    
}

function retrieveFilters() {
    var typeFilters = new Array();
    $("#product-filter-sidebar #types li").each(function() {
        if ($(this).hasClass("selected")) {
            typeFilters.push($(this).attr("id"));
        }
    });
    
    var priceFilters = new Array();
    $("#product-filter-sidebar #price-ranges li").each(function() {
        if ($(this).hasClass("selected")) {
            priceFilters.push($(this).attr("id"));
        }
    });    
    
    return { 
        typeFilters : typeFilters, 
        priceFilters : priceFilters
    }
}

function retrieveHeaderSelects() {
    var headerSelects = new Object();
    headerSelects.sortBy = $("#sort-products-select option:selected").val();
    headerSelects.productsPerPage = $("#products-per-page-select option:selected").val();
    
    return headerSelects;
}

function retrievePageNumber() {
    return $("#pagination .selected").text();
}
function callServerProductsQueryGenerator(settings){

    var url = baseUrl + "/products/ajax/query/";
    
    // Get shopping cart details from server with AJAX
    var ajaxLoaderImage = $("<img/>").attr({
        "src" : "image/ajax/ajax-loader.gif", 
        "class" : "ajax-loader"
    });
    
    ajaxLoaderImage.css({
        "display" : "block",
        "width" : "66px",
        "margin": "30px auto"        
    });
    
    $('#products-list').html(ajaxLoaderImage);    

    // Call the server
    $.ajax({
        type: "GET",
        url: url,
        contentType: "application/json; charset=utf-8",
        data: {
            settings: JSON.stringify(settings)
        }
    })
    .done(function(data) {
        // Replace the products list
        $("#products-list").html(data);
        
        // Ajaxify the new content
        ajaxifyQuantityAdjusters();
        ajaxifyAddToCarts();
        
        // Ajaxify pagination
        ajaxifyPagination();
        
        // Apply rateit
        $('div.rateit').rateit();
    });
}