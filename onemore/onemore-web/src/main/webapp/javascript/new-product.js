/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var baseUrl = "/" + window.location.pathname.split( '/' )[1];

$(function() {
  
    $('.submit').click(function(){
        
            $.ajax({
            type: "POST",
            url: baseUrl + "/admin/ajax/addnewproduct",
            data: {
                productname: $('input[name="productname"]').val(),
                price: $('input[name="price"]').val(),
                description: $('textarea[name="description"]').val(),
                quantity: $('input[name="quantity"]').val(),
                imageurl: $('input[name="imageurl"]').val(),
                producttype: $('.product-type-select-menu').val(),
                other: $('.enter-new-product-type').val()
            }
        })
        .done(function(){

            $('.add-success-notification').fadeIn('slow', function(){
                $('.add-success-notification').delay(2000).fadeOut('slow');
            });
            
        })
        .fail(function() {
            alert('ERROR: Please recheck your input details.');
        });
    });   
});