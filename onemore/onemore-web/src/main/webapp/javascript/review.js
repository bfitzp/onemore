var baseUrl = "/" + window.location.pathname.split( '/' )[1];

$(function() {
    ajaxifyReviews();
});

function ajaxifyReviews() {
    // Link the rating widget with the hidden input
    $("#new-rating").bind("rated", function (event, value) {
        $("input[name=rating]").val(value);
    });
    
    $("#review-button").click(function(e) {
        // Validate the review
        var title = $("#review-title").val();
        var rating = $("#rating").val();
        var body = $("#review-body").val();
        
        if (title === "" || rating === "0" || body === "") {
            alert("Title and body must not be empty and a rating must be given.");
        } else {
            var productDiv = $('div[id^="product-id-"]');
            var productId = $(productDiv).attr("id").split("-")[2];
            
            var url = baseUrl + "/reviews/ajax/add";
        a = 1;
        a += 2;
            // Upload the review to the server
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    productId: productId,
                    title: title,
                    rating: rating,
                    body: body
                }
            })
            .done(function(data) {
                var user = JSON.parse(data);
                $('#add-review-form').remove();
                
                // Slide the review down above the other reviews and remove the new review box
                var review = $("<li/>").attr({"id" : "review-id-" + user.userId + "-" + productId, "class" :  "review"}).hide();
                review.append($("<h4/>").text(title));
                review.append($("<div/>").attr({"class" : "rateit", "id" : "rateit-" + productId}));
                review.append($("<span/>").attr({"class" : "review-user"})
                    .text("by " + user.firstName + " " + user.lastName));
                review.append($("<p/>").text(body));

                $("#reviews-list").prepend(review);
                review.slideDown("slow", function() {
                    
                });
                
                // Apply rateit
                $("#rateit-" + productId).rateit({ max: 5, step: 1, value : rating, readonly : true, ispreset : true });
                
                // Update the number of reviews
                var numReviews = parseInt($("#num-reviews").text());
                $("#num-reviews").text(numReviews + 1);
                
            });            
        } 
        e.preventDefault();
    });
}