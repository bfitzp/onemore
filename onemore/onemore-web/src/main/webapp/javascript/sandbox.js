/**
 * Sandbox for javascript development
 */
$(function() {
    ajaxifyDummyLogin();
});

function ajaxifyDummyLogin() {
    $("#login-button").click(function(e){
        $.ajax({
            type: "POST",
            url: baseUrl + "/ajax/login"
        })
        .done(function(data) {
            var user = JSON.parse(data);
            var loggedInDiv = $('<div/>').attr({'id' : 'logged-in'}).text(user.username + ' logged in');
            var logOutButton = $('<input type="submit" value="Dummy logout" id="logout-button"/>');
            $('#header-login-box').empty().append(loggedInDiv).append(logOutButton);
        })
        .fail(function() {
            alert('FAIL');
        });
        e.preventDefault();
    });
}