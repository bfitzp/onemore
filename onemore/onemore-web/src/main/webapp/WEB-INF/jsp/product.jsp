<%-- 
    Document   : product.jsp
    Created on : 10-Feb-2013, 14:39:53
    Author     : Brian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:right_sidebar_page pageType="product-page">
    <div id="content">
        <div id="content-header">   
            <h1>${product.name}</h1>
        </div>
        <div id="product-id-${product.productId}">
            <div id="details">
                <c:if test="${product.averageRating != 0}">
                    <div id="average-rating">
                        <h4>Average rating:</h4>
                        <div class="rateit" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="${product.averageRating}"></div>
                    </div>
                </c:if>
                <img src="${basePath}/${productImagePath}/${product.productType}/${product.imageUrl}" />
                ${fn:toUpperCase(fn:substring(product.productType, 0, 1))}${fn:toLowerCase(fn:substring(product.productType, 1,fn:length(product.productType)))}</br>
                <c:choose>
                    <c:when test="${product.quantityInStock == 0}">
                        <span class="low-stock">Out of stock</span>
                    </c:when>             
                    <c:when test="${product.quantityInStock lt 10}">
                        <span class="low-stock">Only (${product.quantityInStock}) left</span>
                    </c:when>
                    <c:otherwise>
                        <span class="in-stock">In stock</span>                 
                    </c:otherwise>
                </c:choose>
                <br/>
                <p id="description">${product.description}</p>

                <t:reviews reviews="${product.reviewCollection}"/>
            </div>
            <t:product_quantity productId="${product.productId}" productPrice="${product.price}"/>
        </div>
    </div>
    <script type="text/javascript" src="${basePath}/javascript/product.js"></script>
</t:right_sidebar_page>
