<%-- 
    Document   : newproduct
    Created on : 07-Feb-2013, 19:56:38
    Author     : c.loughnane1@nuigalway.ie - 09101916
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<t:no_sidebar_page pageType="smaller-container">
    <jsp:body>
        <script src="${basePath}/javascript/handle-product-menu.js"></script>
        <script src="${basePath}/javascript/amend-product.js"></script>
        <script src="${basePath}/javascript/new-product.js"></script>
        <link rel="stylesheet" type="text/css" href="${basePath}/css/main-product-user.css">
        <div class="content-product-handling">
            <div id="content-header">
                <span class="button-amend-product">Amend</span>
                <span class="button-seperator">&nbsp;/&nbsp;</span>
                <span class="button-add-product">Add</span></h1>            
                <h1>Product Handling</h1>
            </div>
            <div class="wrapper-product-input-details">
                <form class="contact_form" method="post" name="contact_form">
                    <ul>
                        <li>
                            <h2>New Product</h2>
                            <span class="required_notification">* Denotes Required Field</span>
                        </li>
                        <li>
                            <label for="productname">Product name:</label>
                            <input type="text" name="productname" placeholder="product name" required />
                        </li>
                        <li>
                            <label for="price">Price:</label>
                            <input type="text" name="price" placeholder="0.00" required pattern="^\d+(\.\d{1,2})?$" />
                        </li>
                        <li>
                            <label for="description">Description:</label>
                            <textarea name="description" cols="4" rows="10" placeholder="description" required ></textarea>
                        </li>
                        <li>
                            <label for="quantity">Quantity:</label>
                            <input type="number" min="0" name="quantity" placeholder="quantity" required />
                        </li>
                        <li>
                            <label for="imageurl">Image:</label>
                            <input type="text" name="imageurl" placeholder="imageurl" placeholder="0" required />
                        </li>
                        <li>
                            <label for="producttype">Product Type:</label>
                            <select align="right" name="producttype" class="product-type-select-menu">
                                <c:forEach var="productTypeList" items="${productTypeList}">             
                                    <option value="${productTypeList}">${productTypeList}</option>
                                </c:forEach>
                                <option value="other">other</option>
                            </select>       
                        </li>
                        <li>
                            <label for=""></label>
                            <input type="text"  placeholder="other" name="other" class="enter-new-product-type"/>
                        </li>
                        <li>
                            <button class="submit" type="button">Add Product</button>
                        </li>
                    </ul>
                </form>
                <div class="add-success-notification"></div>
            </div> 
            <div class="wrapper-amend-product-input-details">
                <form class="contact_form" action="${basePath}/amendnewproductz" method="post" name="contact_form">
                    <ul>
                        <li>
                            <label for="selectproduct"><h2>Select Product</h2>:</label>
                            <select align="right" name="amend-producttype" class="product-name-select-menu" autofocus="true">
                                <c:forEach var="productList" items="${productList}">             
                                    <option value="${productList.productId}">${productList.name}</option>
                                </c:forEach>
                            </select> 
                        </li>
                        <li>
                            <h2>Amend Product</h2>
                            <span class="required_notification">* Denotes Required Field</span>
                        </li>
                        <li>
                            <label for="productname">Product name:</label>
                            <input class="amend-productname" type="text" name="amend-productname" placeholder="temp" required />
                        </li>
                        <li>
                            <label for="price">Price:</label>
                            <input type="text" name="amend-price" placeholder="0.00" required pattern="^\d+(\.\d{1,2})?$" />
                        </li>
                        <li>
                            <label for="description">Description:</label>
                            <textarea name="amend-description" cols="4" rows="10" placeholder="description" required ></textarea>
                        </li>
                        <li>
                            <label for="quantity">Quantity:</label>
                            <input type="number" min="0" name="amend-quantity" placeholder="quantity" required />
                        </li>
                        <li>
                            <label for="imageurl">Image:</label>
                            <input type="text" name="amend-imageurl" placeholder="imageurl" placeholder="0" required />
                        </li>
                        <li>
                            <label for="producttype">Product Type:</label>
                            <select align="right" name="amend-producttype-bottom" class="product-type-amend-select-menu">
                                <c:forEach var="productTypeList" items="${productTypeList}">             
                                    <option value="${productTypeList}">${productTypeList}</option>
                                </c:forEach>
                                <option value="other">other</option>
                            </select>       
                        </li>
                        <li>
                            <label for=""></label>
                            <input type="text"  placeholder="other" name="other" class="enter-amend-product-type"/>
                        </li>
                        <li>
                            <button class="amend-submit" type="button">Amend Product</button>
                        </li>
                    </ul>
                </form>
                <div class="amend-display-item"></div>
                <div class="amend-success-notification"></div>
            </div> 
        </div>
<!--
        <div class="upload-image">
            <form action="${pageContext.request.contextPath}/imageupload" method="post" 
                  enctype="multipart/form-data" 
                  name="productForm" id="productForm"><br><br>
                <table width="400px" align="center" border=0 
                       style="background-color:ffeeff;">
                    <tr>
                        <td align="center" colspan=2 style="
                            font-weight:bold;font-size:20pt;">
                            Image Details</td>
                    </tr><tr>
                        <td align="center" colspan=2>&nbsp;</td>
                    </tr><tr>
                        <td>Image Link: </td>
                        <td>
                            <input type="file" name="file" id="file">
                        <td>
                    </tr><tr>
                        <td></td>
                        <td><input type="submit" name="Submit" value="Submit"></td>
                    </tr><tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                </table>
            </form>
        </div>
-->
    </jsp:body>
</t:no_sidebar_page>   