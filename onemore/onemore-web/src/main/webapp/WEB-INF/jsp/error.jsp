<%-- 
    Document   : registrationError
    Created on : 22-Feb-2013, 13:14:13
    Author     : Shane Ryan <s.ryan25@nuigalway.ie>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:no_sidebar_page pageType="new-products-page">
    <jsp:body>
        <div id="container" class="clearfix">
            <div id="content-header">
                <h1>Registration Error</h1>
            </div>
            <div id="error-border">
                <p>Your registration for OneMore failed</p>
                
            </div>
        </div>
    </jsp:body>
</t:no_sidebar_page>   

