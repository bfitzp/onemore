<%-- 
    Document   : FAQ
    Created on : Feb 21, 2013, 8:34:08 PM
    Author     : Chris
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<t:no_sidebar_page pageType="smaller-container">
    <jsp:body>
        <script src="${basePath}/javascript/mydetails.js"></script>
        <link rel="stylesheet" type="text/css" href="${basePath}/css/products.css">
        <link rel="stylesheet" type="text/css" href="${basePath}/css/main-product-user.css">
        <div id="content-header">
            <h1>My Details</h1>
        </div>
        <div id="terms-and-conditions-border">
            <ul id="terms-and-conditions">
                <div id="wrapper-user-details">
                    <span>${userManager.user.email}</span><br><br>
                    <span><b>${userManager.user.firstName} ${userManager.user.lastName}</b></span><br><br>

<!--                    <div id="wrapper-address-all">
                        <c:forEach items="${userManager.user.addressCollection}"  var="address">
                            <div class="wrapper-address-box addressID${address.addressId}">
                                <span><b>${address.addressType}</b></span><br>
                                <span>${address.line1}</span><br>
                                <span>${address.line2}</span><br>
                                <span>${address.townCity}</span><br>
                                <span>${address.county}</span>
                            </div>
                        </c:forEach>
                    </div>-->
                    <ul id="addresses">      
                        <c:forEach items="${userManager.user.addressCollection}"  var="address">
                            <li> 
                                <label class="label-fix">
                                    <div class="details addressID${address.addressId}">
                                        <div>${address.line1}</div>
                                        <div>${address.line2}</div>
                                        <div>${address.townCity}</div>
                                        <div>${address.county}</div>
                                    </div>
                                </label> 
                            </li>
                        </c:forEach>                    
                    </ul>
                    <div class="wrapper-amend-product-input-details user-details-fix">
                        <form class="contact_form" action="${basePath}/******" method="post" name="contact_form">
                            <ul>
                                <li>
                                    <label for="selectproduct"><h2>Select Product</h2>:</label>
                                    <c:set var="count" value="0" scope="page" />
                                    <select align="right" name="amend-mydetails" class="mydetails-select-menu" autofocus="true">
                                        <c:forEach var="address" items="${userManager.user.addressCollection}">             
                                            <option value="${address.addressId}">${address.addressType} <c:if test="${address.addressType.equals('delivery')}"> ${count}</c:if> </option>
                                            <c:set var="count" value="${count + 1}" scope="page"/>
                                        </c:forEach>
                                    </select> 
                                </li>
                                <li>
                                    <h2>Amend My Details</h2>
                                    <span class="required_notification">* Denotes Required Field</span>
                                </li>
                                <li>
                                    <label for="email">Email</label>
                                    <input class="amend-productname" type="text" name="email" placeholder="john.doe@email.com" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" />
                                </li>
                                <li>
                                    <label for="firstName">First Name:</label>
                                    <input id="first-name" type="text" name="firstName" placeholder="John" required />
                                </li>
                                <li>
                                    <label for="description">Last Name:</label>
                                    <input id="last-name" type="text" name="lastName" placeholder="Doe" required>
                                </li>
                                <li>
                                    <label for="password">Password:</label>
                                    <input id="password" type="password" name="password" placeholder="" required >
                                </li>
                                <li>
                                    <label for="line-1">Line 1:</label>
                                    <input id="line-1" type="text" name="line1" placeholder="42 Answer Street" required >
                                </li>
                                <li>
                                    <label for="line-2">Line 2:</label>
                                    <input id="line-2" type="text" name="line2" placeholder="Anywhere" >
                                </li>
                                <li>
                                    <label for="town-city">Town/City:</label>
                                    <input id="town-city" type="text" name="townCity" placeholder="New Town" required >
                                </li>
                                <li>
                                    <label for="county">County:</label>
                                    <input id="county" type="text" name="county" placeholder="Dublin" required >
                                </li>                                
                                <li>
                                    <button class="amend-submit details-fix" type="button">Amend My Details</button>
                                    <button class="add-submit" type="button">Add Address</button>
                                </li>
                            </ul>
                        </form>
                        <div class="amend-display-item"></div>
                        <div class="amend-success-notification"></div>
                    </div> 

            </ul>
        </div>
    </jsp:body>
</t:no_sidebar_page>   
