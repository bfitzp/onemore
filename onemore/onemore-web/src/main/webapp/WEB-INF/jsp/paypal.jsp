<%-- 
    Document   : product.jsp
    Created on : 10-Feb-2013, 14:39:53
    Author     : Brian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:no_sidebar_page pageType="paypal-page">
    <div id="content">
        <div id="content-header">   
            <h1>Transferring to PayPal</h1>
        </div>
        <div id="content-inner">
            <h4>One moment ${userManager.user.firstName}, transferring you to PayPal to complete payment...</h4>
            <img src="${basePath}/image/ajax/ajax-loader.gif"/>
        </div>        
    </div>
</t:no_sidebar_page>
