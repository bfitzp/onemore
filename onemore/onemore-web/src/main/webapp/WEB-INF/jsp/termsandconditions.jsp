<%-- 
    Document   : FAQ
    Created on : Feb 21, 2013, 8:34:08 PM
    Author     : Chris
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<t:no_sidebar_page pageType="smaller-container">
    <div id="content-header">
        <h1>Terms and Conditions</h1>
    </div>
    <div id="terms-and-conditions-border">
        <ul id="terms-and-conditions">
            Age Limit
            It is illegal for anyone under the age of 18 years to purchase alcoholic beverages in the Ireland. Local law applies to all other countries. onemore requires that customers agree to supply proof of age on request and we further undertake that we shall not sell or deliver alcohol to anyone who is, or appears to be, under the age of 18. onemore is fully licensed and by placing an order you confirm that you are at least 18 years old If you are buying an alcoholic gift, the recipient must also be at least 18 years old.
            <br><br>

            Sales
            We are only permitted to conclude sales during the following hours:
            <br><br>
            Monday to Saturday 8am to 10pm
            Sunday 12.30pm to 10pm
            Any orders received out with these hours will not be deemed to be accepted and the sale concluded until the commencement of the next period of permitted hours. onemore will acknowledge an order via e-mail within 24 hours of receipt.
            <br><br>
            Prices
            We are continually working to ensure that the prices of our products are amongst the lowest that can be found in the Ireland. We cannot however guarantee that you will not be able to find the product cheaper somewhere else in the country. Our site prices on all our products and services are clear and easily found. The prices will always be shown in pounds sterling and include Value Added Tax (currently 23%) Any delivery charges are supplementary.
            <br><br>

            Ways of Paying
            You are able to pay with major credit cards, switch cards and solo cards. We will also accept payment by cheque, where upon receipt and clearance, orders will be dispatched.
            <br><br>
            Availability of Products
            Despite our best efforts to maintain fully accurate stock records it may sometimes happen that we are unable to fulfil an order. In this case, customers will be contacted directly to make alternative arrangements.
            <br><br>

            Minimum Orders
            When buying beer, the minimum order is one case. A case normally consists of 24 bottles or cans, in some cases it is 20 - this is clearly marked in the product descriptions. With wines the minimum order is one case (usually 6 bottles). Spirits and champagnes can be bought by the single bottle.
            <br><br>

            High Volume Orders
            If you are ordering over 30 cases in any one purchase, please contact us so we can discuss special delivery options.
            <br><br>

            Delivery
            Next day deliveries will be made between 08:30 and 17:30. You don't have to sign for the delivery, so if you’re not going to be in between 08:30 and 17:30, you can simply add detailed instructions on where the parcel/s can be safely left, such as “leave with neighbour at No. 54” or "Please leave in garden shed behind house", and we shall leave it for them. We endeavour to get all of our orders delivered on the next working day, however because we use third parties to carry out this function we cannot guarantee this. However, late deliveries are rare.
            <br><br>
            Delivery Charges
            The delivery charges are based on a package system. One package can contain either: up to 2 cases of beer, alcopops or soft drinks (48 cans or bottles); or up to 12 bottles of wines, spirits or champagne; or you can have a mixed package with one case of beer, alcopops or soft drinks and up to 6 bottles of wines, spirits or champagne.
            <br><br>
            onemore strives to keep the prices of its products as low as possible. Unlike our competitors, we do not subsidise the delivery cost by inflating the price of the product. Nor do we make any money on the delivery price. We simply pass on the cost which we are charged by Business Post. On a large order, the delivery charge may look high - however, the overall cost of the order is still likely to be lower than buying the equivalent products from the high street. AND we deliver it, so that you don't have to carry it home!
            <br><br>
            The exact delivery charges are shown in the checkout process. Delivery charges are dependant upon the postcode of the order recipient and the size of the order. The postcode of the recipient will automatically fall into one of the following delivery categories:
            <br><br>

            Quality Guarantee
            We will provide an exchange or a refund if any products turn out to be faulty or if goods are different from those ordered and customers contact us within 7 days of receipt of the goods, only where the products have not been opened or damaged.
            <br><br>

            Cancellations
            Goods that are no longer wanted must be returned within 7 days of the original delivery of your order. Any costs associated with returning unwanted goods are at your expense - we recommend that you contact us about the shipping to help reduce the cost. onemore can only credit you upon safe receipt and inspection of the products into our warehouse. We shall accept returns and give full refunds only where the products have not been opened or damaged.
            <br><br>
            Any refund will be provided as soon as possible and at the latest within 30 days of us receiving your returned order. Unfortunately we are not able to reimburse you the initial delivery charge when you have exercised your right to cancel your order.
            <br><br>

            Claims for breakages
            Upon receipt of your order, you should carefully inspect it to make sure that it is intact and in full. The courier must be informed of breakages or shortages at the time of delivery. We will not accept any claims for breakages or shortages unless they are stated on the couriers documentation. We must have confirmation of these breakages or shortages within 7 days of the delivery (contact us).
            <br><br>

            Damage in Transit and Returns
            Although we believe that you will be pleased with your order, there may be occasions where you feel it necessary to return an order. We endeavour to keep this process as simple as possible. We accept returns and give refunds where the goods have not been opened or damaged. You may chose to return all or part of your order (beer returns must be in intact and unopened cases). Shipping costs back to us are normally at your expense (except where the goods do not comply with statutory requirements) and we can only credit you upon safe receipt and inspection of the goods into our warehouse. All refunds will be paid as soon as possible within 30 days of you contacting us provided our courier has picked up the beer.
            <br><br>
            If bottles are broken or damaged in transit to you, the damaged goods must be returned to us. Shipping costs for the damaged portion of the delivery back to us is at our expense – customers must contact us to arrange the return shipping.
            <br><br>
            onemore can only credit customers for the items that have been damaged upon safe receipt of the products into our hands.
            <br><br>

            Handling Complaints
            We deal with complaints effectively, in a fair, confidential, and speedy manner. We endeavour to respond to all complaints within five working days.
            <br><br>

            Cookies
            Cookies are small pieces of information that are stored by your browser on your computer's hard drive. Our cookies do not contain any personally identifying information, but they do enable us to provide features such as storing items in 'Your Fridge' between visits. Most Web browsers automatically accept cookies, but you can usually change your browser to prevent that.
            <br><br>

            Web site Security
            Don’t be nervous about shopping online with onemore. All of our transactions take in a secure environment and we follow strict security procedures in the storage and disclosure of information that you have given us, to prevent unauthorised access. We operate under the rules of the Which Web Trader scheme.
            <br><br>
            The web site registers each individual with their e-mail address and a password - both of which you provide. You are entirely responsible if you do not maintain the confidentiality of your ID and password. You are entirely responsible for all activities that occur under your user ID and password. If there has been any unauthorised use of your user ID or any breach of security known to you, you must inform us immediately.

            <br><br>
            Data Protection/Privacy Policy
            <br><br>

            When you place an order, we need to obtain: your name (and delivery name if different), email address, billing address (and delivery address if different), contact phone number, credit card number and expiry date (switch cards also require an issue number). This information enables us to accurately process and fulfil your order, and to confirm your order by email. We may record which products you are interested in and which products you purchase.
            <br><br>
            When you register on our site, we will ask for your name and email address so we can notify you about promotional activities.
            <br><br>
            When you enter competitions we will ask for your name and email address so we can notify winners.
            <br><br>
            We monitor customer traffic patterns and site usage to help us further develop the web site.
            <br><br>
            We are committed to protecting your privacy. We use the information we collect about you to process orders and to provide a more personalised shopping experience. We may also use it to tell you about changes in our services or about special offers we think you'll find valuable. Please advise us if you wish to stop receiving promotional material from us.
            <br><br>
            We do not sell, trade or rent your personal information to others. We may choose to do so in the future with appropriate third parties, but you can tell us not to at any stage. By using our web site you consent to the collection of this information. Any changes to our privacy will be advertised on this page so you are always clear on what information we collect, how we collect it and to whom we disclose it.
            <br><br>
            If you have any questions regarding our privacy policy, please don’t hesitate to contact us at privacy@onemore.com
            <br><br>


            Governing Law
            Your use of this web site and any purchase by you on this web site will be governed by Scottish Law and will be deemed to have occurred in the United Kingdom.
            <br><br>

            Termination
            onemore reserves the right to terminate immediately a customers ability to use any of our Internet services if the conduct is found to be inconsistent with these conditions.
            <br><br>

            Statutory Rights
            The statutory rights of our customers are not affected by the terms of our service.
            <br><br>
            Disclaimer
            To the fullest extent permitted at law, onemore is providing this web site and its contents on an "as is" basis and makes no (and expressly disclaims all) representations or warranties of any kind with respect to this web site or its contents including, without limitation, warranties of merchantability and fitness for a particular purpose. In addition, onemore does not represent or warrant that the information accessible via this web site is accurate, complete or current. Price and availability information is subject to change without notice. Except as specifically stated on this Web site, to the fullest extent permitted at law, neither onemore nor any of its affiliates, directors, employees or other representatives will be liable for damages arising out of or in connection with the use of this Web site. This is a comprehensive limitation of liability that applies to all damages of any kind, including (without limitation) compensatory, direct, indirect or consequential damages, loss of data, income or profit, loss of or damage to property and claims of third parties. For the avoidance of doubt, onemore does not limit its liability for death or personal injury to the extent only that it arises as a result of the negligence of onemore, its affiliates, directors, employees or other representatives.
            <br><br>
            Privacy Policy
            At onemore.com we are committed to protecting your privacy. We use the information we collect about you to process orders and to provide a more personalised shopping experience. Please read on for more details about our privacy policy.
            <br><br>
            What information do we collect? How do we use it?
            <br><br>
            When you order, we need to know your name, e-mail address, delivery address, credit or debit card number and the card's expiry date. This allows us to process and fulfil your orders and to notify you of the status of your order.
            We also ask for your telephone number which enables us to contact you urgently if there is a problem with your order. For some international deliveries this number may be given to our couriers.
            When you submit a customer review, we also ask for your e-mail address, although you can choose not to have your e-mail address displayed with your review.
            When you enter a contest or other promotional feature, we may ask for your name, address and e-mail address so we can administer the contest and notify winners.
            We may also use the information we collect to occasionally notify you about important functionality changes to the Web site, new onemore.com services and special offers we think you'll find valuable. If you would rather not receive this information, please send an e-mail to privacy@onemore.com. Make sure you change your preferences for each account you hold or e-mail address you have given us.
        </ul>
    </div>
</t:no_sidebar_page>   
