<%-- 
    Document   : shopping
    Created on : 22-Jan-2013, 08:59:06
    Author     : Brian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:standard_page>
    <jsp:body>
        <div id="left">
            <form action="${pageContext.request.contextPath}/shopping" method="POST">
                <h3>Guinness</h3>
                <image src="image/product/beer/guinness.png"/>
                <div><b>Price: </b>€<span id="price-1">2.99</div>
                <span>Quantity:</span><input type="text" name="quantity" value="1" class="product-quantity" id="pquantity-1"/>
                <input type ="submit" value="Add to cart" name="1" class="product" id="pid-1"/>
            </form>
            <form action="${pageContext.request.contextPath}/shopping" method="POST">
                <h3>Whiskey</h3>
                <image src="image/product/vodka/smirnoff.jpg"/>
                <div><b>Price: </b>€<span id="price-2">12.99</div>
                <span>Quantity:</span><input type="text" name="quantity" value="1" class="product-quantity" id="pquantity-2"/>
                <input type ="submit" value="Add to cart" name="2" class="product" id="pid-2"/>
            </form>                    
        </div>
        <div id="right">
        </div>
    </jsp:body>
</t:standard_page>    



