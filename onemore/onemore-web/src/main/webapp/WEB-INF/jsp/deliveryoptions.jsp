<%-- 
    Document   : FAQ
    Created on : Feb 21, 2013, 8:34:08 PM
    Author     : Chris
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<t:no_sidebar_page pageType="smaller-container">
    <jsp:body>
        <div id="content-header">
            <h1>Delivery Options</h1>
        </div>
        <div id="delivery-options-border">
            <ul id="delivery-options">

                <b>Delivery</b>
                <br><br>
                onemore has teamed up with City Link, a leading national distribution company that specialises in the shipment of alcohol, to offer a next working day delivery service in the mainland Ireland for orders placed before 12:00.
                <br><br>
                Next day deliveries will be made between 08:30 and 17:30. You don't have to sign for the delivery, so if you’re not going to be in between 08:30 and 17:30, you can simply add detailed instructions on where the parcel/s can be safely left, such as “leave with neighbour at No. 54” or "Please leave in garden shed behind house", and we shall leave it for them. We endeavour to get all of our orders delivered on the next working day, however because we use third parties to carry out this function we cannot guarantee this.<br><br>
                However, late deliveries are rare.
                <br><br>
                <b>Delivery Charges</b>
                <br><br>
                The delivery charges are based on a package system. One package can contain either: up to 2 cases of beer, alcopops or soft drinks (48 cans or bottles); or up to 12 bottles of wines, spirits or champagne; or you can have a mixed package with one case of beer, alcopops or soft drinks and up to 6 bottles of wines, spirits or champagne.
                <br><br>
                onemore strives to keep the prices of its products as low as possible. Unlike our competitors, we do not subsidise the delivery cost by inflating the price of the product. Nor do we make any money on the delivery price. We simply pass on the cost which we are charged by Business Post. On a large order, the delivery charge may look high - however, the overall cost of the order is still likely to be lower than buying the equivalent products from the high street. AND we deliver it, so that you don't have to carry it home!
                <br><br>
                The exact delivery charges are shown in the checkout process. Delivery charges are dependant upon the postcode of the order recipient and the size of the order. The postcode of the recipient will automatically fall into one of the following delivery categories:
                <br><br>
                €9.50 per package. Order to arrive the next working day.Applies to all Ireland postcodes, except to those mentioned in the following sections 2, 3 and 4.
                <br><br>
                €19.00 per package. Order to arrive within two working days.

            </ul>
        </div>
    </jsp:body>
</t:no_sidebar_page>   
