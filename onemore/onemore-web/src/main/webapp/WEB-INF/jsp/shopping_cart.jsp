<%-- 
    Document   : shopping_cart
    Created on : 07-Feb-2013, 19:53:34
    Author     : Brian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:listing_page>
    <jsp:body>
        <style type="text/css">@import url("${basePath}/css/shopping_cart.css");</style>
        <div id="shopping-cart">
            <h2>Shopping Cart</h2>
            <ul id="product-list">
                <c:forEach items="${sessionShoppingCart.orderProducts}" var="orderProduct">
                    <t:shopping_cart_product orderProduct="${orderProduct}"/>
                </c:forEach>
            </ul>
        </div>
    </jsp:body>
</t:listing_page>