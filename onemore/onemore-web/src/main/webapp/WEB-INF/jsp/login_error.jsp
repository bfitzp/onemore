<%-- 
    Document   : login
    Created on : 07-Feb-2013, 13:47:11
    Author     : Brian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:no_sidebar_page pageType="login-page">
    <div id="content-header">
        <h1>Login to OneMore</h1>
    </div>
    <form class="wrapper-register" action="j_security_check" method="POST">
        <h3>Incorrect username or password</h3>
        <label for="username">Username</label><input id="username" type="email" name="j_username" placeholder="john.doe@email.com" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"><br>
        <label for="password">Password</label><input id="password" type="password" name="j_password" placeholder="******" required >
        <input  id="submit" type="submit" value="Login">
        <div>
            Not a member yet? <a href="${basePath}/register"><br/>Register with OneMore</a>
        </div>        
    </form>
</t:no_sidebar_page>
