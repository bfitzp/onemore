<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:no_sidebar_page pageType="checkout-page">
    <link rel="stylesheet" type="text/css" href="${basePath}/css/products.css">
    <div id="content">
        <div id="content-header">
            <h1>Checkout</h1>       
        </div>
        <div id="content-inner">
            <form id="adddreses">
                <h2>Select a delivery address</h2>
                <ul id="addresses">
                    <c:forEach items="${userManager.user.addressCollection}"  var="address">
                        <t:checkout_address address="${address}"/>
                    </c:forEach>                    
                </ul>
            </form>
            <t:checkout_shopping_cart/>
            <form action="paypal" method="post">
                <input type="hidden" value="checkout"/>
                <input name="payment" id="checkout-button" class="standard-button" type="submit" value="Proceed to payment"/>
            </form>
        </div>
    <script type="text/javascript" src="${basePath}/javascript/checkout.js"></script>
</t:no_sidebar_page>
