<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:left_right_sidebar_page pageType="products-page">
    <link rel="stylesheet" type="text/css" href="${basePath}/css/products.css">
    <div id="content">
        <div id="content-header">
            <h1>Products</h1>
            <form id="content-header-selects">
                <label for="sort-products-select">Sort by</label>
                <select id="sort-products-select" name="sort_products_select">
                    <option value="price_low_to_high">Price: Low to High</option>
                    <option value="price_high_to_low">Price: High to Low</option>
                    <option value="name_low_to_high">Name: Low to High</option>
                    <option value="name_high_to_low">Name: High to Low</option>                    
                    <option value="rating_low_to_high">Rating: Low to High</option>
                    <option value="rating_high_to_low">Rating: High to Low</option>
                </select>
                <label for="products-per-page-select" name="products-per-page-select">Show</label>
                <select id="products-per-page-select">
                    <option value="5">5 per page</option>
                    <option value="10">10 per page</option>
                    <option value="20">20 per page</option>
                </select>                
            </form>            
        </div>
        <ul id="products-list">
            <c:forEach items="${productManager.products}"  var="product">
                <t:product product="${product}"/>
            </c:forEach>
            <t:products_pagination/>
        </ul>
    </div>
    <script type="text/javascript" src="${basePath}/javascript/product.js"></script>
</t:left_right_sidebar_page>
