<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:if test="${not empty noProducts}">
    <div id="no-products">There are no products matching your selection.</div>
</c:if>
<c:forEach items="${productManager.products}"  var="product">
    <t:product product="${product}"/>
</c:forEach>
<t:products_pagination/>