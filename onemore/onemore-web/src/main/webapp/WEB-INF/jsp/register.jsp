<%-- 
    Document   : login
    Created on : 07-Feb-2013, 13:47:11
    Author     : Brian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:no_sidebar_page pageType="register-page">
    <script>
        function checkEmail(input) {
            if (input.value != document.getElementById('email').value) {
                input.setCustomValidity('The two email addresses must match.');
            } else {
                // input is valid -- reset the error message
                input.setCustomValidity('');
            }
        }

        function checkPassword(input) {
            if (input.value != document.getElementById('password').value) {
                input.setCustomValidity('The two passwords must match.');
            } else {
                // input is valid -- reset the error message
                input.setCustomValidity('');
            }
        }
    </script>
    <div id="content-header">
        <h1>Register with OneMore</h1>
    </div>
    <form id="content-inner" class="wrapper-register" action="${basePath}/user/register" method="POST">
        <div id="left">
            <h2>Login details</h2>
            <label for="email">Email</label><input id="email" type="text" name="email" placeholder="john.doe@email.com" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}"><div class="message">${violation_email}</div>
            <label for="confirm-email">Confirm email</label><input id="confirm-email" type="text" name="confirmEmail" placeholder="john.doe@email.com" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" oninput="checkEmail(this)"><div class="message">${violation_confirmEmail}</div>
            <label for="password">Password</label><input id="password" type="password" name="password" placeholder="******" required ><div class="message">${violation_password}</div>
            <label for="confirm-password">Password</label><input id="confirm-password" type="password" name="confirmPassword" placeholder="******" required oninput="checkPassword(this)"><div class="message">${violation_confirmPassword}</div>
        </div>
        <div id="right">
            <h2>Personal details</h2>
            <label for="first-name">First name</label><input id="first-name" type="text" name="firstName" placeholder="John" required><div class="message">${violation_firstName}</div>
            <label for="last-name">Last name</label><input id="last-name" type="text" name="lastName" placeholder="Doe" required><div class="message">${violation_lastName}</div>      
            <label for="line-1">Line 1</label><input id="line-1" type="text" name="line1" placeholder="42 Answer Street" required ><div class="message">${violation_line1}</div>
            <label for="line-2">Line 2</label><input id="line-2" type="text" name="line2" placeholder="Anywhere" ><div class="message">${violation_line2}</div>
            <label for="town-city">Town/City</label><input id="password" type="text" name="townCity" placeholder="New Town" required ><div class="message">${violation_townCity}</div>
            <label for="county">County</label><input id="password" type="text" name="county" placeholder="Dublin" required ><div class="message">${violation_county}</div>
            <input  id="register-button" class="standard-button" type="submit" value="Register">
        </div>
    </form>
    <!--<script type="text/javascript" src="${basePath}/javascript/register.js"></script>-->
</t:no_sidebar_page>
