<%-- 
    Document   : FAQ
    Created on : Feb 21, 2013, 8:34:08 PM
    Author     : Chris
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<t:no_sidebar_page pageType="smaller-container">
    <jsp:body>
        <script src="${basePath}/javascript/contact.js"></script>
        <link rel="stylesheet" type="text/css" href="${basePath}/css/main-product-user.css">
        <div id="content-header">
            <h1>Contact</h1>
        </div>
        <div id="faq-border">
            <ul id="faq-list">
                <div class="wrapper-product-input-details contact-form-fix">
                    <form class="contact_form" method="post" name="contact_form">
                        <ul>
                            <li>
                                <h2>Send a message</h2>
                                <span class="required_notification">* Denotes Required Field</span>
                            </li>
                            <li>
                                <label for="productname">Name:</label>
                                <input type="text" name="name" placeholder="joe bloggs" required />
                            </li>
                            <li>
                                <label for="price">Email:</label>
                                <input type="email" name="email" placeholder="joe.bloggs@gmail.com" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" />
                            </li>
                            <li>
                                <label for="description">Message:</label>
                                <textarea class="contant-form-textarea" name="description" cols="10" rows="25" maxlength='400' placeholder="Hi onemore..." required ></textarea>
                            </li>
                            <li>
                                <button class="contact-submit" type="button">Send Message</button>
                            </li>
                        </ul>
                    </form>
                    <div class="contact-success-notification"></div>
                </div>
            </ul>
        </div>
    </jsp:body>
</t:no_sidebar_page>   
