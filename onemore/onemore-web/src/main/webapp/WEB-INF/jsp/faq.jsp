<%-- 
    Document   : FAQ
    Created on : Feb 21, 2013, 8:34:08 PM
    Author     : Chris
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<t:no_sidebar_page pageType="smaller-container">
    <jsp:body>
        <div id="content-header">
            <h1>FAQ</h1>
        </div>
        <div id="faq-border">
            <ul id="faq-list">
                <b>1. Who should I contact regarding any aspect of my order from this site?</b>
                <br><br>
                Any queries regarding orders from this site should be directed to onemore.
                <br><br>
                onemore
                <br>
                NUIG IT314
                <br>
                Sleepy Desk
                <br>
                Galway
                <br><br>

                E mail: help@onemore.com
                <br><br>
                091 ASKAWAY (for delivery queries)
                <br>
                091 MORELOLLY (for marketing queries)
                <br><br>
                Company registration: 666
                Vat Registration: 762 5643 44

                <br><br>

                <b>2. Is my transaction secure?</b>
                <br><br>
                Our Promise
                Making you, the customer happy is our main intention. So, if you have concerns about using our service, we wish to put them to rest now with our main promises. If you are still not happy, we would like to know why and put it right.
                <br><br>
                Total Security
                We have been certified by Trustwise and Which? (tbc) as an A-class internet business. This means that, when we say you can trust us to look after your personal details, we mean it.
                Safe Delivery - even if you are not there
                It does not matter to us if you, or whoever is due to receive your order cannot be there in person when we deliver. We can follow any special delivery instructions if required - please leave a clear note telling us where we can safely leave your order and we will do just that.
                <br><br>

                <b>3. Can I order if I live outside the Ireland</b>
                <br><br>
                You can place an order from anywhere in the world as long as the recipient of that order lives in the Ireland.
                <br><br>


                <b>4. How much does delivery cost?</b>
                <br><br>
                Next day deliveries will be made between 08:30 and 17:30. You don't have to sign for the delivery, so if you’re not going to be in between 08:30 and 17:30, you can simply add detailed instructions on where the parcel/s can be safely left, such as “leave with neighbour at No. 54” or "Please leave in garden shed behind house", and we shall leave it for them.
                <br><br>
                The delivery charges are based on a package system. One package can contain either: up to 2 cases of beer, alcopops or soft drinks (48 cans or bottles); or up to 12 bottles of wines, spirits or champagne; or you can have a mixed package with one case of beer, alcopops or soft drinks and up to 6 bottles of wines, spirits or champagne.
                <br><br>
                onemore strives to keep the prices of its products as low as possible. Unlike our competitors, we do not subsidise the delivery cost by inflating the price of the product. Nor do we make any money on the delivery price. We simply pass on the cost which we are charged by Business Post. On a large order, the delivery charge may look high - however, the overall cost of the order is still likely to be lower than buying the equivalent products from the high street. AND we deliver it, so that you don't have to carry it home!
                <br><br>
                The exact delivery charges are shown in the checkout process. Delivery charges are dependant upon the postcode of the order recipient and the size of the order. The postcode of the recipient will automatically fall into one of the following delivery categories:
                <br><br>
                €5.99 per package. Order to arrive the next working day.Applies to all Ireland postcodes, except to those mentioned in the following sections 2, 3 and 4.
                €15.00 per package. Order to arrive within two working days.
                <br><br>

                <b>5. What is your privacy policy?</b>
                <br><br>
                When you place an order, we need to obtain: your name (and delivery name if different), email address, billing address (and delivery address if different), contact phone number, credit card number and expiry date (switch cards also require an issue number). This information enables us to accurately process and fulfil your order, and to confirm your order by email. We may record which products you are interested in and which products you purchase.
                <br><br>
                When you register on our site, we will ask for your name and email address so we can notify you about promotional activities.
                <br><br>
                When you enter competitions we will ask for your name and email address so we can notify winners.
                <br><br>
                We monitor customer traffic patterns and site usage to help us further develop the web site.
                <br><br>
                We are committed to protecting your privacy. We use the information we collect about you to process orders and to provide a more personalised shopping experience. We may also use it to tell you about changes in our services or about special offers we think you'll find valuable. Please advise us if you wish to stop receiving promotional material from us.
                <br><br>
                We do not sell, trade or rent your personal information to others. We may choose to do so in the future with appropriate third parties, but you can tell us not to at any stage.
                <br><br>
                By using our web site you consent to the collection of this information. Any changes to our privacy will be advertised on this page so you are always clear on what information we collect, how we collect it and to whom we disclose it.
                <br><br>
                If you have any questions regarding our privacy policy, please don’t hesitate to contact us at privacy@onemore.com
            </ul>
        </div>
    </jsp:body>
</t:no_sidebar_page>   
