<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Template the navbar" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="navbar">
    <ul>
        <li><a href="${basePath}/products">Home</a></li>
        <li><a href="${basePath}/products">Products</a></li>
        <li><a href="${basePath}/termsandconditions">Terms</a></li>
        <li><a href="${basePath}/faq">FAQ</a></li>
        <li><a href="${basePath}/contact">Contact</a></li>

        <c:choose>
            <c:when test="${empty userManager.user.userId}">
                <li><a href="${basePath}/register">Register</a></li>
            </c:when>

        </c:choose>
    </ul>
</div>