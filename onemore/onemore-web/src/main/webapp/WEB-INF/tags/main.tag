<%@tag description="Template for page with left and right sidebars" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@attribute name="pageType" required="true"%>
<%@attribute name="bodyId" required="true"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>One More</title>
        <link rel="icon" type="image/vnd.microsoft.icon" href="${basePath}/image/icon/favicon<%= (int) (Math.random() * 3)%>.ico" />
        <link rel="stylesheet" type="text/css" href="${basePath}/css/main.css">
        <link rel="stylesheet" type="text/css" href="${basePath}/css/products.css">
        <link rel="stylesheet" type="text/css" href="${basePath}/css/login_register.css">      
        <link rel="stylesheet" type="text/css" href="${basePath}/css/rateit.css">
        <link rel="stylesheet" type="text/css" href="${basePath}/css/report.css">
        <link rel="stylesheet" type="text/css" href="${basePath}/css/checkout.css">
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
        <script type="text/javascript" src="${basePath}/javascript/jquery.rateit.min.js"></script>
        <script type="text/javascript" src="${basePath}/javascript/review.js"></script>
        <!-- Start of Facebook Meta Tags -->
        <meta property="og:title" content="onemore" />
        <meta property="og:type" content="blog" />
        <meta property="og:url" content="http://danu2.it.nuigalway.ie:8071/onemore-web/" />
        <meta property="og:image" content="http://portfolio.chrisloughnane.net/images/onemore.png" />
        <meta property="og:site_name" content="onemore" />
        <meta property="fb:admins" content="653128111" />
        <meta property="og:description" content="JEE Group project. Brian Fitzpatrick, Shane Ryan and I put this together in January and February 2013 for our 3rd year Software Engineering Course." /> 
        <!-- End of Facebook Meta Tags -->
    </head>
    <body id="${bodyId}" class="${pageType}">
        <div id="wrapper">
            <t:header/>
            <t:navbar/>
            <div id="container" class="clearfix">
                <jsp:doBody/>
            </div>
        </div>
        <t:footer/>
        <!-- Start of StatCounter Code for Default Guide -->
            <script type="text/javascript">
            var sc_project=8788627; 
            var sc_invisible=0; 
            var sc_security="2170a843"; 
            var scJsHost = (("https:" == document.location.protocol) ?
            "https://secure." : "http://www.");
            document.write("<sc"+"ript type='text/javascript' src='" +
            scJsHost+
            "statcounter.com/counter/counter.js'></"+"script>");
            </script>
            <noscript><div class="statcounter"><a title="web stats"
            href="http://statcounter.com/" target="_blank"><img
            class="statcounter"
            src="http://c.statcounter.com/8788627/0/2170a843/0/"
            alt="web stats"></a></div></noscript>
        <!-- End of StatCounter Code for Default Guide -->
    </body>
</html>