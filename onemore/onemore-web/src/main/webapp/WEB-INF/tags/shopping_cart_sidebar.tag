<%@tag description="Template for the shopping cart sidebar on the listing pages" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="shopping-cart-sidebar">
    <div id="shopping-cart">
        <style type="text/css">@import url("${basePath}/css/shopping_cart_sidebar.css");</style>
        <h2>Shopping Cart</h2>
        <table class="shopping-cart-table">
            <thead>
                <tr class="header">
                    <th colspan="3" class="quantity" id="quantity">Quantity</th>
                    <th class="product" id="product">Product</th>
                    <th class="price" id="price">Price</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:set var="totalPrice" value="0"/>
                <c:forEach items="${orderManager.order.orderProductCollection}" var="orderProduct">
                    <c:set var="product" value="${orderProduct.product}"/>
                    <c:set var="totalPrice" value="${totalPrice + product.price * orderProduct.quantity}"/>
                    <tr id="product-id-${product.productId}">
                        <td class="quantity-cell"><div class="decrease-quantity" title="decrease-quantity">-</div></td>
                        <td class="quantity">${orderProduct.quantity}</td>
                        <td class="quantity-cell"><div class="increase-quantity" title="increase-quantity">+</div></td>
                        <td>${product.name}</td>
                        <td>€<span class="price"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${product.price}"/></span></td>
                        <td class="remove" title="Remove item">X</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <c:if test="${fn:length(orderManager.order.orderProductCollection) gt 0}">
            <div id="shopping-cart-total">
                Total: €<span><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalPrice}"/></span>
            </div>
            <a id="checkout-button" class="standard-button" href="${basePath}/checkout">Checkout</a>
        </c:if>
    </div>
</div>