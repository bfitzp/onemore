<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@attribute name="pageType" required="true"%>

<t:main bodyId="no-sidebar-page" pageType="${pageType}">
    <jsp:doBody/>
</t:main>      