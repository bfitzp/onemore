<%@tag description="Template shopping cart section of the checkout" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<form id="checkout-shopping-cart">
    <style type="text/css">@import url("${basePath}/css/shopping_cart_sidebar.css");</style>
    <h2>Shopping Cart</h2>
    <table class="shopping-cart-table">
        <thead>
            <tr class="header">
                <th class="product" id="product">Product</th>                
                <th colspan="3" class="quantity" id="quantity">Quantity</th>
                <th class="price" id="price">Price</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:set var="totalPrice" value="0"/>
            <c:forEach items="${orderManager.order.orderProductCollection}" var="orderProduct">
                <c:set var="product" value="${orderProduct.product}"/>
                <c:set var="totalPrice" value="${totalPrice + product.price * orderProduct.quantity}"/>
                <tr id="product-id-${product.productId}">
                    <td class="product">
                        <div class="product-image">
                            <a href="${basePath}/products/${product.name}_${product.productId}">
                                <img src="${basePath}/${productImagePath}/${product.productType}/${product.imageUrl}"/>
                            </a>
                        </div>        
                        <a class="title" href="${basePath}/products/${product.name}_${product.productId}">
                            ${product.name}
                        </a>
                        ${fn:toUpperCase(fn:substring(product.productType, 0, 1))}${fn:toLowerCase(fn:substring(product.productType, 1,fn:length(product.productType)))}</br>
                        <c:choose>
                            <c:when test="${product.quantityInStock == 0}">
                                <div class="low-stock">Out of stock</div>
                            </c:when>             
                            <c:when test="${product.quantityInStock lt 10}">
                                <div class="low-stock">Only (${product.quantityInStock}) left</div>
                            </c:when>
                            <c:otherwise>
                                <div class="in-stock">In stock</div>                 
                            </c:otherwise>
                        </c:choose>                        
                    </td>                
                    <td class="quantity-cell"><div class="decrease-quantity" title="decrease-quantity">-</div></td>
                    <td class="quantity">${orderProduct.quantity}</td>
                    <td class="quantity-cell"><div class="increase-quantity" title="increase-quantity">+</div></td>
                    <td class="price">€<span class="price"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${product.price}"/></span></td>
                    <td class="remove" title="Remove item">X</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <div id="shopping-cart-total">
        Total: €<span><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalPrice}"/></span>
    </div>            
</form>