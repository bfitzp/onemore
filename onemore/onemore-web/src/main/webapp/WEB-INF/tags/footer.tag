<%@tag description="Template the footer" pageEncoding="UTF-8"%>

<div id="footer">
    <span><h3><a href="${basePath}/products">Home</a></h3></span> | 
    <span><h3><a href="${basePath}/deliveryoptions">Delivery options</a></h3></span> | 
    <span><h3><a href="${basePath}/contact">Contact onemore.com</a></h3></span> | 
    <span><h3><a href="${basePath}/termsandconditions">Terms & conditions</span></h3></a> | 
    <span><h3><a href="${basePath}/faq">FAQ</span></h3></a>
</div>