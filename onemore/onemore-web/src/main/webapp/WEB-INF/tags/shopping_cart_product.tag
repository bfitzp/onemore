<%@tag description="Template for products on the shopping cart page" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@tag import="com.onemore.entity.OrderProduct" %>
<%@attribute name="orderProduct" required="true" type="com.onemore.entity.OrderProduct"%>

<c:set var="product" value="${orderProduct.product}"/>
<div class="shopping-cart-row">
    <div class="product-image">
        <a href="/products?type=${product.productType}&id=${product.productId}">
            <img src="${productImagePath}/beer/guinness.png" height="90" width="90"/>
        </a>
    </div>
    <div class="details">
        <div class="title">
            <a href="/products?type=${product.productType}&id=${product.productId}">${product.name}</a></div>
        <div class=${product.quantityInStock == 0 ? "not-in-stock" : "in-stock"}>In stock</div>
        <div class="price">${product.price}</div>
        <div class="quantity">
            <input type="text" size="3" maxlength="3" value="${orderProduct.quantity}" name="quantity-${product.productId}"/>
        </div>
    </div>
</div>