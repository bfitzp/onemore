<%@tag description="Template for product quantity widget" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@tag import="com.onemore.entity.Product" %>
<%@attribute name="productId" required="true"%>
<%@attribute name="productPrice" required="true"%>

<div class="quantity">
    <div class="price">€<span><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${productPrice}"/></span></div>
    <label for="quantity-${productId}">Quantity</label>
    <form>        
        <div class="decrease-quantity" title="Decrease quantity">-</div>
        <input type="text" size="1" maxlength="3" value="1" name="quantity-${productId}"
               id="quantity-${productId}" class="textbox"/>
        <div class="increase-quantity" title="Increase quantity">+</div>
        <input type="submit" value="Add to cart" name="add_cart" id="add-to-cart-${productId}" class="standard-button add-to-cart">
        <input type="submit" value="Add to wishlist" name="add_wishlist" id="add-to-wishlist-${productId}" class="standard-button add-to-wishlist">
    </form>
</div>