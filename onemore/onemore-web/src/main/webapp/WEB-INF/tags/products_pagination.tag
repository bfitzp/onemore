<%@tag description="Template for review on individual product pages" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${empty noProducts}">
    <div id="showing-results">Showing ${paginationRange} of ${paginationTotal} results</div>
    <ul id="pagination">
        <c:forEach items="${productManager.pagination}" var="pagination">
            <li id="${pagination.id}"
                <c:if test="${pagination.selected == true}">
                    class="selected"
                </c:if>>${pagination.text}</li>      
        </c:forEach> 
    </ul>
</c:if>