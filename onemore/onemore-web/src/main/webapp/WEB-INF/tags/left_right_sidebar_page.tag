<%@tag description="Template for a page with left and right sidebars" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@attribute name="pageType" required="true"%>

<t:main bodyId="left-right-sidebar-page" pageType="${pageType}">
    <t:product_filter_sidebar/>
    <jsp:doBody/>
    <c:if test="${userManager.user.role != 'employee'}">
        <t:shopping_cart_sidebar/>
    </c:if>
</t:main>