<%@tag description="Template for products on the products page" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@tag import="com.onemore.entity.Product" %>
<%@attribute name="product" required="true" type="com.onemore.entity.Product"%>

<li id="product-id-${product.productId}" class="product">
    <div class="details">
        <div class="product-image">
            <a href="${basePath}/products/${product.name}_${product.productId}">
                <img src="${basePath}/${productImagePath}/${product.productType}/${product.imageUrl}"/>
            </a>
        </div>        
        <a class="title" href="${basePath}/products/${product.name}_${product.productId}">
            ${product.name}
        </a>
        ${fn:toUpperCase(fn:substring(product.productType, 0, 1))}${fn:toLowerCase(fn:substring(product.productType, 1,fn:length(product.productType)))}</br>
        <c:choose>
            <c:when test="${product.quantityInStock == 0}">
                <div class="low-stock">Out of stock</div>
            </c:when>             
            <c:when test="${product.quantityInStock lt 10}">
                <div class="low-stock">Only (${product.quantityInStock}) left</div>
            </c:when>
            <c:otherwise>
                <div class="in-stock">In stock</div>                 
            </c:otherwise>
        </c:choose>
        <c:if test="${product.averageRating != 0}">
            <div class="rateit" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-value="${product.averageRating}"></div>
        </c:if>
    </div>
    <t:product_quantity productId="${product.productId}" productPrice="${product.price}"/>
</li>
