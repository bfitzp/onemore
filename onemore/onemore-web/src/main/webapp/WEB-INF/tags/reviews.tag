<%@tag description="Template for product review component" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@tag import="java.util.Collection" %>
<%@attribute name="reviews" required="true" type="java.util.Collection"%>

<div id="reviews">
    <h2>Reviews (<span id="num-reviews">${fn:length(reviews)}</span>)</h2>

    <c:set var="showReviewForm" value="true"/>
    <ul id="reviews-list">
        <c:forEach items="${reviews}"  var="review">
            <t:review review="${review}"/>
            <c:if test="${review.user.userId == userManager.user.userId}">
                <c:set var="showReviewForm" value="false"/>
            </c:if>
        </c:forEach>
    </ul>

    <c:if test="${showReviewForm == true && userManager.user.userId != null}">
        <form id="add-review-form" action="reviews/add_review" method="POST">
            <h3>Add a review</h3>
            <label for="review-title">Title</label>
            <input type="text" name="title" id="review-title"/>
            <label for="rateit" name="rating">Rating</label>
            <div id="new-rating" class="rateit" data-rateit-resetable="false" data-rateit-min="0" data-rateit-step="1"></div>
            <input type="hidden" value="0" name="rating" id="rating"/>          
            <label for="review-body" name="body">Body</label>
            <textarea id="review-body"></textarea>
            <input type="submit" value="Submit" id="review-button" class="standard-button" name="submitReview"/>
        </form>
    </c:if>
</div>