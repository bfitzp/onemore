<%@tag description="Template for review on individual product pages" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@tag import="com.onemore.entity.Review" %>
<%@attribute name="review" required="true" type="com.onemore.entity.Review"%>

<li id="review-id-${review.reviewPK.userId}-${review.reviewPK.productId}" class="review">
    <h4>${review.title}</h4>
    <div class="rateit" data-rateit-value="${review.rating}" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
    <span class="review-user">by ${review.user.firstName} ${review.user.lastName}</span>
    <p>${review.body}</p>
</li>