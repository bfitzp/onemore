<%@tag description="Template for products on the products page" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@tag import="com.onemore.entity.Address" %>
<%@attribute name="address" required="true" type="com.onemore.entity.Address"%>

<li> 
    <label>
        <input type="radio" name="address" value="address_"${address.addressId}/><span class="dispatch">Dispatch to this address</span>
        <div class="details">
            <div>${address.line1}</div>
            <div>${address.line2}</div>
            <div>${address.townCity}</div>
            <div>${address.county}</div>
        </div>
    </label> 
</li> 