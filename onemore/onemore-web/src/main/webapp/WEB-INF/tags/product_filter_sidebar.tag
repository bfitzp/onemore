<%@tag description="Template for the product filter sidebar" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div id="product-filter-sidebar">
    <h2>Type</h2>
    <ul id="types">
        <c:forEach items="${productManager.typeFilters}"  var="filter">
            <li id="${filter}"><span>${filter}</span></li>
        </c:forEach>        
    </ul>

    <h2>Price range</h2>
    <ul id="price-ranges">
        <li id="price-0-199"><span>€0.00 - €1.99</span></li>
        <li id="price-200-499"><span>€2.00 - €4.99</span></li>
        <li id="price-400-999"><span>€4.00 - €9.99</span></li>
        <li id="price-1000-1999"><span>€10.00 - €19.99</span></li>
        <li id="price-2000"><span>€20.00+</li>
    </ul>
</div>