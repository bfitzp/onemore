<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@attribute name="pageType" required="true"%>

<t:main bodyId="right-sidebar-page" pageType="${pageType}">
    <jsp:doBody/>
    <c:if test="${userManager.user.role != 'employee'}">
        <t:shopping_cart_sidebar/>
    </c:if>
</t:main>       