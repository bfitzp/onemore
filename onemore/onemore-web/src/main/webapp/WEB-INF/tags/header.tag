<%@tag description="Template the header" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="header">
    <div id="header-content">
        <a href="${basePath}/products" id="site-title">
            ONEMORE
            <div id="site-tagline">There's always time for one more!</div>
        </a>
        <c:choose>
            <c:when test="${userManager.user.userId gt 0}">
                <div id="logged-in">
                    Logged in: <span class="name">${userManager.user.firstName} ${userManager.user.lastName}</span>
                    <c:if test="${userManager.user.role.equals('employee')}">
                        <div>
                            <a href="${basePath}/admin">Admin</a>
                        </div>
                    </c:if>
                    <div>
                        <a href="${basePath}/mydetails">My Details</a>
                        <a href="${basePath}/logout">Log out</a>
                    </div>
                </div>                
            </c:when>             
            <c:otherwise>
                <div id="not-logged-in">
                    <a href="${basePath}/login">Log in</a>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>