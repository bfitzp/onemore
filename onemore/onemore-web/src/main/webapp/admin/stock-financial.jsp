<%-- 
    Document   : stock-financial
    Created on : 14-Feb-2013, 15:49:49
    Author     : Shanwe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<t:no_sidebar_page pageType="smaller-container">
    <div id="content-header">
        <h1>Stock Report</h1>
    </div>
    <div id="terms-and-conditions-border">
        <div id="stock-report-form">
            <form name="stock-form" action="${basePath}/admin/financial-report-stock" method="POST">   
                <table cellspacing="2" cellpadding="2" style="float: left">
                    <thead>
                        <tr>
                            <th>Search a Stock Order</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><h2>Select a Order by quantity range</h2>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Min Order Quantity</th>
                                            <th>Max Order Quantity</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input type="text" name="minQuantity" value="" /></td>
                                            <td><input type="text" name="maxQuantity" value="" /></td>
                                            <td><input type="submit" value="Query" name="queryId" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td><h2>Select a Order by price range</h2>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Min Order Price</th>
                                            <th>Max Order Price</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input type="text" name="minPrice" value="" /></td>
                                            <td><input type="text" name="maxPrice" value="" /></td>
                                            <td><input type="submit" value="Query" name="queryId" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>

                        </tr>
                        <tr>
                            <td><h2>Select a Order by Date Range</h2>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input name="startOrderDate" type="date" value=""></td>
                                            <td><input name="endOrderDate" type="date" value=""></td>
                                            <td><input type="submit" value="Query" name="queryId" /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </form>
        </div>
        <div class="stock-report-results">
            <c:if test="${empty stockOrders && !noQuery}"><tr><td>Your query returned no results</td></tr></c:if>
            <c:if test="${!stockOrders.isEmpty() && stockOrders.size() gt 0}">
                <table>
                    <thead>
                        <tr>
                            <th>Order Supplier ID</th>
                            <th>Order Product Name</th>
                            <th>Stock Order Cost</th>
                            <th>Stock Order Quantity</th>
                            <th>Stock Order Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="order" items="${stockOrders}">
                            <tr>
                                <td>${order.supplier.supplierId}</td>
                                <td>${order.product.productId}</td>
                                <td>€${order.cost}</td>
                                <td>${order.quantity}</td>
                                <td>${order.date}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <table>
                    <caption>Query Statistics</caption>
                    <thead>
                        <tr>
                            <td>Number of Orders</td>
                            <td>Total Order cost</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>${fn:length(stockOrders)}</td>
                            <td>€${totalCost}</td>
                        </tr>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</t:no_sidebar_page>   

