<%-- 
    Document   : Admin Indext page
    Created on : Feb 22, 2013, 8:34:08 PM
    Author     : Shane
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<t:no_sidebar_page pageType="smaller-container">
    <div id="content-header">
        <h1>Employee Admin</h1>
    </div>
    <div id="terms-and-conditions-border">
        <ul id="terms-and-conditions">
            <p><b>Admin Page</b></p>
            <li><a href="${basePath}/admin/newproduct">Product Administration</a></li>
            <li><a href="${basePath}/admin/financial-report-index">Report Administration</a></li>
            <li><a href="${basePath}/admin/newemployee">Employee Management</a></li>
        </ul>
    </div>
</t:no_sidebar_page>   
