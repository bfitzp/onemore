<%-- 
    Document   : Employee Management
    Created on : 07-Feb-2013, 19:56:38
    Author     : Shane Ryan 10340427
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<t:no_sidebar_page pageType="smaller-container">
    <jsp:body>
        <script src="${basePath}/javascript/handle-product-menu.js"></script>
        <script src="${basePath}/javascript/remove-employee.js"></script>
        <script src="${basePath}/javascript/new-employee.js"></script>
        <link rel="stylesheet" type="text/css" href="${basePath}/css/main-product-user.css">
        <div class="content-product-handling">
            <div id="content-header">
                <span class="button-amend-product">Remove</span>
                <span class="button-seperator">&nbsp;/&nbsp;</span>
                <span class="button-add-product">Add</span></h1>            
                <h1>Employee Management</h1>
            </div>
            <div class="wrapper-product-input-details">
                <form class="contact_form" method="post" name="contact_form">
                    <ul>
                        <li>
                            <h2>New Employee</h2>
                            <span class="required_notification">* Denotes Required Field</span>
                        </li>
                        <li>
                            <h3>Employee name</h3>
                        </li>
                        <li>
                            <label for="employeeFName">First Name</label>
                            <input type="text" name="employeeFName" placeholder="Employee First Name" required />
                        </li>
                        <li>
                            <label for="employeeLName">Last Name</label>
                            <input type="text" name="employeeLName" placeholder="Employee Last Name" required />
                        </li>
                        <li>
                            <label for="employeePPS">Employee PPS</label>
                            <input type="text" name="employeePPS" placeholder="8087395M" required pattern="^(\d{7})([A-Z]{1,2})$" />
                        </li>
                        <li>
                            <label for="employeeWage">Employee Hourly Wage</label>
                            <input type="text" name="employeeWage" placeholder="€20.00" required />
                        </li>
                        <li>
                            <h3>Employee Address</h3>
                        </li>
                        <li>
                            <label for="line-1">Line 1</label>
                            <input id="line-1" type="text" name="line1" placeholder="42 Answer Street" required=""><div class="message"></div>
                        </li>
                        <li>
                            <label for="line-2">Line 2</label>
                            <input id="line-2" type="text" name="line2" placeholder="Anywhere"><div class="message"></div>
                        </li>
                        <li>
                            <label for="town-city">Town/City</label>
                            <input id="password" type="text" name="townCity" placeholder="New Town" required=""><div class="message"></div>
                        </li>
                        <li>
                            <label for="county">County</label>
                            <input id="password" type="text" name="county" placeholder="Dublin" required=""><div class="message"></div>
                        </li>
                        <li>
                            <label for="password">Password</label>
                            <input type="text" name="employeePassword" placeholder="password" required />
                        </li>
                        <li>
                            <label for="confirm">Confirm</label>
                            <input type="text" name="confirm" placeholder="password" required onclick="checkPassword(this)"/>
                        </li>
                        <li>
                            <input id="register-button" class="standard-button" type="submit" value="Create">
                        </li>
                    </ul>
                </form>
                <div class="add-success-notification"></div>
            </div> 
            <div class="wrapper-amend-product-input-details">
                <form class="contact_form" action="${basePath}/removeEmployee" method="post" name="remove_employee">
                    <ul>

                        <li>
                            <h2>Remove Employee</h2>
                            <span class="required_notification">* Denotes Required Field</span>
                        </li>
                        <li>
                            <h3>Remove By Name</h3>
                        </li>
                        <li>
                            <label for="employeeFName">First Name</label>
                            <input type="text" name="employeeFName" placeholder="Employee First Name" required />
                        </li>
                        <li>
                            <label for="employeeLName">Last Name</label>
                            <input type="text" name="employeeLName" placeholder="Employee Last Name" required />
                        </li>
                        <li>
                            <h2>OR</h2>
                        </li>
                        <li>
                            <h3>Remove By PPS Number</h3>
                        </li>
                        <li>
                            <label for="employeePPS">Employee PPS</label>
                            <input type="text" name="employeePPS" placeholder="8087395M" required pattern="^(\d{7})([A-Z]{1,2})$" />
                        </li>
                        <li>
                            <button class="amend-submit" type="button">Remove Employee</button>
                        </li>
                    </ul>
                </form>
                <div class="amend-display-item"></div>
                <div class="amend-success-notification"></div>
            </div> 
        </div>
    </jsp:body>
</t:no_sidebar_page>   