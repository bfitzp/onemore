<%-- 
    Document   : employee-financial
    Created on : 14-Feb-2013, 15:49:40
    Author     : Shanwe
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Enumeration"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<t:no_sidebar_page pageType="smaller-container">
    <div id="content-header">
        <h1>Employee Records</h1>
    </div>
    <div id="terms-and-conditions-border">
        <ul>
            <div id="content-inner">
                <form action="${basePath}/admin/financial-report-employee" method="POST">
                    <table border="0" cellspacing="5" cellpadding="2">
                        <tbody>
                            <tr>
                                <td><h2>Search by Employee Name</h2>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>First</th>
                                                <th>Last</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><input type="text" name="firstName" value=""/></td>
                                                <td><input type="text" name="lastName" value=""/></td>
                                                <td><input type="submit" value="Query" name="employeeNameSubmit" /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><h2>Search by Employee PPS</h2>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Employee PPS Number</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><input type="text" name="employeePPS" value=""/></td>
                                                <td><input type="submit" value="Query" name="employeePPSSubmit" /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>

                            </tr>
                            <tr>
                                <td><h2>Search by Employee ID</h2>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>Employee Id</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><input type="text" name="employeeID" value=""/></td>
                                                <td><input type="submit" value="Query" name="employeeIDSubmit" /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </form>
                <table style="float: left">
                    <thead>
                        <c:if test="${empty employees && !noQuery}"><tr><td>Your query returned no results</td></tr></c:if>
                        <c:if test="${!employees.isEmpty() && employees.size() > 0}">
                            <tr>
                                <td>Employee ID</td>
                                <td>First Name</td>
                                <td>Last Name</td>
                                <td>Employee PPS</td>
                                <td>Wage per hour</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="employee" items="${employees}">
                                <tr>
                                    <td>${employee.user.userId}</td>
                                    <td>${employee.user.firstName}</td>
                                    <td>${employee.user.lastName}</td>
                                    <td>${employee.ppsNo}</td>
                                    <td>€${employee.wage}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>

                    <table style="float: right">
                        <thead>
                            <tr>
                                <td>Number of Employees</td>
                                <td>Wage total per hour</td>
                                <td>Result Count</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>${employeeCount}</td>
                                <td>€${totalWage}</td>
                                <td>Your query returned ${employees.size()} results</td>
                            </tr>
                        </tbody>
                    </table>
                </c:if>
            </div>
        </ul>
    </div>
</t:no_sidebar_page>   

