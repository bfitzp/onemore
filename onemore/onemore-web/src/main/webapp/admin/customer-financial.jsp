
<%-- 
    Document   : customer-financial
    Created on : 14-Feb-2013, 16:42:44
    Author     : Shane
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<t:no_sidebar_page pageType="smaller-container">
    <div id="content-header">
        <h1>Customer Report Page</h1>
    </div>
    <div id="terms-and-conditions-border">
        <ul>
            <div id="customer-report-form">
                <div id="content-inner">
                    <form action="${basePath}/admin/financial-report-customer" method="POST">
                        <table class="customer-report-form-table" border="0">
                            <thead>
                                <tr>
                                    <th>Customer Query Form</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><h2>Find by Name</h2>
                                        <br/>
                                        <table border="0">
                                            <thead>
                                                <tr>
                                                    <th>First Name</th>
                                                    <th>Last Name</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input type="text" name="customerFname" value="" /></td>
                                                    <td><input type="text" name="customerLname" value="" /></td>
                                                    <td><input type="submit" value="Submit" name="submitNameQuery" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><h2>Find by Id</h2>
                                        <table border="0">
                                            <thead>
                                                <tr>
                                                    <th>Customer Id</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input type="text" name="customerId" value="" /></td>
                                                    <td><input type="submit" value="Submit" name="submitIdQuery" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><h2>Find by order range</h2>
                                        <table border="0">
                                            <thead>
                                                <tr>
                                                    <th>Min Quantity Range</th>
                                                    <th>Max Quantity Range</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input type="text" name="orderMin" value="" /></td>
                                                    <td><input type="text" name="orderMax" value="" /></td>
                                                    <td><input type="submit" value="Submit" name="submitQuantityRangeQuery" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>


                <c:if test="${!empty queryResults}">
                    <div id="customer-report-statistic">
                        <h2>Customer Statistics</h2>
                        <table class="customer-report-statistic-table">
                            <thead>
                                <tr>
                                    <th>Statistic</th>
                                    <th>Value</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Total Number Customers</td>
                                    <td>${totalCustomerCount}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br/>
                    <br/>
                    <div id="customer-report-results">
                        <table class="customer-report-results-table">
                            <caption>OneMore Customer Data</caption>
                            <tr>
                                <th>User Id</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>password</th>
                                <th>email</th>
                                <th>Creation Date</th>
                                <th>status</th>
                                <th>Number of Orders</th>
                            </tr>
                            <c:forEach var="customer" items="${customers}" varStatus="status">
                                <tr>
                                    <td>${customer.userId}</td>
                                    <td>${customer.firstName}</td>
                                    <td>${customer.lastName}</td>
                                    <td>${customer.password}</td>
                                    <td>${customer.email}</td>
                                    <td>${customer.creationDate}</td>
                                    <td>${customer.status}</td>
                                    <td>${orderCount[status.index]}</td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </c:if>
            </div>
        </ul>
    </t:no_sidebar_page>   

