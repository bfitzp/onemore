<%-- 
    Document   : index-financial
    Created on : 24-Feb-2013, 17:14:40
    Author     : Shane Ryan <s.ryan25@nuigalway.ie>
--%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<t:no_sidebar_page pageType="smaller-container">
    <div id="content-header">
        <h1>Report</h1>
    </div>
    <div id="terms-and-conditions-border">
        <ul id="terms-and-conditions">
            <p>Report Administration</p>
            <li><a href="${basePath}/admin/financial-report-customer">Customer Report</a></li>
            <li><a href="${basePath}/admin/financial-report-stock">Stock Report</a></li>
            <li><a href="${basePath}/admin/financial-report-employee">Employee Report</a></li>
            <li><a href="${basePath}/admin/index.jsp">Back to main admin page</a></li>
        </ul>
    </div>
</t:no_sidebar_page>   
