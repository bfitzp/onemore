-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2013 at 05:03 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `onemore`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `address_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `address_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `line1` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `line2` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `town_city` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `county` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`address_id`),
  KEY `fk_address_user1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address_id`, `user_id`, `address_type`, `line1`, `line2`, `town_city`, `county`) VALUES
(1, 1, 'billing', '114 The Hut', 'Ballybane', 'Galway', 'Co. Galway'),
(2, 2, 'billing', '12 road way', 'anywhere', 'Galway', 'Co. Galway'),
(3, 3, 'billing', 'new address', 'anywhere', 'Tuam', 'Galway'),
(4, 4, 'billing', '12 Dumb Street', 'anywhere', 'Belfast', 'Co. Belfast'),
(5, 5, 'billing', '54 someplace', 'whocares', 'Cork', 'Co. Cork'),
(6, 1, 'delivery', 'erterterte', 'Secret Place 1', 'Tuam', 'Co. Galway'),
(7, 1, 'delivery', '43 Hidden Street', 'Another Place', 'Oranmore', 'Co. Galway'),
(13, 3, 'delivery', '345 amended', 'anywherexcvxcvxc', 'Tuam', 'Galwayxcvxcv');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `user_id` int(10) unsigned NOT NULL,
  `wage` float unsigned NOT NULL,
  `pps_no` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE IF NOT EXISTS `order_product` (
  `product_product_id` int(10) unsigned NOT NULL,
  `user_order_order_id` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  PRIMARY KEY (`product_product_id`,`user_order_order_id`),
  KEY `fk_product_has_user_order_user_order1_idx` (`user_order_order_id`),
  KEY `fk_product_has_user_order_product1_idx` (`product_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`product_product_id`, `user_order_order_id`, `quantity`) VALUES
(14, 3, 9),
(45, 3, 7),
(78, 1, 4),
(78, 14, 14),
(81, 11, 4),
(81, 13, 20),
(81, 14, 3),
(82, 14, 6),
(85, 14, 8),
(88, 1, 3),
(104, 3, 8),
(104, 11, 13),
(104, 12, 4);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `description` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity_in_stock` int(10) unsigned NOT NULL,
  `image_url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `product_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `average_rating` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=147 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `name`, `price`, `description`, `quantity_in_stock`, `image_url`, `product_type`, `average_rating`) VALUES
(1, 'Kumala Semillon Chardonnay (South Africa)', 8.01, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 0, 'Kumala_Semillon_Chardonnay_(South_Africa).png', 'white wine', 3),
(2, 'La Croisade Sauvignon Blanc, Vin de Pays d''Oc', 7.03, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 80, 'La_Croisade_Sauvignon_Blanc_Vin_de_Pays_dOc.png', 'white wine', 3),
(3, 'Smirnoff', 12.99, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 63, 'smirnoff.png', 'vodka', 3),
(4, 'Japanese whiskey', 21.56, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 25, 'whiskey.png', 'whiskey', 2),
(5, 'Asghil', 1.97, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 34, 'asghil_bottle.png', 'beer', 3),
(6, 'Boddington', 1.97, 'this is nice stuff.. or so they say', 96, 'boddington_draught_bitter_can.png', 'bitter', 3),
(7, 'Budweiser', 0.37, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 79, 'bud_bottle.png', 'beer', 3),
(8, 'Budweiser', 1.93, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 7, 'bud_can.png', 'beer', 3),
(9, 'Bullmers', 1.97, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 97, 'bulmers_bottle.png', 'cider', 2),
(10, 'Calls', 2.59, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 65, 'calls_bottle.png', 'beer', 4),
(11, 'Carling', 1.06, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 36, 'carling_bottle.png', 'beer', 2),
(12, 'Carlsberg', 0.59, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 86, 'carlsberg_can.png', 'beer', 3),
(13, 'Carlsberg Export', 2.78, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 20, 'carlsberg_export_bottle.png', 'beer', 3),
(14, 'Carlsberg Export', 0.1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 45, 'carlsberg_export_can.png', 'beer', 2),
(15, 'Carlsbery Special Brew', 1.18, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 655555, 'carlsberg_special_brew_can.png', 'beer', 4),
(16, 'Carlsberg', 2.58, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 81, 'carlsberg_can.png', 'beer', 3),
(17, 'Chang', 0.35, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 16, 'chang_bottle.png', 'beer', 2),
(18, 'Corona', 0.84, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 38, 'corona_bottle.png', 'beer', 3),
(19, 'Guiness', 2.16, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 43, 'guiness_can.png', 'beer', 3),
(20, 'Kangarilla Road Chardonnay (Australia)', 7.77, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 68, 'Kangarilla_Road_Chardonnay_(Australia).png', 'white wine', 3),
(21, 'Birra Moretti', 1.66, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 83, 'Birra_Moretti.png', 'beer', 4),
(22, 'Erdinger Weissbier', 1.84, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 9, 'Erdinger_Weissbier.png', 'beer', 4),
(23, 'Fosters', 1.22, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 96, 'Fosters.png', 'beer', 3),
(24, 'Grolsch', 0.55, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 52, 'Grolsch.png', 'beer', 4),
(25, 'Heineken', 2.12, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 75, 'Heineken.png', 'beer', 3),
(26, 'Holsten Pils', 2.96, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 17, 'Holsten_Pils.png', 'beer', 2),
(27, 'Kingfisher', 2.42, 'Lorem ipsum dolor sit amet, cb hjcghkjh onsectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 70, 'Kingfisher.png', 'beer', 3),
(28, 'Kronenbourg 1664', 0.21, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 53, 'Kronenbourg_1664.png', 'beer', 2),
(29, 'Birra Moretti', 2.79, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 84, 'Birra_Moretti.png', 'beer', 2),
(30, 'Erdinger Weissbier', 1.31, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 62, 'Erdinger_Weissbier.png', 'beer', 2),
(31, 'Fosters', 1.19, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 57, 'Fosters.png', 'beer', 2),
(32, 'Grolsch', 2.02, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 98, 'Grolsch.png', 'beer', 2),
(33, 'Heineken', 0.55, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 23, 'Heineken.png', 'beer', 2),
(34, 'Holsten Pils', 2.67, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 18, 'Holsten_Pils.png', 'beer', 2),
(35, 'Kingfisher', 2.72, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 23, 'Kingfisher.png', 'beer', 4),
(36, 'Kronenbourg 1664', 2.56, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 62, 'Kronenbourg_1664.png', 'beer', 3),
(37, 'Birra Moretti', 1.67, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 39, 'Birra_Moretti.png', 'beer', 2),
(38, 'Erdinger Weissbier', 0.68, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 13, 'Erdinger_Weissbier.png', 'beer', 3),
(39, 'Fosters', 1.37, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 45, 'Fosters.png', 'beer', 3),
(40, 'Grolsch', 1.79, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 90, 'Grolsch.png', 'beer', 4),
(41, 'Heineken', 1.87, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 13, 'Heineken.png', 'beer', 3),
(42, 'Holsten Pils', 0.96, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 97, 'Holsten_Pils.png', 'beer', 4),
(43, 'Kingfisher', 2.19, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 48, 'Kingfisher.png', 'beer', 3),
(44, 'Kronenbourg 1664', 2.09, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 48, 'Kronenbourg_1664.png', 'beer', 3),
(45, 'Murphy''s Irish Stout', 0.86, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 95, 'Murphys_Irish_Stout.png', 'beer', 3),
(46, 'Peroni', 1.05, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 30, 'Peroni.png', 'beer', 3),
(47, 'Stella Artois La Grande Biere', 2.65, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 72, 'Stella_Artois_La_Grande_Biere.png', 'beer', 3),
(48, 'Tiger Beer', 1.12, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 69, 'Tiger_Beer.png', 'beer', 3),
(49, 'Tsingtao', 0.63, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 27, 'Tsingtao.png', 'beer', 4),
(50, 'London Pride Ale 500ml Bottle', 0.37, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 28, 'London_Pride_Ale_500ml_Bottle.png', 'bitter', 4),
(51, 'London Pride Ale 500ml Bottle', 1.93, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 60, 'London_Pride_Ale_500ml_Bottle.png', 'bitter', 4),
(52, 'Marston''s Old Empire Pale Ale', 2.57, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 17, 'Marstons_Old_Empire_Pale_Ale.png', 'bitter', 4),
(53, 'Marston''s Owd Rodger 500ml', 1.06, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 7, 'Marstons_Owd_Rodger_500ml.png', 'bitter', 2),
(54, 'McEwans Export', 0.59, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 82, 'McEwans_Export.png', 'bitter', 3),
(55, 'Newcastle Brown Ale', 2.78, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 90, 'Newcastle_Brown_Ale.png', 'bitter', 4),
(56, 'Ringwood  Premium Ale 500ml', 0.1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 5, 'Ringwood_Brewery_Fortyniner_Premium_Ale_500ml.png', 'bitter', 3),
(57, 'Bollinger Special Cuvee Champagne', 59.67, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 57, 'Bollinger_Special_Cuvee_Champagne.png', 'champagne', 4),
(58, 'Canard-Duchene Brut Champagne', 43.67, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 70, 'Canard-Duchene_Brut_Champagne.png', 'champagne', 3),
(59, 'Dom Perignon 2002 Vintage', 59.35, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 78, 'Dom_Perignon_2002_Vintage.png', 'champagne', 2),
(60, 'Jacquart Brut Mosaique NV Champagne', 65.73, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 81, 'Jacquart_Brut_Mosaique_NV_Champagne.png', 'champagne', 4),
(61, 'Krug Grande Cuvee', 50.63, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 71, 'Krug_Grande_Cuvee.png', 'champagne', 3),
(62, 'Lanson Black Label Brut NV', 45.93, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 11, 'Lanson_Black_Label_Brut_NV.png', 'champagne', 3),
(63, 'Lanson Brut Rose Champagne', 67.76, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 45, 'Lanson_Brut_Rose_Champagne.png', 'champagne', 4),
(64, 'Lanson Vintage Gold Label', 41.01, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 94, 'Lanson_Vintage_Gold_Label.png', 'champagne', 3),
(65, 'Brothers Strawberry Mixed Pear cider', 0.37, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 32, 'Brothers_Strawberry_Mixed_Pear_cider.png', 'cider', 2),
(66, 'Diamond White', 1.93, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 79, 'Diamond_White.png', 'cider', 3),
(67, 'Gaymer''s Olde English', 2.57, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 1, 'Gaymers_Olde_English.png', 'cider', 3),
(68, 'Magners Original Pear Cider 568ml', 1.06, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 67, 'Magners_Original_Pear_Cider_568ml.png', 'cider', 3),
(69, 'Scrumpy Jack', 0.59, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 29, 'Scrumpy_Jack.png', 'cider', 2),
(70, 'Shepherd Neame Bishops Finger 500ml', 2.78, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 73, 'Shepherd_Neame_Bishops_Finger_500ml.png', 'cider', 2),
(71, 'Strongbow', 1.01, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 59, 'Strongbow.png', 'cider', 3),
(72, 'Tetley''s Smoothflow', 1.18, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 77, 'Tetleys_Smoothflow.png', 'cider', 3),
(73, 'Woodpecker', 2.58, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 7, 'Woodpecker.png', 'cider', 4),
(74, 'Bombay Sapphire', 16.56, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 7, 'Bombay_Sapphire.png', 'gin', 3),
(75, 'Gordon''s Sloe Gin', 11.22, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 13, 'Gordons_Sloe_Gin.png', 'gin', 3),
(76, 'Hendrick''s Gin 70cl', 16.45, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 44, 'Hendricks_Gin_70cl.png', 'gin', 2),
(77, 'Tanqueray® London Dry Gin Export Strength 70c', 18.58, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 81, 'Tanqueray®_London_Dry_Gin_Export_Strength,_70cl.png', 'gin', 3),
(78, 'Aftershock', 21.56, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 76, 'Aftershock.png', 'liqueurs', 3),
(81, 'Amarula', 23.58, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 82, 'Amarula.png', 'liqueurs', 3),
(82, 'Antica Sambuca Classic', 18.54, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 36, 'Antica_Sambuca_Classic_Liqueur_70cl.png', 'liqueurs', 4),
(83, 'Antica Sambuca with Orange & Mango Flavour Li', 16.98, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 33, 'Antica_Sambuca_with_Orange_&_Mango_Flavour_Liqueur_70cl.png', 'liqueurs', 3),
(84, 'Antica Sambuca with Raspberry Flavour Liqueur', 24.25, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 59, 'Antica_Sambuca_with_Raspberry_Flavour_Liqueur_70cl.png', 'liqueurs', 4),
(85, 'Kahlua', 15.34, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 98, 'Kahlua.png', 'liqueurs', 4),
(86, 'Southern Comfort', 18.92, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 10, 'Southern_Comfort.png', 'liqueurs', 3),
(87, 'Tia Maria', 23.59, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 57, 'Tia_Maria.png', 'liqueurs', 3),
(88, 'Antario Vendemmia Barolo (Italy)', 8.28, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 57, 'Antario_Vendemmia_Barolo_(Italy).png', 'red wine', 2),
(89, 'Argento Malbec Mendoza (Argentina)', 5.61, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 14, 'Argento_Malbec_Mendoza_(Argentina).png', 'red wine', 4),
(90, 'Blossom Hill Cabernet Sauvignon (USA)', 8.22, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 0, 'Blossom_Hill_Cabernet_Sauvignon_(USA).png', 'red wine', 3),
(91, 'Blossom Hill Califronian Red (USA)', 9.29, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 60, 'Blossom_Hill_Califronian_Red_(USA).png', 'red wine', 4),
(92, 'Campo Dorado Rioja (Spain)', 6.77, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 97, 'Campo_Dorado_Rioja_(Spain).png', 'red wine', 4),
(93, 'Campo Dorado Rioja Crianza (Spain)', 5.99, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 8, 'Campo_Dorado_Rioja_Crianza_(Spain).png', 'red wine', 4),
(94, 'Campo Dorado Rioja Tinto 75cl (Spain)', 9.63, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 50, 'Campo_Dorado_Rioja_Tinto_75cl_(Spain).png', 'red wine', 3),
(95, 'Canaletto Pinot Noir IGT, Pavia 75cl (Italy)', 5.17, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 27, 'Canaletto_Pinot_Noir_IGT,_Pavia_75cl_(Italy).png', 'red wine', 3),
(96, 'Casillero Diablo Cabernet Sauvignon (Chile)', 6.96, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 83, 'Casillero_Diablo_Cabernet_Sauvignon_(Chile).png', 'red wine', 3),
(97, 'Chateau Villadière Lussac Saint Emilion 750ml', 9.29, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 37, 'Chateau_Villadière_Lussac_Saint_Emilion_750ml_(France).png', 'red wine', 3),
(98, 'Chateauneuf-du-Pape, Domaine Lucien Barrot (F', 5.59, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 36, 'Chateauneuf-du-Pape,_Domaine_Lucien_Barrot_(France).png', 'red wine', 3),
(99, 'Chianti Classico 75cl (Italy)', 5.07, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 68, 'Chianti_Classico_75cl_(Italy).png', 'red wine', 4),
(100, 'Claret 75cl (France)', 8.61, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 35, 'Claret_75cl_(France).png', 'red wine', 3),
(101, 'Cotes du Rhone 2009 Vidal- Fleury', 7.77, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 68, 'Cotes_du_Rhone_2009_Vidal-_Fleury.png', 'red wine', 2),
(102, 'Cuvee de Richard Red, Vin de Pays de l''Aude (', 8.07, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 38, 'Cuvee de Richard Red, Vin de Pays de lAude (France).png', 'red wine', 3),
(103, 'E & J Gallo Turning Leaf Zinfandel (USA)', 7.03, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 86, 'E_&_J_Gallo_Turning_Leaf_Zinfandel_(USA).png', 'red wine', 3),
(104, 'Absolut Vodka', 13.28, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 15, 'Absolut_Vodka.png', 'rum', 3),
(105, 'Archers Peach Schnapps', 10.61, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 17, 'Archers_Peach_Schnapps.png', 'rum', 2),
(106, 'Bacardi White Rum', 13.22, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 41, 'Bacardi_White_Rum.png', 'rum', 4),
(107, 'Grey Goose Vodka 70cl', 14.29, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 54, 'Grey_Goose_Vodka_70cl.png', 'rum', 3),
(108, 'Malibu', 11.77, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 50, 'Malibu.png', 'rum', 3),
(109, 'Morgans Spiced Rum', 10.99, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 88, 'Morgans_Spiced_Rum.png', 'rum', 3),
(110, 'Skyy Vodka® 70cl', 14.63, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 91, 'Skyy_Vodka®_70cl.png', 'rum', 3),
(111, 'Smirnoff Lime Vodka 70cl', 10.17, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 89, 'Smirnoff_Lime_Vodka_70cl.png', 'rum', 3),
(112, 'Smirnoff Red Label', 11.96, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 72, 'Smirnoff_Red_Label.png', 'rum', 3),
(113, 'Wood''s® 100 Old Navy Rum 70cl', 14.29, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 96, 'Woods®_100_Old_Navy_Rum_70cl.png', 'rum', 2),
(114, 'Freixenet Cordon Negro (Spain)', 26.56, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 62, 'Freixenet_Cordon_Negro_(Spain).png', 'sparkling', 3),
(115, 'Lambrini Original 75cl (United Kingdom)', 21.22, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 25, 'Lambrini_Original_75cl_(United Kingdom).png', 'sparkling', 2),
(116, 'Montana Lindauer (New Zealand)', 26.45, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 36, 'Montana_Lindauer_(New Zealand).png', 'sparkling', 3),
(117, 'Sierra Silver Tequila Mexico', 16.56, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 9, 'Sierra_Silver_Tequila_Mexico.png', 'tequila', 2),
(118, 'Tequila Gold Cuervo Mexico', 11.22, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 37, 'Tequila_Gold_Cuervo_Mexico.png', 'tequila', 3),
(119, 'Tequila Silver Cuervo Mexico', 16.45, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 60, 'Tequila_Silver_Cuervo_Mexico.png', 'tequila', 3),
(120, 'Black Bush Whiskey', 16.22, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 87, 'Black_Bush_Whiskey.png', 'whiskey', 4),
(121, 'Bushmills', 21.45, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 54, 'Bushmills.png', 'whiskey', 4),
(122, 'Bushmills 10yr Malt Gift Box', 23.58, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 12, 'Bushmills_10yr_Malt_Gift_Box.png', 'whiskey', 3),
(123, 'Bushmills Malt Single Malt Irish Whiskey Aged', 18.54, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 98, 'Bushmills_Malt_Single_Malt_Irish_Whiskey_Aged_10_years_700ml.png', 'whiskey', 3),
(124, 'Drambuie', 16.98, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 53, 'Drambuie.png', 'whiskey', 3),
(125, 'Glayva', 24.25, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 72, 'Glayva.png', 'whiskey', 3),
(126, 'Glenfiddich Special Reserve Highland Malt', 15.34, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 3, 'Glenfiddich_Special_Reserve_Highland_Malt.png', 'whiskey', 3),
(127, 'Isle of Jura 10 YO Islands Single Malt', 18.92, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 99, 'Isle_of_Jura_10_YO_Islands_Single_Malt.png', 'whiskey', 2),
(128, 'Jack Daniel''s', 23.59, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 88, 'Jack_Daniels.png', 'whiskey', 3),
(129, 'Jameson''s', 16.18, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 41, 'Jamesons.png', 'whiskey', 3),
(130, 'Jim Beam', 15.15, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 43, 'Jim_Beam.png', 'whiskey', 3),
(131, 'Johnnie Walker Black Label', 22.2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 92, 'Johnnie_Walker_Black_Label.png', 'whiskey', 2),
(132, 'Talisker 10 YO Isle of Skye Malt', 20.55, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 32, 'Talisker_10_YO_Isle_of_Skye_Malt.png', 'whiskey', 3),
(133, 'Teacher''s', 21.14, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 84, 'Teachers.png', 'whiskey', 3),
(134, 'Black Tower Rivaner (Germany)', 8.28, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 23, 'Black_Tower_Rivaner_(Germany).png', 'white wine', 3),
(135, 'Blossom Hill Californian White (USA)', 5.61, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 63, 'Blossom_Hill_Californian_White_(USA).png', 'white wine', 3),
(136, 'Blossom Hill Chardonnay (USA)', 8.22, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 46, 'Blossom_Hill_Chardonnay_(USA).png', 'white wine', 4),
(137, 'Blue Nun (Germany)', 9.29, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 43, 'Blue_Nun_(Germany).png', 'white wine', 3),
(138, 'Casillero del Diablo Chardonnay (Chile)', 6.77, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 78, 'Casillero_del_Diablo_Chardonnay_(Chile).png', 'white wine', 3),
(139, 'Chablis Premier Cru Montmains Vocoret, Burgun', 5.99, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 60, 'Chablis_Premier_Cru_Montmains_Vocoret,_Burgundy_(France).png', 'white wine', 2),
(140, 'E&J Gallo Chardonnay (USA)', 9.63, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 69, 'E&J_Gallo_Chardonnay_(USA).png', 'white wine', 3),
(141, 'E&J Gallo Turning Leaf Chardonnay (USA)', 5.17, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 65, 'E&J_Gallo_Turning_Leaf_Chardonnay_(USA).png', 'white wine', 3),
(142, 'E & J Gallo White Zinfandel-Rose (USA)', 6.96, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 19, 'E_&_J_Gallo_White_Zinfandel-Rose_(USA).png', 'white wine', 3),
(143, 'Hardy''s Nottage Hill Chardonnay (Australia)', 9.29, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 0, 'Hardys_Nottage_Hill_Chardonnay_(Australia).png', 'white wine', 3),
(144, 'Hardy''s Stamp Chardonnay Semillon (Australia)', 5.59, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 45, 'Hardys_Stamp_Chardonnay_Semillon_(Australia).png', 'white wine', 3),
(145, 'Jacob''s Creek Chardonnay (Australia)', 5.07, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 25, 'Jacobs_Creek_Chardonnay_(Australia).png', 'white wine', 2),
(146, 'Jacob''s Creek Semillon Chardonnay (Australia)', 8.6, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor quam sed ligula aliquet nec volutpat lorem commodo. Proin tincidunt volutpat ipsum quis facilisis. Nullam.', 88, 'Jacobs_Creek_Semillon_Chardonnay_(Australia).png', 'white wine', 3);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `user_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `title` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `body` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `rating` smallint(6) NOT NULL,
  PRIMARY KEY (`user_id`,`product_id`),
  KEY `fk_review_user1_idx` (`user_id`),
  KEY `fk_review_product1_idx` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`user_id`, `product_id`, `title`, `body`, `rating`) VALUES
(1, 81, 'good', 'piss', 3),
(1, 82, 'nice drink', 'really enjoyed this', 5),
(1, 104, 'Great beer', 'Love it!', 4),
(2, 1, 'Quisque varius.', 'sapien imperdiet ornare. In faucibus. Morbi vehicula. Pellentesque tincidunt tempus risus. Donec egestas. Duis ac arcu. Nunc mauris. Morbi', 2),
(2, 2, 'enim. Etiam', 'ut dolor dapibus gravida. Aliquam tincidunt, nunc ac mattis ornare,', 2),
(2, 3, 'Cras dolor dolor, tempus', 'euismod est arcu ac orci. Ut semper pretium neque. Morbi quis', 1),
(2, 4, 'velit. Quisque', 'Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui augue eu', 5),
(2, 5, 'ac metus vitae', 'nisi nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut semper pretium neque. Morbi quis urna. Nunc quis', 2),
(2, 6, 'elit pede, malesuada', 'Nunc', 5),
(2, 7, 'eu, eleifend nec, malesuada ut,', 'pede, nonummy ut, molestie in, tempus eu, ligula. Aenean euismod mauris eu elit. Nulla facilisi. Sed neque. Sed eget lacus. Mauris non dui nec', 1),
(2, 8, 'eu', 'tempor, est ac mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula', 3),
(2, 9, 'risus a', 'metus facilisis lorem tristique aliquet. Phasellus fermentum', 3),
(2, 10, 'tincidunt, nunc ac', 'Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae mauris', 4),
(2, 11, 'Aenean euismod mauris eu elit.', 'Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at fringilla purus mauris a nunc. In at pede. Cras vulputate velit eu sem. Pellentesque ut', 2),
(2, 12, 'molestie', 'lorem eu metus. In lorem. Donec elementum, lorem ut aliquam iaculis, lacus pede sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique senectus et netus', 3),
(2, 13, 'at, iaculis quis, pede. Praesent', 'eget tincidunt dui augue eu tellus. Phasellus elit pede, malesuada vel, venenatis vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non,', 1),
(2, 14, 'aliquet libero. Integer', 'placerat, orci lacus vestibulum lorem, sit amet ultricies sem', 1),
(2, 15, 'consequat auctor, nunc nulla', 'dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero.', 5),
(2, 16, 'pede blandit congue. In', 'rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent interdum', 3),
(2, 17, 'tincidunt dui', 'posuere cubilia Curae; Phasellus ornare. Fusce mollis.', 3),
(2, 18, 'Nullam nisl.', 'egestas a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam', 1),
(2, 19, 'sit amet orci.', 'metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit amet,', 1),
(2, 20, 'a ultricies adipiscing,', 'Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel', 3),
(2, 21, 'at augue id', 'massa. Integer vitae nibh. Donec est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet,', 4),
(2, 22, 'Integer', 'adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu', 2),
(2, 23, 'cursus vestibulum. Mauris magna.', 'vitae, sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi metus. Vivamus euismod', 4),
(2, 24, 'at augue id ante dictum', 'pede blandit', 5),
(2, 25, 'amet, dapibus id, blandit at,', 'ante blandit viverra.', 4),
(2, 26, 'diam. Sed diam', 'ante dictum mi, ac mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum.', 3),
(2, 27, 'Donec', 'magnis dis parturient montes, nascetur ridiculus mus. Donec dignissim magna a tortor. Nunc commodo auctor velit.', 5),
(2, 28, 'sem egestas blandit. Nam nulla', 'ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat', 2),
(2, 29, 'pulvinar arcu et pede. Nunc', 'nec, imperdiet nec, leo. Morbi neque tellus,', 4),
(2, 30, 'sit', 'ultrices posuere cubilia Curae; Phasellus ornare. Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec,', 2),
(2, 31, 'est. Nunc ullamcorper, velit', 'Maecenas malesuada fringilla est. Mauris eu turpis. Nulla aliquet. Proin velit. Sed malesuada augue ut lacus. Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus,', 4),
(2, 32, 'dolor, tempus non, lacinia at,', 'parturient montes, nascetur ridiculus mus. Proin vel arcu eu odio tristique pharetra. Quisque ac libero nec ligula consectetuer rhoncus. Nullam velit dui, semper et, lacinia vitae, sodales at,', 2),
(2, 33, 'gravida.', 'euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim.', 3),
(2, 34, 'orci. Ut sagittis', 'eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at fringilla purus mauris a nunc. In at pede. Cras vulputate velit eu sem. Pellentesque', 3),
(2, 35, 'eu', 'Donec est. Nunc ullamcorper, velit in aliquet lobortis, nisi', 5),
(2, 36, 'in,', 'adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing', 5),
(2, 37, 'elit elit', 'nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget,', 1),
(2, 38, 'iaculis', 'venenatis vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis', 5),
(2, 39, 'Cras sed leo.', 'Maecenas', 2),
(2, 40, 'luctus', 'amet ornare lectus', 5),
(2, 41, 'Donec egestas. Aliquam nec enim.', 'aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam', 1),
(2, 42, 'vel arcu. Curabitur ut odio', 'tellus eu augue porttitor interdum. Sed auctor odio a purus.', 4),
(2, 43, 'massa lobortis', 'tincidunt tempus risus. Donec egestas. Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare,', 2),
(2, 44, 'at fringilla purus mauris a', 'elit fermentum risus, at fringilla purus mauris a nunc. In at pede. Cras vulputate velit eu sem. Pellentesque ut ipsum ac mi eleifend egestas. Sed pharetra, felis eget varius ultrices,', 1),
(2, 45, 'auctor. Mauris', 'ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas', 3),
(2, 46, 'nascetur ridiculus', 'blandit. Nam nulla magna, malesuada vel, convallis in, cursus et, eros. Proin ultrices. Duis volutpat', 3),
(2, 47, 'lacinia vitae,', 'lacinia', 4),
(2, 48, 'interdum', 'ac sem ut dolor dapibus gravida. Aliquam tincidunt, nunc ac mattis ornare,', 2),
(2, 49, 'augue id ante', 'Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim,', 5),
(2, 50, 'auctor ullamcorper, nisl arcu', 'mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus vitae mauris sit amet lorem semper', 4),
(2, 51, 'arcu imperdiet ullamcorper.', 'pede. Nunc sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam.', 5),
(2, 52, 'Class aptent taciti sociosqu', 'penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque', 5),
(2, 53, 'vitae odio', 'egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui,', 1),
(2, 54, 'ipsum cursus vestibulum.', 'id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce', 1),
(2, 55, 'vitae risus. Duis a', 'penatibus et magnis dis parturient montes,', 5),
(2, 56, 'Curabitur', 'eget, venenatis a, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui, in sodales elit erat vitae', 3),
(2, 57, 'fermentum fermentum arcu. Vestib', 'vehicula', 5),
(2, 58, 'in faucibus orci luctus', 'tristique senectus et netus', 5),
(2, 59, 'odio a purus. Duis', 'ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in aliquet lobortis, nisi nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut semper', 3),
(2, 60, 'mauris eu elit. Nulla', 'lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at fringilla purus mauris a nunc. In', 5),
(2, 61, 'Vestibulum accumsan neque et', 'magna. Nam ligula elit,', 4),
(2, 62, 'dolor dolor,', 'Maecenas iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim.', 1),
(2, 63, 'Sed congue,', 'in molestie', 5),
(2, 64, 'tellus justo sit amet', 'volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor', 2),
(2, 65, 'Aliquam', 'venenatis a, magna. Lorem ipsum', 4),
(2, 66, 'pellentesque a,', 'nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In ornare sagittis felis. Donec tempor, est ac mattis semper, dui lectus rutrum', 5),
(2, 67, 'libero nec', 'aliquet, sem ut cursus luctus, ipsum leo elementum sem,', 4),
(2, 68, 'Pellentesque habitant morbi tris', 'tortor. Integer aliquam', 3),
(2, 69, 'malesuada', 'ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet,', 3),
(2, 70, 'sed consequat auctor,', 'Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui augue eu tellus. Phasellus elit pede, malesuada vel, venenatis', 3),
(2, 71, 'risus. Quisque libero lacus,', 'tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent interdum ligula eu', 2),
(2, 72, 'id, ante.', 'ac tellus. Suspendisse sed dolor. Fusce mi', 4),
(2, 73, 'commodo auctor velit. Aliquam ni', 'enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius et, euismod et, commodo at, libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus', 4),
(2, 74, 'tincidunt congue turpis. In', 'ut quam vel sapien imperdiet ornare. In faucibus. Morbi vehicula. Pellentesque tincidunt tempus risus. Donec egestas. Duis ac arcu. Nunc mauris.', 2),
(2, 75, 'parturient', 'mollis', 2),
(2, 76, 'lobortis quis,', 'consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non', 3),
(2, 77, 'eget ipsum. Suspendisse sagittis', 'turpis. Aliquam adipiscing lobortis risus. In mi pede, nonummy ut, molestie in,', 2),
(2, 78, 'Nam', 'Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus', 5),
(2, 81, 'ut', 'mattis. Cras eget nisi dictum augue malesuada malesuada. Integer id magna et', 2),
(2, 82, 'lobortis. Class', 'consequat auctor, nunc nulla vulputate dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla', 3),
(2, 83, 'magna. Nam ligula elit, pretium', 'montes,', 4),
(2, 84, 'tincidunt', 'velit. Sed malesuada augue ut lacus. Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse', 3),
(2, 85, 'cursus luctus, ipsum leo element', 'ullamcorper eu, euismod ac, fermentum vel, mauris. Integer sem elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed et libero. Proin mi. Aliquam gravida mauris ut mi. Duis', 4),
(2, 86, 'fames ac', 'semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis', 3),
(2, 87, 'Etiam ligula tortor, dictum', 'velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi metus. Vivamus euismod urna.', 3),
(2, 88, 'eget massa.', 'tempus risus. Donec egestas. Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing.', 1),
(2, 89, 'imperdiet ornare.', 'Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla', 4),
(2, 90, 'et magnis dis parturient montes,', 'tincidunt congue turpis. In condimentum. Donec', 3),
(2, 91, 'et risus. Quisque', 'aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in molestie tortor nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie', 4),
(2, 92, 'Pellentesque ultricies dignissim', 'Mauris blandit enim consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec,', 4),
(2, 93, 'mi.', 'sit amet nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet, metus urna convallis erat, eget tincidunt dui augue', 2),
(2, 94, 'penatibus et magnis dis', 'augue. Sed molestie. Sed id risus quis diam', 3),
(2, 95, 'aliquet diam.', 'nec tempus scelerisque, lorem ipsum sodales purus, in molestie tortor nibh sit amet orci.', 1),
(2, 96, 'justo. Proin', 'cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at', 1),
(2, 97, 'dictum eu,', 'hymenaeos. Mauris ut quam vel sapien imperdiet ornare. In faucibus. Morbi vehicula. Pellentesque tincidunt tempus risus. Donec egestas. Duis ac arcu. Nunc mauris. Morbi non sapien', 5),
(2, 98, 'augue scelerisque mollis. Phasel', 'dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna.', 4),
(2, 99, 'est, vitae sodales nisi', 'ultricies adipiscing, enim mi tempor lorem, eget mollis lectus', 4),
(2, 100, 'lacus. Ut nec urna et', 'sociosqu ad litora torquent per conubia', 3),
(2, 101, 'ultrices. Duis', 'Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc', 1),
(2, 102, 'ligula consectetuer rhoncus.', 'Vivamus euismod urna. Nullam lobortis quam a felis ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae,', 4),
(2, 103, 'lorem, auctor quis, tristique ac', 'tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet', 1),
(2, 104, 'magnis', 'enim diam vel arcu. Curabitur ut odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue', 3),
(2, 105, 'at, velit.', 'felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed', 4),
(2, 106, 'elit fermentum risus, at', 'cursus in, hendrerit consectetuer, cursus et, magna. Praesent interdum ligula eu enim. Etiam imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus', 5),
(2, 107, 'Nullam lobortis quam', 'amet ornare lectus justo eu arcu. Morbi sit amet massa.', 4),
(2, 108, 'bibendum ullamcorper. Duis cursu', 'ac nulla. In tincidunt congue turpis. In condimentum. Donec', 1),
(2, 109, 'dictum.', 'vitae, aliquet nec, imperdiet nec, leo. Morbi neque tellus, imperdiet non, vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit', 4),
(2, 110, 'at, libero. Morbi', 'Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada', 2),
(2, 111, 'quis diam.', 'Nulla tempor augue ac ipsum. Phasellus vitae mauris', 4),
(2, 112, 'elit.', 'tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui', 3),
(2, 113, 'rutrum urna,', 'lacinia at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna.', 2),
(2, 114, 'ligula consectetuer rhoncus. Nul', 'vel, convallis in, cursus et, eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus a ultricies', 2),
(2, 115, 'enim. Curabitur', 'libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum', 1),
(2, 116, 'magnis dis parturient', 'mi eleifend egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem. Donec elementum, lorem ut aliquam iaculis, lacus', 3),
(2, 117, 'sem magna nec quam. Curabitur', 'pharetra. Quisque ac libero nec ligula consectetuer rhoncus. Nullam velit dui, semper et, lacinia vitae, sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum', 1),
(2, 118, 'rutrum urna, nec', 'lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent', 1),
(2, 119, 'Aliquam', 'nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque', 5),
(2, 120, 'Duis mi enim, condimentum', 'pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor.', 3),
(2, 121, 'iaculis odio. Nam interdum enim', 'erat neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed sem egestas blandit. Nam nulla', 5),
(2, 122, 'ipsum porta elit,', 'interdum. Curabitur dictum. Phasellus in felis.', 1),
(2, 123, 'Aenean sed pede', 'Proin nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id', 5),
(2, 124, 'ac, feugiat', 'ut', 5),
(2, 125, 'eros non', 'Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare,', 4),
(2, 126, 'dolor dolor, tempus', 'consequat enim diam vel arcu. Curabitur ut odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus', 3),
(2, 127, 'id enim. Curabitur massa. Vestib', 'eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non lorem vitae', 1),
(2, 128, 'dignissim tempor arcu.', 'Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu.', 5),
(2, 129, 'et', 'odio. Phasellus at augue id ante dictum cursus. Nunc', 4),
(2, 130, 'morbi', 'tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a purus.', 4),
(2, 131, 'dictum. Proin', 'non nisi. Aenean eget metus.', 3),
(2, 132, 'purus ac tellus. Suspendisse sed', 'sed dui. Fusce aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in molestie tortor nibh sit', 4),
(2, 133, 'dolor quam,', 'ridiculus mus. Donec dignissim magna a tortor.', 2),
(2, 134, 'Cras', 'penatibus et', 3),
(2, 135, 'Suspendisse ac metus', 'turpis. Nulla aliquet. Proin velit. Sed malesuada augue ut lacus. Nulla tincidunt, neque vitae semper egestas,', 1),
(2, 136, 'Integer in magna. Phasellus dolo', 'semper rutrum. Fusce dolor quam, elementum at, egestas a,', 3),
(2, 137, 'eleifend vitae, erat. Vivamus', 'Aenean euismod mauris eu elit. Nulla facilisi. Sed neque. Sed eget', 5),
(2, 138, 'In nec orci. Donec nibh.', 'facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a', 4),
(2, 139, 'ornare,', 'massa. Integer vitae nibh. Donec est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus,', 1),
(2, 140, 'semper pretium neque. Morbi quis', 'mus. Proin vel arcu eu odio tristique pharetra. Quisque ac libero nec ligula consectetuer rhoncus. Nullam velit dui, semper et, lacinia vitae, sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam', 1),
(2, 141, 'adipiscing lacus. Ut', 'tellus sem mollis dui, in sodales elit erat vitae risus. Duis a mi fringilla mi lacinia mattis. Integer eu', 1),
(2, 142, 'quis urna. Nunc quis', 'risus odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi neque tellus, imperdiet non, vestibulum', 5),
(2, 143, 'Donec luctus aliquet odio. Etiam', 'mi, ac mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor', 5),
(2, 144, 'consectetuer', 'elementum, lorem ut aliquam iaculis, lacus pede sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam fringilla', 1),
(2, 145, 'sem', 'non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus non, egestas a, dui. Cras', 1),
(2, 146, 'Ut semper', 'Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem,', 3),
(3, 1, 'nec tempus mauris erat eget', 'dignissim', 3),
(3, 2, 'lobortis quam', 'vitae erat vel pede blandit congue. In scelerisque', 4),
(3, 3, 'laoreet posuere, enim nisl eleme', 'vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras', 5),
(3, 4, 'ultrices,', 'tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula.', 1),
(3, 5, 'facilisis, magna tellus faucibus', 'libero', 1),
(3, 6, 'non, vestibulum nec, euismod', 'diam. Proin dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio.', 2),
(3, 7, 'nascetur ridiculus mus. Aenean e', 'Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec', 2),
(3, 8, 'Vivamus molestie dapibus ligula.', 'felis. Donec tempor, est ac mattis semper, dui lectus rutrum urna, nec luctus felis', 4),
(3, 9, 'nec, leo.', 'porttitor eros nec tellus. Nunc lectus pede, ultrices', 4),
(3, 10, 'nisi dictum augue', 'consequat', 3),
(3, 11, 'Duis mi enim,', 'non, feugiat', 1),
(3, 12, 'non leo. Vivamus nibh', 'Sed', 1),
(3, 13, 'et pede. Nunc sed orci', 'diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel sapien imperdiet ornare. In', 3),
(3, 14, 'magna, malesuada vel, convallis', 'libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in', 1),
(3, 15, 'cursus in,', 'eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing ligula. Aenean gravida nunc sed pede. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus', 1),
(3, 16, 'at, nisi.', 'sed leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus', 5),
(3, 17, 'aliquet molestie tellus. Aenean', 'mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at fringilla purus mauris a', 5),
(3, 18, 'blandit viverra.', 'adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin a,', 5),
(3, 19, 'imperdiet dictum magna.', 'pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate', 3),
(3, 20, 'fermentum risus,', 'sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed', 1),
(3, 21, 'ultrices sit amet, risus.', 'sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi metus. Vivamus euismod', 5),
(3, 22, 'arcu', 'eros. Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus a ultricies adipiscing,', 3),
(3, 23, 'dui. Suspendisse ac metus', 'amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et', 4),
(3, 24, 'Duis mi enim, condimentum eget,', 'massa. Integer vitae nibh. Donec est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum leo elementum sem, vitae aliquam', 3),
(3, 25, 'ac mi eleifend', 'nec, eleifend non, dapibus rutrum, justo.', 1),
(3, 26, 'aliquet lobortis, nisi', 'enim non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim.', 1),
(3, 27, 'ac ipsum. Phasellus', 'tempus risus. Donec egestas. Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci', 1),
(3, 28, 'congue, elit sed consequat aucto', 'nec tellus. Nunc lectus pede, ultrices a, auctor non, feugiat nec, diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum.', 2),
(3, 29, 'purus gravida sagittis.', 'risus varius orci, in consequat enim diam vel arcu. Curabitur ut odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit', 2),
(3, 30, 'erat neque non quam.', 'arcu.', 2),
(3, 31, 'Vivamus nibh', 'habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus.', 1),
(3, 32, 'lorem ipsum sodales purus,', 'cubilia', 4),
(3, 33, 'arcu', 'lectus pede et risus. Quisque libero lacus, varius et, euismod et, commodo at, libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi', 1),
(3, 34, 'facilisis facilisis,', 'vulputate ullamcorper magna. Sed eu eros. Nam consequat', 1),
(3, 35, 'elementum, lorem', 'non, feugiat nec, diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis', 5),
(3, 36, 'quis diam.', 'turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim', 1),
(3, 37, 'accumsan neque et nunc. Quisque', 'amet, consectetuer adipiscing elit. Aliquam auctor, velit eget', 1),
(3, 38, 'rhoncus.', 'in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan neque et nunc. Quisque ornare tortor at risus. Nunc ac sem', 2),
(3, 39, 'Ut', 'vehicula. Pellentesque tincidunt tempus risus. Donec egestas. Duis ac arcu.', 4),
(3, 40, 'imperdiet ullamcorper. Duis at l', 'ante ipsum primis in faucibus orci', 2),
(3, 41, 'ac', 'scelerisque dui.', 5),
(3, 42, 'iaculis, lacus pede', 'vel pede blandit', 5),
(3, 43, 'Vestibulum accumsan neque et', 'Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque', 5),
(3, 44, 'senectus', 'est, vitae sodales nisi magna sed dui. Fusce aliquam, enim nec tempus scelerisque,', 1),
(3, 45, 'quis, pede. Suspendisse dui. Fus', 'rutrum magna. Cras convallis convallis', 1),
(3, 46, 'in', 'cursus. Integer mollis. Integer tincidunt aliquam', 5),
(3, 47, 'nunc risus', 'vitae, sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi metus. Vivamus euismod urna. Nullam lobortis quam a felis ullamcorper viverra. Maecenas iaculis', 1),
(3, 48, 'varius et, euismod', 'consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus.', 2),
(3, 49, 'dui quis accumsan convallis, ant', 'semper egestas, urna justo faucibus lectus, a sollicitudin orci', 5),
(3, 50, 'erat eget ipsum.', 'viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem,', 4),
(3, 51, 'fringilla', 'ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor', 5),
(3, 52, 'diam dictum', 'in felis. Nulla', 3),
(3, 53, 'Nullam ut nisi a odio', 'purus ac tellus. Suspendisse sed', 2),
(3, 54, 'cubilia Curae; Phasellus ornare.', 'ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus ornare. Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat', 4),
(3, 55, 'consequat purus. Maecenas libero', 'sed tortor. Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien,', 5),
(3, 56, 'sem elit, pharetra', 'egestas. Duis ac arcu. Nunc mauris.', 2),
(3, 57, 'Nam ligula elit, pretium et,', 'Nulla eget metus eu erat semper', 4),
(3, 58, 'neque venenatis', 'adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui', 1),
(3, 59, 'nonummy ipsum non arcu. Vivamus', 'et malesuada fames ac turpis egestas. Aliquam', 2),
(3, 60, 'pharetra sed, hendrerit a,', 'Cum sociis', 5),
(3, 61, 'nisi. Cum sociis natoque', 'et, commodo at, libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi sem semper erat, in consectetuer ipsum nunc id enim.', 4),
(3, 62, 'Donec tincidunt. Donec vitae era', 'aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada', 4),
(3, 63, 'et netus et malesuada fames', 'ante bibendum ullamcorper.', 3),
(3, 64, 'arcu. Curabitur ut odio', 'lacus pede sagittis augue, eu tempor erat neque non quam.', 2),
(3, 65, 'adipiscing elit. Curabitur sed', 'scelerisque dui. Suspendisse ac metus vitae velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui,', 2),
(3, 66, 'Nunc pulvinar arcu et', 'ut, nulla. Cras', 5),
(3, 67, 'ornare, lectus ante', 'ac ipsum. Phasellus vitae mauris sit amet lorem semper auctor. Mauris vel turpis. Aliquam adipiscing lobortis risus. In mi', 1),
(3, 68, 'euismod ac, fermentum vel,', 'rhoncus. Donec est. Nunc ullamcorper, velit in aliquet lobortis, nisi nibh lacinia orci, consectetuer', 3),
(3, 69, 'dictum eu, eleifend nec, malesua', 'sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris ut quam', 1),
(3, 70, 'erat vel pede blandit congue.', 'dis parturient montes, nascetur ridiculus mus. Proin vel nisl. Quisque fringilla euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris', 3),
(3, 71, 'est,', 'molestie in, tempus', 3),
(3, 72, 'eget', 'quam dignissim pharetra. Nam ac nulla. In tincidunt congue turpis. In condimentum. Donec at arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec', 2),
(3, 73, 'nisi', 'consectetuer,', 4),
(3, 74, 'nulla ante, iaculis', 'Sed et libero. Proin mi. Aliquam gravida mauris ut mi. Duis risus odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi neque tellus, imperdiet', 5),
(3, 75, 'quam dignissim pharetra. Nam', 'eu turpis. Nulla aliquet. Proin velit. Sed malesuada augue ut lacus. Nulla tincidunt, neque vitae semper', 2),
(3, 76, 'lorem ac risus.', 'sociis natoque penatibus et', 3),
(3, 77, 'Aenean sed pede', 'eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis', 2),
(3, 78, 'gravida sit amet,', 'Nunc mauris sapien, cursus in, hendrerit consectetuer, cursus et, magna. Praesent interdum ligula eu enim. Etiam imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam suscipit, est ac facilisis', 1),
(3, 81, 'purus gravida', 'Phasellus dolor elit, pellentesque a, facilisis non,', 4),
(3, 82, 'in', 'neque sed sem egestas blandit. Nam nulla magna, malesuada vel, convallis in, cursus', 4),
(3, 83, 'enim, condimentum eget, volutpat', 'neque sed sem egestas blandit. Nam nulla magna, malesuada vel,', 5),
(3, 84, 'non nisi. Aenean', 'amet, dapibus id,', 1),
(3, 85, 'venenatis a, magna.', 'convallis convallis', 1),
(3, 86, 'risus. Morbi metus. Vivamus euis', 'orci. Donec nibh. Quisque nonummy ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed nunc est, mollis non, cursus non, egestas', 4),
(3, 87, 'volutpat nunc sit amet metus.', 'non sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo', 4),
(3, 88, 'dis', 'magna. Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum, neque sed dictum eleifend, nunc risus varius orci, in', 3),
(3, 89, 'ac mattis ornare, lectus ante', 'accumsan neque et nunc. Quisque ornare tortor at risus. Nunc ac sem ut dolor dapibus gravida. Aliquam tincidunt, nunc ac mattis ornare, lectus ante dictum mi, ac', 3),
(3, 90, 'senectus et netus et', 'magna a tortor. Nunc commodo auctor velit. Aliquam nisl. Nulla eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc', 5),
(3, 91, 'Suspendisse sagittis. Nullam', 'neque. Nullam nisl. Maecenas', 5),
(3, 92, 'Ut semper pretium neque. Morbi', 'risus, at fringilla purus mauris a nunc. In at pede. Cras vulputate velit eu sem. Pellentesque ut ipsum ac mi eleifend egestas. Sed', 5),
(3, 93, 'amet risus.', 'Morbi vehicula. Pellentesque tincidunt', 3),
(3, 94, 'eleifend', 'justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate, lacus. Cras interdum. Nunc sollicitudin', 2),
(3, 95, 'sollicitudin commodo ipsum. Susp', 'placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed', 4),
(3, 96, 'Donec egestas.', 'est. Mauris eu turpis. Nulla aliquet. Proin velit. Sed malesuada augue ut lacus. Nulla tincidunt,', 3),
(3, 97, 'orci. Ut', 'malesuada id,', 2),
(3, 98, 'velit. Aliquam nisl.', 'libero. Donec consectetuer mauris id', 5),
(3, 99, 'dolor. Quisque', 'elementum,', 5),
(3, 100, 'vulputate velit eu sem. Pellente', 'auctor velit. Aliquam nisl. Nulla eu neque pellentesque', 2),
(3, 101, 'neque venenatis lacus.', 'nisi', 1),
(3, 102, 'nunc sed', 'Pellentesque tincidunt tempus risus. Donec egestas. Duis ac arcu. Nunc mauris. Morbi non sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu', 2),
(3, 103, 'quam. Pellentesque', 'faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae sodales nisi magna', 3),
(3, 104, 'sed', 'mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus', 3),
(3, 105, 'facilisis non, bibendum', 'In condimentum. Donec at arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae', 1),
(3, 106, 'Sed eu nibh vulputate', 'elit pede, malesuada vel, venenatis vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia at, iaculis quis,', 3),
(3, 107, 'sem', 'In nec orci. Donec', 5),
(3, 108, 'sapien imperdiet', 'auctor non, feugiat nec, diam. Duis mi enim, condimentum eget,', 4),
(3, 109, 'eu tempor', 'a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit,', 5),
(3, 110, 'nulla.', 'nonummy ultricies ornare, elit elit fermentum risus, at fringilla purus mauris a nunc. In at pede. Cras', 1),
(3, 111, 'a nunc. In at pede.', 'nascetur ridiculus mus. Aenean eget magna.', 3),
(3, 112, 'commodo hendrerit. Donec', 'Mauris magna. Duis dignissim tempor arcu. Vestibulum ut eros', 4),
(3, 113, 'sapien,', 'nascetur ridiculus mus. Proin vel arcu eu odio tristique pharetra. Quisque ac libero nec ligula consectetuer', 2),
(3, 114, 'eget', 'montes, nascetur ridiculus', 4),
(3, 115, 'fringilla purus mauris a', 'lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus. Nunc lectus pede,', 4),
(3, 116, 'gravida molestie arcu. Sed eu', 'sagittis semper. Nam tempor diam dictum sapien. Aenean massa. Integer vitae nibh. Donec est mauris, rhoncus', 1),
(3, 117, 'vel, venenatis vel, faucibus', 'eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar', 1),
(3, 118, 'fermentum fermentum', 'a, magna. Lorem ipsum dolor sit amet,', 3),
(3, 119, 'Curabitur', 'sit amet risus.', 2),
(3, 120, 'enim', 'ante blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit amet ultricies sem magna nec', 2),
(3, 121, 'pharetra. Quisque', 'turpis egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed sem egestas blandit. Nam nulla magna, malesuada vel, convallis in, cursus et, eros. Proin', 3),
(3, 122, 'eget lacus. Mauris non dui', 'ligula consectetuer rhoncus. Nullam velit dui, semper et, lacinia vitae, sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam', 2),
(3, 123, 'nisl. Quisque fringilla', 'et', 2),
(3, 124, 'penatibus et', 'ligula consectetuer rhoncus. Nullam velit dui, semper et,', 3),
(3, 125, 'ante.', 'feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum', 2),
(3, 126, 'non', 'elit erat vitae risus. Duis a mi fringilla mi lacinia mattis.', 3),
(3, 127, 'at auctor ullamcorper, nisl', 'sodales at,', 4),
(3, 128, 'at', 'semper. Nam tempor diam dictum sapien. Aenean massa. Integer vitae nibh. Donec est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut', 5),
(3, 129, 'sem, vitae aliquam eros', 'gravida. Aliquam tincidunt, nunc ac mattis ornare, lectus ante dictum mi, ac mattis velit', 3),
(3, 130, 'dui. Cum sociis', 'Vivamus non lorem vitae odio sagittis semper. Nam tempor diam dictum sapien. Aenean massa.', 3),
(3, 131, 'Curabitur', 'et magnis dis parturient montes, nascetur ridiculus mus. Proin vel', 2),
(3, 132, 'nisl elementum', 'egestas, urna justo', 2),
(3, 133, 'aliquam, enim nec tempus', 'Proin vel nisl. Quisque fringilla euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla ante,', 4),
(3, 134, 'lectus justo eu arcu.', 'non lorem vitae odio sagittis semper. Nam tempor diam dictum sapien. Aenean massa. Integer vitae nibh. Donec est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet,', 2),
(3, 135, 'Nunc', 'mauris. Suspendisse', 5),
(3, 136, 'accumsan', 'amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel nisl. Quisque fringilla euismod enim. Etiam gravida molestie', 3),
(3, 137, 'erat volutpat.', 'faucibus lectus, a sollicitudin orci', 2),
(3, 138, 'in,', 'sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus', 3),
(3, 139, 'nec, malesuada ut, sem.', 'vel quam dignissim pharetra. Nam ac nulla. In tincidunt congue turpis. In condimentum. Donec at arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices', 2),
(3, 140, 'montes,', 'eu, ligula. Aenean euismod mauris eu elit. Nulla facilisi. Sed neque. Sed eget lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu.', 5),
(3, 141, 'lacus. Ut nec urna', 'tellus justo sit amet nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium', 3),
(3, 142, 'Pellentesque habitant morbi tris', 'gravida. Aliquam', 2),
(3, 143, 'feugiat placerat velit.', 'volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat', 3),
(3, 144, 'amet lorem semper auctor. Mauris', 'Duis a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at fringilla purus mauris a nunc. In at pede. Cras', 5),
(3, 145, 'Fusce aliquet magna a neque.', 'in aliquet lobortis, nisi nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut semper pretium neque. Morbi quis urna.', 2),
(3, 146, 'aliquet nec,', 'dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit eget laoreet posuere, enim nisl', 3),
(4, 1, 'amet risus.', 'dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu', 1),
(4, 2, 'pede.', 'ipsum primis in faucibus orci luctus et ultrices posuere cubilia', 2),
(4, 3, 'bibendum fermentum', 'sem eget massa. Suspendisse eleifend.', 2),
(4, 4, 'arcu vel quam dignissim pharetra', 'neque venenatis lacus. Etiam bibendum fermentum', 2),
(4, 5, 'dui.', 'ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl. Maecenas malesuada fringilla est. Mauris eu', 5),
(4, 6, 'ac', 'Mauris magna. Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit', 2),
(4, 7, 'eget,', 'Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque neque. Nullam nisl.', 5),
(4, 8, 'tincidunt tempus', 'Sed auctor odio a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae sodales nisi magna sed dui. Fusce aliquam, enim nec', 4),
(4, 9, 'tellus', 'Aenean gravida nunc sed pede. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel arcu eu odio tristique pharetra. Quisque', 2),
(4, 10, 'sit amet diam', 'non, bibendum sed, est. Nunc laoreet', 4),
(4, 11, 'elit', 'non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras', 2),
(4, 12, 'in', 'aliquam iaculis, lacus pede sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam', 5),
(4, 13, 'ullamcorper', 'egestas. Aliquam fringilla cursus', 3),
(4, 14, 'pretium neque. Morbi quis urna.', 'ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum', 1),
(4, 15, 'blandit mattis. Cras eget nisi', 'et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam', 5),
(4, 16, 'sem, vitae', 'justo sit amet nulla. Donec non justo. Proin', 1),
(4, 17, 'mauris sit', 'accumsan sed, facilisis', 1),
(4, 18, 'Fusce aliquet magna a neque.', 'velit', 5),
(4, 19, 'adipiscing, enim mi tempor', 'luctus, ipsum leo elementum sem, vitae aliquam eros turpis', 4),
(4, 20, 'vulputate mauris sagittis placer', 'magna. Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit id, ante.', 3),
(4, 21, 'sagittis', 'Morbi', 3),
(4, 22, 'Suspendisse tristique neque vene', 'dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit eget laoreet posuere,', 4),
(4, 23, 'Ut tincidunt orci quis lectus.', 'tincidunt nibh. Phasellus nulla. Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem, eget', 3),
(4, 24, 'Vivamus nisi. Mauris', 'nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam', 3),
(4, 25, 'litora', 'Integer sem elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed et libero. Proin mi.', 5),
(4, 26, 'sed sem egestas blandit. Nam', 'felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem. Donec elementum, lorem ut aliquam iaculis, lacus pede sagittis augue, eu tempor erat', 1),
(4, 27, 'habitant', 'eu turpis.', 1),
(4, 28, 'fringilla, porttitor vulputate, ', 'massa. Quisque porttitor eros nec tellus. Nunc lectus pede, ultrices a,', 5),
(4, 29, 'Mauris molestie pharetra nibh.', 'sapien, gravida non, sollicitudin a, malesuada id, erat. Etiam vestibulum', 1),
(4, 30, 'Vivamus non lorem vitae odio', 'tortor nibh sit', 1),
(4, 31, 'Aliquam tincidunt, nunc', 'Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at,', 4),
(4, 32, 'venenatis a, magna. Lorem ipsum', 'neque et nunc. Quisque ornare tortor at risus. Nunc ac sem ut dolor dapibus gravida. Aliquam tincidunt, nunc ac mattis ornare, lectus ante dictum mi, ac mattis velit justo nec', 1),
(4, 33, 'sit amet, consectetuer adipiscin', 'malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor', 2),
(4, 34, 'arcu imperdiet ullamcorper. Duis', 'ut, pellentesque eget,', 1),
(4, 35, 'adipiscing', 'vitae aliquam eros turpis non enim. Mauris quis', 3),
(4, 36, 'sed,', 'quam. Pellentesque habitant morbi tristique senectus et netus et', 3),
(4, 37, 'porttitor interdum. Sed auctor', 'scelerisque dui. Suspendisse ac metus vitae velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui, nec tempus mauris erat eget ipsum.', 2),
(4, 38, 'quam quis diam. Pellentesque', 'dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum', 1),
(4, 39, 'ac urna. Ut', 'penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 2),
(4, 40, 'ultricies dignissim lacus. Aliqu', 'vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce', 3),
(4, 41, 'commodo hendrerit.', 'magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean sed pede nec ante blandit viverra.', 3),
(4, 42, 'Phasellus in felis. Nulla', 'mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet, dapibus id, blandit at, nisi. Cum sociis natoque penatibus et magnis', 5),
(4, 43, 'et ultrices posuere', 'tellus. Aenean egestas hendrerit neque. In ornare sagittis felis. Donec', 3),
(4, 44, 'quis diam. Pellentesque habitant', 'sed orci lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed,', 4),
(4, 45, 'tellus. Aenean', 'dignissim magna', 3),
(4, 46, 'ipsum. Curabitur consequat,', 'Aliquam rutrum lorem ac', 3),
(4, 47, 'lorem, luctus ut, pellentesque', 'natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean', 4),
(4, 48, 'litora torquent per conubia nost', 'Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem,', 4),
(4, 49, 'enim, gravida sit amet,', 'netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam ultrices iaculis odio.', 3),
(4, 50, 'dolor dapibus gravida. Aliquam', 'eu metus. In lorem. Donec', 4),
(4, 51, 'Vivamus sit', 'sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel nisl. Quisque fringilla euismod enim.', 3),
(4, 52, 'fermentum', 'Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at fringilla', 5),
(4, 53, 'tristique pellentesque, tellus', 'risus. Quisque libero lacus, varius et, euismod et, commodo at, libero. Morbi accumsan', 1),
(4, 54, 'ante blandit viverra. Donec temp', 'vitae diam. Proin dolor. Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum', 4),
(4, 55, 'rutrum eu, ultrices', 'Nullam suscipit, est ac facilisis', 2),
(4, 56, 'Curae; Donec', 'eu metus. In lorem. Donec elementum, lorem ut aliquam iaculis, lacus pede sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique senectus et', 2),
(4, 57, 'natoque penatibus et magnis dis', 'lacus. Aliquam rutrum lorem ac risus. Morbi metus. Vivamus euismod urna. Nullam lobortis quam a felis', 1),
(4, 58, 'tincidunt dui augue', 'risus. Quisque libero lacus, varius et,', 3),
(4, 59, 'sollicitudin', 'eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in aliquet lobortis, nisi nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut', 1),
(4, 60, 'fringilla euismod enim. Etiam gr', 'et netus et malesuada', 1),
(4, 61, 'malesuada augue ut lacus. Nulla', 'lacus. Etiam bibendum fermentum metus. Aenean', 2),
(4, 62, 'nec urna suscipit nonummy. Fusce', 'auctor odio a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est, vitae sodales nisi magna sed dui. Fusce aliquam, enim', 3),
(4, 63, 'tristique senectus et netus et', 'urna. Ut tincidunt vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum', 5),
(4, 64, 'purus, accumsan', 'erat. Sed nunc est, mollis non, cursus non, egestas a, dui.', 4),
(4, 65, 'tincidunt dui augue eu', 'tristique pellentesque, tellus sem mollis dui, in sodales elit erat vitae risus. Duis a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit', 4),
(4, 66, 'dolor. Fusce', 'Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa.', 1);
INSERT INTO `review` (`user_id`, `product_id`, `title`, `body`, `rating`) VALUES
(4, 67, 'non, cursus non, egestas a,', 'facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo sit amet nulla. Donec non justo. Proin non massa non ante bibendum ullamcorper. Duis cursus, diam at pretium aliquet,', 3),
(4, 68, 'Phasellus dolor elit,', 'non, sollicitudin a, malesuada id, erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede', 3),
(4, 69, 'a', 'malesuada augue ut lacus. Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci', 4),
(4, 70, 'sagittis felis. Donec tempor, es', 'dui, in sodales elit erat vitae risus. Duis a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus,', 2),
(4, 71, 'enim. Sed nulla ante, iaculis', 'neque. Nullam', 5),
(4, 72, 'Cras', 'tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim', 3),
(4, 73, 'mi, ac mattis velit', 'Aliquam adipiscing lobortis risus. In mi pede, nonummy ut, molestie', 5),
(4, 74, 'elementum purus, accumsan interd', 'nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet', 2),
(4, 75, 'rhoncus. Proin nisl', 'vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero.', 1),
(4, 76, 'commodo ipsum. Suspendisse non l', 'erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt', 2),
(4, 77, 'odio a', 'Cras eget nisi dictum augue malesuada malesuada. Integer id magna', 3),
(4, 78, 'Suspendisse aliquet, sem', 'egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam', 2),
(4, 81, 'nec tellus. Nunc lectus pede,', 'nec tempus scelerisque, lorem ipsum sodales purus, in', 2),
(4, 82, 'ipsum cursus vestibulum. Mauris ', 'ipsum. Donec', 1),
(4, 83, 'arcu.', 'nulla vulputate dui, nec tempus mauris erat eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper tellus id nunc interdum', 2),
(4, 84, 'eu metus.', 'convallis convallis dolor. Quisque tincidunt pede', 4),
(4, 85, 'Duis', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel arcu eu', 5),
(4, 86, 'sodales elit erat vitae', 'sem semper erat, in consectetuer', 2),
(4, 87, 'Sed molestie. Sed', 'Curabitur egestas', 3),
(4, 88, 'parturient', 'et malesuada fames ac turpis egestas. Aliquam fringilla cursus purus. Nullam scelerisque neque sed sem egestas blandit. Nam nulla magna, malesuada vel,', 3),
(4, 89, 'mattis. Integer eu lacus. Quisqu', 'pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus', 5),
(4, 90, 'adipiscing, enim mi tempor', 'ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis.', 2),
(4, 91, 'scelerisque', 'Maecenas libero est, congue a, aliquet vel, vulputate eu, odio.', 4),
(4, 92, 'taciti sociosqu ad litora', 'amet ultricies sem magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec dignissim magna a', 5),
(4, 93, 'lacus. Cras interdum. Nunc solli', 'et ipsum', 5),
(4, 94, 'congue. In scelerisque scelerisq', 'aliquet molestie tellus. Aenean egestas hendrerit neque. In', 5),
(4, 95, 'eu', 'convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis a, magna. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque,', 4),
(4, 96, 'Donec tincidunt.', 'tellus justo', 5),
(4, 97, 'luctus.', 'aliquam adipiscing', 2),
(4, 98, 'tristique senectus et', 'faucibus ut, nulla. Cras eu tellus eu augue porttitor interdum. Sed auctor odio a purus. Duis elementum, dui quis accumsan convallis, ante lectus convallis est,', 4),
(4, 99, 'cubilia Curae; Phasellus ornare.', 'Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus', 4),
(4, 100, 'et, eros.', 'fermentum fermentum arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus ornare. Fusce mollis. Duis sit amet', 4),
(4, 101, 'lobortis quis, pede.', 'non, luctus sit amet, faucibus ut,', 3),
(4, 102, 'tincidunt. Donec', 'a, auctor non, feugiat nec, diam. Duis mi enim, condimentum eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing', 4),
(4, 103, 'ipsum', 'neque tellus, imperdiet non, vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit eget laoreet posuere, enim nisl elementum', 4),
(4, 104, 'blandit at, nisi. Cum', 'feugiat non, lobortis quis, pede. Suspendisse dui. Fusce diam nunc, ullamcorper eu, euismod ac, fermentum vel, mauris. Integer sem elit, pharetra ut, pharetra sed,', 4),
(4, 105, 'sollicitudin', 'accumsan laoreet ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi', 1),
(4, 106, 'non magna.', 'montes, nascetur ridiculus mus. Proin vel nisl. Quisque fringilla euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed', 1),
(4, 107, 'vel sapien imperdiet ornare.', 'consequat nec, mollis vitae, posuere at, velit. Cras', 3),
(4, 108, 'metus', 'a, malesuada id, erat. Etiam', 4),
(4, 109, 'Vivamus nisi. Mauris', 'ligula consectetuer rhoncus. Nullam velit dui, semper et,', 2),
(4, 110, 'purus ac tellus. Suspendisse sed', 'tempor augue ac ipsum. Phasellus vitae mauris sit amet lorem semper auctor. Mauris vel turpis. Aliquam adipiscing lobortis risus. In mi pede, nonummy ut, molestie in, tempus', 3),
(4, 111, 'vulputate dui,', 'enim consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum', 2),
(4, 112, 'at, libero. Morbi accumsan', 'dolor dapibus gravida. Aliquam tincidunt, nunc ac mattis ornare, lectus ante dictum mi, ac mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate,', 1),
(4, 113, 'egestas rhoncus. Proin nisl sem,', 'turpis egestas. Fusce aliquet magna a neque. Nullam', 2),
(4, 114, 'ac mi eleifend egestas. Sed', 'a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer', 1),
(4, 115, 'libero', 'Morbi accumsan laoreet ipsum. Curabitur consequat, lectus', 1),
(4, 116, 'augue ut lacus. Nulla tincidunt,', 'non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit,', 3),
(4, 117, 'augue. Sed', 'sapien. Aenean massa. Integer vitae nibh. Donec est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse', 1),
(4, 118, 'eu eros. Nam consequat', 'enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius et, euismod et, commodo at, libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus sit amet', 1),
(4, 119, 'accumsan interdum libero dui nec', 'pede.', 1),
(4, 120, 'Aenean', 'mauris. Morbi non sapien molestie orci', 5),
(4, 121, 'mi felis, adipiscing fringilla, ', 'libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec tellus.', 3),
(4, 122, 'ipsum primis in faucibus orci', 'id, blandit at, nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel', 5),
(4, 123, 'purus ac', 'purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate eu, odio. Phasellus', 2),
(4, 124, 'nunc sed', 'ligula. Nullam feugiat', 4),
(4, 125, 'velit in aliquet', 'nunc nulla vulputate dui, nec tempus mauris erat', 5),
(4, 126, 'libero.', 'semper erat, in consectetuer ipsum nunc', 1),
(4, 127, 'luctus, ipsum', 'neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames', 4),
(4, 128, 'lorem, eget mollis lectus', 'mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc', 1),
(4, 129, 'faucibus lectus, a sollicitudin', 'pharetra. Nam ac nulla. In tincidunt congue turpis. In condimentum. Donec at arcu. Vestibulum ante', 4),
(4, 130, 'urna. Nullam lobortis quam a', 'tempor diam dictum sapien. Aenean massa. Integer vitae nibh. Donec est mauris, rhoncus id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum', 1),
(4, 131, 'rhoncus. Proin', 'placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc', 1),
(4, 132, 'ut lacus.', 'faucibus id,', 2),
(4, 133, 'enim mi tempor lorem, eget', 'viverra. Donec tempus, lorem fringilla ornare placerat, orci lacus vestibulum lorem, sit amet ultricies sem magna nec quam. Curabitur', 3),
(4, 134, 'Phasellus dapibus quam quis diam', 'Cum sociis', 4),
(4, 135, 'ornare placerat, orci', 'enim, sit amet ornare lectus justo eu', 5),
(4, 136, 'a,', 'sem. Pellentesque ut ipsum ac mi eleifend egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum porta', 4),
(4, 137, 'Maecenas mi felis, adipiscing', 'placerat eget, venenatis a,', 3),
(4, 138, 'est, vitae sodales', 'Nunc ut erat. Sed nunc est, mollis non,', 1),
(4, 139, 'mi. Aliquam gravida mauris', 'mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at fringilla purus mauris a nunc. In at', 2),
(4, 140, 'mauris erat eget ipsum.', 'egestas blandit. Nam nulla magna, malesuada', 1),
(4, 141, 'molestie', 'orci luctus et ultrices posuere', 5),
(4, 142, 'In tincidunt congue', 'Quisque fringilla euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla ante,', 3),
(4, 143, 'enim nisl elementum purus, accum', 'eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non lorem vitae odio sagittis semper. Nam tempor diam', 1),
(4, 144, 'malesuada fames ac turpis', 'rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim, gravida sit amet,', 4),
(4, 145, 'gravida', 'est, congue a, aliquet vel, vulputate eu, odio. Phasellus at augue id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum.', 1),
(4, 146, 'viverra.', 'Nunc mauris. Morbi non sapien molestie', 1),
(5, 1, 'euismod', 'lobortis, nisi nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut semper pretium neque. Morbi quis urna. Nunc quis arcu vel quam dignissim pharetra. Nam ac nulla. In tincidunt', 2),
(5, 2, 'Sed', 'ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit', 2),
(5, 3, 'Ut semper pretium', 'sit amet luctus vulputate, nisi sem semper erat, in consectetuer ipsum nunc id enim. Curabitur massa. Vestibulum accumsan neque et nunc. Quisque ornare tortor at risus. Nunc', 3),
(5, 4, 'erat vel pede', 'erat. Etiam vestibulum massa rutrum magna. Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt', 1),
(5, 5, 'sem ut cursus luctus,', 'interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo. Vivamus nibh dolor, nonummy ac, feugiat non, lobortis quis, pede. Suspendisse dui. Fusce diam nunc,', 1),
(5, 6, 'ultrices', 'gravida mauris ut mi. Duis risus odio, auctor vitae, aliquet nec, imperdiet nec, leo. Morbi neque tellus, imperdiet non, vestibulum nec, euismod in,', 1),
(5, 7, 'dolor', 'rutrum urna, nec luctus felis purus ac tellus.', 2),
(5, 8, 'feugiat. Sed nec metus facilisis', 'vitae erat vel pede blandit congue. In scelerisque', 4),
(5, 9, 'non,', 'lectus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 1),
(5, 10, 'accumsan laoreet ipsum.', 'Fusce fermentum fermentum arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus ornare. Fusce mollis.', 5),
(5, 11, 'vel, convallis', 'adipiscing elit. Aliquam auctor, velit eget', 5),
(5, 12, 'sed,', 'diam luctus lobortis. Class aptent taciti sociosqu ad', 3),
(5, 13, 'malesuada augue ut lacus.', 'Cras convallis convallis dolor. Quisque tincidunt pede ac urna. Ut', 1),
(5, 14, 'imperdiet ornare. In faucibus.', 'sit amet ante. Vivamus', 5),
(5, 15, 'tempor bibendum. Donec felis', 'metus. Vivamus euismod urna. Nullam', 4),
(5, 16, 'ac mattis', 'Mauris eu turpis. Nulla aliquet. Proin velit. Sed malesuada augue ut lacus. Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a sollicitudin orci sem', 5),
(5, 17, 'felis, adipiscing fringilla,', 'ipsum ac mi eleifend egestas. Sed pharetra, felis eget varius ultrices, mauris ipsum porta elit, a feugiat tellus lorem eu metus. In lorem.', 1),
(5, 18, 'non, vestibulum', 'tincidunt congue turpis. In condimentum. Donec at arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;', 1),
(5, 19, 'urna. Nunc quis arcu', 'convallis in, cursus', 5),
(5, 20, 'ultrices', 'lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris', 5),
(5, 21, 'interdum enim', 'consequat dolor vitae', 3),
(5, 22, 'tincidunt aliquam arcu. Aliquam ', 'tempor arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus', 5),
(5, 23, 'ultrices a, auctor non, feugiat', 'turpis nec mauris blandit', 1),
(5, 24, 'magna. Lorem', 'vitae sodales nisi magna sed dui. Fusce aliquam, enim nec tempus scelerisque, lorem ipsum sodales purus, in molestie tortor nibh sit amet orci. Ut sagittis', 4),
(5, 25, 'diam. Proin dolor.', 'nec urna et arcu imperdiet ullamcorper.', 1),
(5, 26, 'arcu.', 'lorem lorem, luctus ut,', 2),
(5, 27, 'ipsum primis', 'id ante dictum cursus. Nunc mauris elit, dictum eu, eleifend nec, malesuada ut, sem. Nulla interdum. Curabitur dictum. Phasellus in felis.', 5),
(5, 28, 'lacus vestibulum', 'urna justo faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse eleifend. Cras sed leo. Cras vehicula aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque a,', 2),
(5, 29, 'quis lectus. Nullam', 'et, lacinia vitae, sodales at, velit. Pellentesque ultricies dignissim lacus. Aliquam rutrum lorem ac risus. Morbi metus. Vivamus euismod urna. Nullam lobortis', 1),
(5, 30, 'et,', 'dictum eu, eleifend nec,', 3),
(5, 31, 'id nunc', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin', 1),
(5, 32, 'diam luctus lobortis. Class apte', 'tellus justo', 3),
(5, 33, 'ac', 'ipsum non arcu. Vivamus sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut erat. Sed', 1),
(5, 34, 'natoque', 'lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui,', 1),
(5, 35, 'Fusce', 'Curae; Phasellus ornare. Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget,', 2),
(5, 36, 'dictum eleifend,', 'mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies', 4),
(5, 37, 'mus. Donec dignissim magna', 'amet ultricies sem magna nec quam. Curabitur vel lectus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur', 2),
(5, 38, 'tellus. Aenean egestas', 'lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus', 3),
(5, 39, 'Proin non massa non', 'sit amet ante. Vivamus non lorem vitae odio sagittis semper. Nam tempor diam dictum sapien. Aenean massa. Integer vitae', 2),
(5, 40, 'purus, in molestie', 'nisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin vel nisl. Quisque fringilla euismod enim. Etiam', 4),
(5, 41, 'nibh.', 'odio a purus. Duis elementum, dui quis accumsan convallis, ante lectus', 5),
(5, 42, 'mus. Proin vel nisl. Quisque', 'orci, in consequat enim diam vel arcu. Curabitur ut odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut,', 2),
(5, 43, 'cubilia Curae; Donec', 'iaculis enim, sit amet ornare lectus justo eu arcu. Morbi sit amet massa. Quisque porttitor eros nec', 4),
(5, 44, 'porttitor tellus', 'Proin ultrices. Duis volutpat nunc sit amet metus. Aliquam erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus a', 4),
(5, 45, 'et ultrices posuere', 'arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus ornare. Fusce mollis.', 4),
(5, 46, 'Nam', 'nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet molestie tellus.', 2),
(5, 47, 'rhoncus id, mollis', 'felis ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam lorem, auctor quis, tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie', 3),
(5, 48, 'Integer tincidunt aliquam', 'vestibulum nec, euismod in, dolor. Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam auctor, velit eget laoreet posuere, enim nisl', 2),
(5, 49, 'pede,', 'Phasellus nulla. Integer', 3),
(5, 50, 'quam, elementum at, egestas a,', 'enim', 2),
(5, 51, 'neque vitae semper egestas, urna', 'posuere at, velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat, augue. Sed molestie. Sed id risus quis diam', 5),
(5, 52, 'lectus convallis est,', 'odio tristique pharetra.', 5),
(5, 53, 'per inceptos', 'arcu. Aliquam ultrices iaculis odio. Nam interdum enim non nisi. Aenean eget metus. In nec orci. Donec nibh. Quisque nonummy ipsum non arcu.', 2),
(5, 54, 'est. Mauris eu turpis.', 'nisl. Nulla eu neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in aliquet lobortis, nisi nibh lacinia orci, consectetuer', 2),
(5, 55, 'Class aptent taciti', 'natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 5),
(5, 56, 'massa lobortis', 'Nunc ac sem ut dolor dapibus gravida. Aliquam tincidunt, nunc ac mattis ornare, lectus ante dictum mi, ac mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate,', 5),
(5, 57, 'nec tempus scelerisque, lorem ip', 'dolor. Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer adipiscing', 5),
(5, 58, 'ipsum leo elementum sem,', 'a, facilisis non, bibendum sed, est. Nunc laoreet lectus quis massa. Mauris vestibulum,', 2),
(5, 59, 'lorem. Donec elementum,', 'eget, ipsum.', 3),
(5, 60, 'urna. Ut tincidunt', 'Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem, eget mollis', 3),
(5, 61, 'morbi tristique senectus', 'est. Nunc laoreet lectus', 4),
(5, 62, 'quam. Curabitur', 'eu enim. Etiam imperdiet dictum magna. Ut tincidunt orci quis lectus. Nullam suscipit, est ac facilisis facilisis,', 4),
(5, 63, 'montes, nascetur ridiculus mus.', 'vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida.', 4),
(5, 64, 'Duis mi enim, condimentum eget,', 'a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius', 4),
(5, 65, 'eu elit.', 'pharetra, felis eget varius', 1),
(5, 66, 'sed turpis', 'Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas', 5),
(5, 67, 'Sed id risus', 'diam nunc, ullamcorper eu, euismod ac, fermentum vel, mauris. Integer sem elit, pharetra ut, pharetra sed, hendrerit a, arcu. Sed et libero. Proin mi. Aliquam gravida mauris ut mi. Duis', 5),
(5, 68, 'dictum magna.', 'Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui, in sodales elit erat vitae risus. Duis a mi fringilla mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat', 4),
(5, 69, 'enim commodo hendrerit.', 'semper cursus.', 3),
(5, 70, 'Aliquam', 'sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl arcu iaculis enim, sit amet ornare lectus justo eu arcu. Morbi', 3),
(5, 71, 'nec metus facilisis lorem tristi', 'ipsum sodales purus, in molestie tortor nibh sit amet orci. Ut sagittis lobortis mauris. Suspendisse aliquet', 1),
(5, 72, 'pretium et, rutrum non,', 'scelerisque dui. Suspendisse ac metus vitae velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla vulputate dui, nec tempus mauris erat', 3),
(5, 73, 'Nam nulla', 'lorem ac risus. Morbi metus. Vivamus', 4),
(5, 74, 'nec,', 'non, cursus non, egestas a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper', 2),
(5, 75, 'lectus quis massa. Mauris', 'at, iaculis quis, pede. Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean', 4),
(5, 76, 'nec quam. Curabitur vel', 'id, mollis nec, cursus a, enim. Suspendisse aliquet, sem ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis.', 1),
(5, 77, 'lectus ante dictum mi,', 'turpis. Nulla aliquet. Proin velit. Sed malesuada augue ut lacus. Nulla tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a', 4),
(5, 78, 'et', 'nonummy ut, molestie', 3),
(5, 81, 'id sapien. Cras', 'risus varius orci, in consequat enim diam vel arcu. Curabitur ut odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras eu tellus', 4),
(5, 82, 'pellentesque a, facilisis', 'vel turpis. Aliquam', 5),
(5, 83, 'felis eget varius', 'felis. Donec tempor, est ac mattis semper, dui lectus rutrum urna, nec luctus felis purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et,', 1),
(5, 84, 'eu, eleifend', 'Duis gravida. Praesent eu nulla at sem molestie sodales. Mauris blandit enim consequat purus. Maecenas libero est, congue a, aliquet vel, vulputate', 5),
(5, 85, 'dui lectus', 'leo. Vivamus nibh dolor, nonummy ac, feugiat non, lobortis quis, pede. Suspendisse dui. Fusce diam nunc, ullamcorper eu, euismod ac, fermentum', 5),
(5, 86, 'amet ornare', 'Donec egestas. Duis ac arcu. Nunc mauris. Morbi non sapien molestie', 4),
(5, 87, 'Mauris ut quam', 'Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum eu, ultrices sit amet, risus. Donec nibh enim,', 3),
(5, 88, 'lorem, eget', 'mollis. Integer tincidunt aliquam arcu. Aliquam', 2),
(5, 89, 'convallis erat,', 'scelerisque sed, sapien. Nunc pulvinar arcu et pede. Nunc sed orci lobortis', 4),
(5, 90, 'ante. Vivamus', 'diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a', 1),
(5, 91, 'aliquet diam. Sed diam', 'Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus', 4),
(5, 92, 'aliquet magna', 'Nulla semper tellus id nunc interdum feugiat. Sed nec metus facilisis lorem tristique aliquet. Phasellus fermentum convallis ligula. Donec luctus aliquet odio. Etiam ligula tortor, dictum', 3),
(5, 93, 'lorem.', 'Donec sollicitudin adipiscing ligula. Aenean gravida nunc', 5),
(5, 94, 'in faucibus orci', 'felis, adipiscing fringilla, porttitor vulputate, posuere vulputate,', 2),
(5, 95, 'at,', 'ante dictum mi, ac', 4),
(5, 96, 'enim nec tempus', 'eget, volutpat ornare, facilisis eget, ipsum. Donec sollicitudin adipiscing ligula. Aenean gravida nunc sed pede. Cum sociis', 3),
(5, 97, 'nunc. In at pede. Cras', 'metus vitae velit egestas lacinia. Sed congue, elit sed', 3),
(5, 98, 'condimentum.', 'sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris ut quam vel sapien imperdiet ornare. In faucibus. Morbi vehicula.', 1),
(5, 99, 'et, rutrum non, hendrerit id,', 'scelerisque mollis. Phasellus libero mauris, aliquam eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus quam quis diam. Pellentesque habitant morbi tristique senectus et netus et', 3),
(5, 100, 'lobortis,', 'sem ut cursus luctus, ipsum leo elementum sem, vitae aliquam eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis', 4),
(5, 101, 'Sed nunc', 'tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius et, euismod et, commodo at, libero. Morbi accumsan laoreet ipsum. Curabitur consequat, lectus sit amet', 3),
(5, 102, 'sollicitudin', 'tristique ac, eleifend vitae, erat. Vivamus nisi. Mauris nulla.', 1),
(5, 103, 'vitae odio', 'enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius et, euismod et, commodo at,', 2),
(5, 104, 'nisl sem, consequat', 'eros nec tellus. Nunc lectus pede, ultrices', 2),
(5, 105, 'montes, nascetur', 'dolor. Donec fringilla. Donec feugiat metus sit amet ante. Vivamus non lorem vitae odio', 3),
(5, 106, 'ut ipsum ac', 'adipiscing non, luctus sit amet, faucibus', 4),
(5, 107, 'velit eu sem. Pellentesque ut', 'lacus. Mauris non dui nec urna suscipit nonummy. Fusce fermentum fermentum arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus ornare. Fusce', 1),
(5, 108, 'tincidunt,', 'orci. Ut semper pretium', 2),
(5, 109, 'venenatis a,', 'porttitor tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus in, hendrerit consectetuer, cursus et,', 1),
(5, 110, 'sagittis lobortis', 'Nullam vitae diam. Proin dolor. Nulla semper tellus id nunc interdum feugiat.', 4),
(5, 111, 'vitae aliquam eros turpis non', 'Integer aliquam adipiscing lacus. Ut nec urna et arcu imperdiet ullamcorper. Duis at lacus. Quisque purus sapien, gravida non, sollicitudin', 3),
(5, 112, 'mus. Proin vel nisl.', 'id, blandit at, nisi. Cum sociis natoque', 4),
(5, 113, 'senectus et netus', 'lectus. Nullam suscipit, est ac facilisis facilisis, magna tellus faucibus leo, in lobortis tellus justo sit amet nulla. Donec non justo. Proin non massa', 3),
(5, 114, 'neque', 'pede sagittis augue, eu tempor erat neque non quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam fringilla cursus purus. Nullam', 3),
(5, 115, 'a nunc. In at pede.', 'diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra,', 3),
(5, 116, 'eget', 'aliquet. Phasellus fermentum convallis ligula. Donec', 3),
(5, 117, 'gravida', 'felis ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam lorem,', 3),
(5, 118, 'pede ac urna. Ut', 'ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend non, dapibus rutrum, justo. Praesent luctus. Curabitur egestas nunc sed libero. Proin sed turpis nec mauris blandit mattis.', 4),
(5, 119, 'ac metus vitae velit', 'purus ac tellus. Suspendisse sed dolor. Fusce mi lorem, vehicula et, rutrum', 3),
(5, 120, 'sagittis felis. Donec', 'Suspendisse commodo tincidunt nibh. Phasellus nulla. Integer vulputate, risus a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et', 4),
(5, 121, 'ac turpis egestas. Aliquam fring', 'cursus non, egestas a, dui. Cras pellentesque. Sed dictum. Proin eget odio. Aliquam vulputate ullamcorper magna. Sed eu eros. Nam', 4),
(5, 122, 'a, magna. Lorem ipsum dolor', 'turpis. Aliquam adipiscing', 1),
(5, 123, 'Morbi accumsan', 'eros turpis non enim. Mauris quis turpis vitae purus gravida sagittis. Duis gravida. Praesent eu nulla at sem molestie sodales.', 3),
(5, 124, 'odio semper cursus. Integer', 'euismod in,', 3),
(5, 125, 'tincidunt vehicula risus. Nulla ', 'mus. Proin vel nisl.', 5),
(5, 126, 'lacus pede sagittis augue,', 'consequat enim diam vel arcu. Curabitur ut odio vel est tempor bibendum. Donec felis orci, adipiscing non, luctus sit amet, faucibus ut,', 5),
(5, 127, 'sagittis augue, eu', 'aliquet, metus urna convallis', 1),
(5, 128, 'iaculis enim,', 'erat, in consectetuer ipsum nunc id', 3),
(5, 129, 'diam. Pellentesque', 'Fusce mollis. Duis sit amet diam eu dolor egestas rhoncus. Proin nisl sem, consequat nec, mollis vitae, posuere at, velit. Cras', 1),
(5, 130, 'et risus. Quisque libero lacus,', 'sit amet, consectetuer adipiscing elit. Aliquam auctor, velit eget', 3),
(5, 131, 'tempor augue ac ipsum. Phasellus', 'hendrerit consectetuer,', 4),
(5, 132, 'enim. Mauris quis turpis vitae', 'morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer', 4),
(5, 133, 'Etiam vestibulum', 'ultrices sit amet, risus. Donec nibh', 5),
(5, 134, 'sagittis', 'mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at fringilla purus mauris a', 2),
(5, 135, 'dolor', 'erat. Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas', 2),
(5, 136, 'Quisque nonummy ipsum', 'penatibus et magnis dis parturient montes, nascetur ridiculus', 4),
(5, 137, 'Proin non massa non', 'sed', 4),
(5, 138, 'imperdiet ullamcorper.', 'vestibulum. Mauris magna. Duis dignissim tempor arcu. Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna. Nam ligula elit, pretium et, rutrum', 3),
(5, 139, 'pede. Praesent', 'risus a ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius et, euismod et, commodo at, libero. Morbi accumsan laoreet', 3),
(5, 140, 'Quisque fringilla euismod enim.', 'diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos', 5),
(5, 141, 'ac mattis', 'euismod enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec, eleifend', 1),
(5, 142, 'mollis dui, in', 'non, bibendum sed,', 2),
(5, 143, 'nascetur ridiculus', 'vehicula risus. Nulla eget metus eu erat semper rutrum. Fusce dolor quam, elementum at, egestas a, scelerisque sed, sapien. Nunc pulvinar arcu et pede.', 2),
(5, 144, 'odio a purus. Duis', 'Praesent eu dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam bibendum fermentum metus. Aenean sed', 2),
(5, 145, 'euismod in, dolor. Fusce feugiat', 'Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat placerat velit. Quisque varius. Nam porttitor scelerisque', 2),
(5, 146, 'luctus.', 'risus. Quisque libero lacus, varius et, euismod et, commodo at,', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stock_order`
--

CREATE TABLE IF NOT EXISTS `stock_order` (
  `supplierId` int(10) unsigned NOT NULL,
  `productId` int(10) unsigned NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `cost` float DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`supplierId`,`productId`),
  KEY `SupplierId_idx` (`supplierId`),
  KEY `productId_idx` (`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `supplier_id` int(10) unsigned NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_order` int(11) DEFAULT NULL,
  `lead_time` int(11) DEFAULT NULL,
  `min_order` int(11) DEFAULT NULL COMMENT 'The minimum order that can be placed on this supplier',
  `address_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`supplier_id`),
  KEY `fk_supplier_address1_idx` (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `supplies`
--

CREATE TABLE IF NOT EXISTS `supplies` (
  `supplierId` int(10) unsigned NOT NULL,
  `productId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`supplierId`,`productId`),
  KEY `product_id_idx` (`productId`),
  KEY `supplier_id_idx` (`supplierId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `email`, `first_name`, `last_name`, `password`, `role`, `creation_date`, `status`) VALUES
(1, 'chris.loughnane@gmx.com', 'Chris', 'Loughnane', 'caf1a3dfb505ffed0d024130f58c5cfa', 'customer', '2013-02-21 14:37:00', 'yes'),
(2, 'riverpatrol@hotmail.com', 'Brian', 'Fitzpatrick', 'caf1a3dfb505ffed0d024130f58c5cfa', 'customer', '2013-02-21 14:52:12', 'yes'),
(3, 'a@hotmail.com', 'Shane', 'Ryan', 'caf1a3dfb505ffed0d024130f58c5cfa', 'employee', '2013-02-21 14:52:53', 'yes'),
(4, 'bb@hotmail.com', 'bb', 'bbbbbb', 'caf1a3dfb505ffed0d024130f58c5cfa', 'customer', '2013-02-21 14:53:51', 'yes'),
(5, 'cc@hotmail.com', 'Cc', 'cccc', 'caf1a3dfb505ffed0d024130f58c5cfa', 'customer', '2013-02-21 14:54:25', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `user_order`
--

CREATE TABLE IF NOT EXISTS `user_order` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` timestamp NULL DEFAULT NULL,
  `receipt` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `fk_customer_order_user1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `user_order`
--

INSERT INTO `user_order` (`order_id`, `date`, `receipt`, `status`, `user_id`) VALUES
(1, '2013-02-25 19:40:54', NULL, 'complete', 1),
(2, NULL, NULL, 'shopping_cart', 2),
(3, NULL, NULL, 'shopping_cart', 3),
(4, NULL, NULL, 'shopping_cart', 4),
(5, NULL, NULL, 'shopping_cart', 5),
(11, '2013-02-25 20:42:59', NULL, 'complete', 1),
(12, '2013-02-25 21:04:11', NULL, 'complete', 1),
(13, '2013-02-26 11:26:55', NULL, 'complete', 1),
(14, NULL, NULL, 'shopping_cart', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE IF NOT EXISTS `wishlist` (
  `wishlist_id` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`wishlist_id`),
  KEY `fk_wishlist_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wishlist_has_product`
--

CREATE TABLE IF NOT EXISTS `wishlist_has_product` (
  `wishlist_id` int(11) NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`wishlist_id`,`product_id`),
  KEY `fk_wishlist_has_product_product1_idx` (`product_id`),
  KEY `fk_wishlist_has_product_wishlist1_idx` (`wishlist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `fk_address_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `fk_employee_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `fk_product_has_user_order_product1` FOREIGN KEY (`product_product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_has_user_order_user_order1` FOREIGN KEY (`user_order_order_id`) REFERENCES `user_order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `fk_review_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_review_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `stock_order`
--
ALTER TABLE `stock_order`
  ADD CONSTRAINT `productId` FOREIGN KEY (`productId`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `SupplierId` FOREIGN KEY (`supplierId`) REFERENCES `supplier` (`supplier_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `supplier`
--
ALTER TABLE `supplier`
  ADD CONSTRAINT `addressId` FOREIGN KEY (`address_id`) REFERENCES `address` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `supplies`
--
ALTER TABLE `supplies`
  ADD CONSTRAINT `product_id` FOREIGN KEY (`productId`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `supplier_id` FOREIGN KEY (`supplierId`) REFERENCES `supplier` (`supplier_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_order`
--
ALTER TABLE `user_order`
  ADD CONSTRAINT `fk_customer_order_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `fk_wishlist_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `wishlist_has_product`
--
ALTER TABLE `wishlist_has_product`
  ADD CONSTRAINT `fk_wishlist_has_product_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_wishlist_has_product_wishlist1` FOREIGN KEY (`wishlist_id`) REFERENCES `wishlist` (`wishlist_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
